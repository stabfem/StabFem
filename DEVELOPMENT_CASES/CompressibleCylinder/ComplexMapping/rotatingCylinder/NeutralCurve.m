
clear all;
% close all;
run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
tinit = tic;
verbosity=20;


Ma = 0.3;
%% Recover previous mesh and bf
ffmesh = importFFmesh('WORK/MESHES/mesh_adapt17_Re100_Ma0.05_Omegax1.3.msh');
bf = importFFdata(ffmesh,'WORK/BASEFLOWS/BaseFlow_Re168.7804Ma0.3Omegax12.4.ff2m');
%%

bf=SF_BaseFlow(bf,'Re',100,'Mach',Ma,'Omegax',0.0,'type','NEW');

alpha=[0:0.1:15];

ResS_tab = nan*ones(size(alpha)); 
ResS_tab(1) = 47;
Re_RangeS = [46.5:1.0:51.5];
omegaS_tab = nan*ones(size(alpha));
omegaS_tab(1) = 0.72;
omegasSguess = 0.72;
k_Range = linspace(0,1,11);
kS_tab = nan*ones(size(alpha));
status_tab = nan*ones(size(alpha));
iter = linspace(5,18,14);

for i=126:length(alpha)
    % Update parameter
    Omegax = alpha(i);
    % Update mesh
    if(Omegax < 1.5 && Omegax ~= 0.0)
        ffmesh = importFFmesh(['WORK/MESHES/mesh_adapt',num2str(iter(uint16(Omegax*10))),'_Re100_Ma0.05_Omegax',num2str(Omegax),'.msh']);
        bf = importFFdata(ffmesh,['WORK/MESHES/BaseFlow_adapt',num2str(iter(uint16(Omegax*10))),'_Re100_Ma0.05_Omegax',num2str(Omegax),'.ff2m']);
    end
    % Compute threshold
    [bf,EVS,ResS,omegasS,KOut,status] = SF_Stability_LoopBisRe(bf,Re_RangeS,k_Range,omegasSguess*1i,'Mach',Ma,...
                                            'Omegax',Omegax);
    % Save current values
    ResS_tab(i) = ResS;
    omegaS_tab(i) = omegasS;
    kS_tab(i) = KOut;
    status_tab(i) = status;
    if(i<2) % guess = start value
        ResSguess = ResS_tab(1); omegasSguess = omegaS_tab(1); kGuess = kS_tab(1);
        Re_RangeS = [ResSguess*0.95:ResSguess*.05:1.25*ResSguess];
    elseif(i<length(alpha)) % guess = extrapolation using two previous points
        ResSguess = interp1(alpha(1:i),ResS_tab(1:i),alpha(i+1),'spline','extrap');
        Re_RangeS = [ResSguess*1.1:-ResSguess*0.05:0.95*ResSguess];
        % omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),alpha(i+1),'spline','extrap')
        omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),13.4,'spline','extrap');
        kGuess = interp1(alpha(1:i),kS_tab(1:i),alpha(i+1),'spline','extrap');
    end
    %k_Range = [kGuess*0.9:(kGuess*.2+0.1)/4.0:kGuess*1.1+0.1];
    k_Range = [0.0];
end



%% Small computation for Re = 174.6 and alpha=12.9 omega=2.0562i
% This is done after the computation of many points of the curve
% We perform an extrapolation and we want to verify that the prediction is
% good
i = 125
ResSguess = interp1(alpha(1:i),ResS_tab(1:i),12.9,'spline','extrap');
omegasSguess = interp1(alpha(1:i),omegaS_tab(1:i),12.9,'spline','extrap');

bf=SF_BaseFlow(bf,'Re',ResSguess,'Mach',Ma,'Omegax',12.9,'type','NEW');
[evNCM,em] = SF_Stability(bf,'k',0.0,'shift',omegasSguess*1i,'nev',5);% to initiate the cont mode

bf2=SF_BaseFlow(bf,'Re',ResSguess*0.975,'Mach',Ma,'Omegax',12.9,'type','NEW');
[evNCM2,em2] = SF_Stability(bf2,'k',0.0,'shift',omegasSguess*1i,'nev',5);% to initiate the cont mode

Rec = interp1([real(evNCM2(1)),real(evNCM(1))],[ResSguess*0.975,ResSguess],0,'spline','extrap');
omegac = interp1([real(evNCM2(1)),real(evNCM(1))],[imag(evNCM2(1)),imag(evNCM(1))],0,'spline','extrap');

% Second one i = 135
i = 125;
ResSguess = interp1([alpha(1:i),alpha(130:131)],[ResS_tab(1:i),ResS_tab(130:131)],13.0,'spline','extrap');
omegasSguess = interp1([alpha(1:i),alpha(130:131)],[omegaS_tab(1:i),omegaS_tab(130:131)],13.0,'spline','extrap');

bf=SF_BaseFlow(bf,'Re',ResSguess,'Mach',Ma,'Omegax',13.1,'type','NEW');
[evNCM,em] = SF_Stability(bf,'k',0.0,'shift',omegasSguess*1i,'nev',5);% to initiate the cont mode

bf2=SF_BaseFlow(bf,'Re',ResSguess*1.025,'Mach',Ma,'Omegax',13.0,'type','NEW');
[evNCM2,em2] = SF_Stability(bf2,'k',0.0,'shift',omegasSguess*1i,'nev',5);% to initiate the cont mode

Rec = interp1([real(evNCM2(1)),real(evNCM(1))],[ResSguess*1.025,ResSguess],0,'spline','extrap');
omegac = interp1([real(evNCM2(1)),real(evNCM(1))],[imag(evNCM2(1)),imag(evNCM(1))],0,'spline','extrap');

% third alpha = 13.6
ResSguess = interp1([alpha(1:i),alpha(130:131),13.6],[ResS_tab(1:i),ResS_tab(130:131),ResS_tab(137)],14.4,'spline','extrap');
omegasSguess = interp1([alpha(1:i),alpha(130:131),13.6],[omegaS_tab(1:i),omegaS_tab(130:131),omegaS_tab(137)],14.4,'spline','extrap');

bf=SF_BaseFlow(bf,'Re',ResSguess,'Mach',Ma,'Omegax',14.4,'type','NEW');
[evNCM,em] = SF_Stability(bf,'k',0.0,'shift',omegasSguess*1i,'nev',5);% to initiate the cont mode

bf2=SF_BaseFlow(bf,'Re',ResSguess*0.975,'Mach',Ma,'Omegax',14.4,'type','NEW');
[evNCM2,em2] = SF_Stability(bf2,'k',0.0,'shift',evNCM(3),'nev',5);% to initiate the cont mode

Rec = interp1([real(evNCM2(2)),real(evNCM(3))],[ResSguess*0.975,ResSguess],0,'spline','extrap');
omegac = interp1([real(evNCM2(2)),real(evNCM(3))],[imag(evNCM2(2)),imag(evNCM(3))],0,'spline','extrap');


%% Test of critical Reynolds with Ma = 0.2 and alpha = 0
mesh.xinfv = 80;
mesh.xinfm = 40;
mesh.yinf = 40;
Params =[mesh.xinfv*0.5 mesh.xinfm*0.5 10000000 mesh.xinfv*0.6 -0.3 mesh.yinf*0.5 10000000 mesh.yinf*0.6 -0.3]; % x0, x1, LA, LC, gammac, y0, LAy, LCy, gammacy
ffmesh = SF_SetMapping(ffmesh,'MappingType','box','MappingParams',Params);

Ma = 0.5;
bf=SF_BaseFlow(bf,'Re',ResS_tab(31),'Mach',Ma,'Omegax',alpha(31),'type','NEW');
[evNCM,em] = SF_Stability(bf,'k',0.0,'shift',omegaS_tab(57)*1i,'nev',1);% to initiate the cont mode
[evNCM1,em] = SF_Stability(bf,'k',0.1,'shift',omegaS_tab(57)*1i,'nev',1);% to initiate the cont mode
[evNCM2,em] = SF_Stability(bf,'k',0.2,'shift',omegaS_tab(31)*1i,'nev',1);% to initiate the cont mode
[evNCM3,em] = SF_Stability(bf,'k',0.3,'shift',omegaS_tab(31)*1i,'nev',1);% to initiate the cont mode
[evNCM4,em] = SF_Stability(bf,'k',1.0,'shift',omegaS_tab(31)*1i,'nev',10);% to initiate the cont mode





