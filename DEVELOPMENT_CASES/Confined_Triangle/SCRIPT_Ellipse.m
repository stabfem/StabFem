
%% Flow around a triangular body in a chanel
%
% We compare several solvers for a quasi-static displacement and
% a harmonically imposed displacement.


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);
SF_core_arborescence('cleanall');
set(0,'defaultAxesFontSize',18);

%% Chapter 1 : Base flow (symmetrical) for Re = 25 ; initial mesh

    ffmesh = SF_Mesh('Mesh_ConfinedEllipse.edp','Params',[0 -5 7],'problemtype','2D');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_BaseFlow(bf,'Re',50);
    bf = SF_BaseFlow(bf,'Re',100);
    
  
%% 
% a few plots
figure; subplot(2,1,1); SF_Plot(bf,'mesh','xlim',[-1.2 2]);
subplot(2,1,2); SF_Plot(bf,'ux','ColorMap','jet','xlim',[-1.2 2]);
hold on; SF_Plot(bf,'psi','contour','only');


%% QS calculations 

omega = 1e-10;
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y') 

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')

%% Comparing structures

figure; subplot(2,1,1); SF_Plot(ffALE,'uya','xlim',[-1 2]);
subplot(2,1,2);SF_Plot(ffAA,'uy','xlim',[-1 2]);


%% Computing a second base flow with offset
Yrange = [-.02,-.01, 0.01, 0.02];
    for i=1:length(Yrange)
      Y = Yrange(i);     
      ffmesh = SF_Mesh('Mesh_ConfinedEllipse.edp','Params',[Y -5 7],'problemtype','2D');
      bf01 = SF_BaseFlow(ffmesh,'Re',1);
      bf01 = SF_BaseFlow(bf01,'Re',25);
      bf01 = SF_BaseFlow(bf01,'Re',50); 
      bf01 = SF_BaseFlow(bf01,'Re',100); 
      Fytab(i) = bf01.Fy;
    end
    
%%
 figure;   plot(Yrange,Fytab,'rs',Yrange,bf.Fy+ffALE.Fy*Yrange,'b--',...
   Yrange,bf.Fy+ffRR.Fy*Yrange,'r--',Yrange,bf.Fy+ffAA.Fy*Yrange,'k--',Yrange,bf.Fy+(ffAA.Fy-ffAA.FyAS)*Yrange,'m--');
 xlabel('Y');ylabel('Fy');   
 legend('Offset','ALE','REL','ABS', 'ABS (no AS)');  


%% SUMMARY
SF_Status


% [[PUBLISH]]

