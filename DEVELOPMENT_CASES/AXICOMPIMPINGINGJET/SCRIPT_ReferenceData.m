%% Input gas
data_air.Rg = 287;
data_air.gamma = 1.4;
data_air.mu = 1.8e-5;
%% Reference values at infinity
Xinf = -100; Yinf = 0;
rho_inf = SF_ExtractData(bf,'rho',Xinf,Yinf);
ux_inf = SF_ExtractData(bf,'ux',Xinf,Yinf);
T_inf = SF_ExtractData(bf,'T',Xinf,Yinf);
p_hydro_inf = SF_ExtractData(bf,'p',Xinf,Yinf);

data_inf.rho_avg = rho_inf;
data_inf.ux_avg = ux_inf;
data_inf.T_avg = T_inf;
data_inf.p_avg = p_hydro_inf;
data_inf.z = Xinf;
data_inf.r = Yinf;


%% Values at the inlet of the pipe
X0 = -5.11; Y0 = [0.0:0.001:0.5];
rho0 = SF_ExtractData(bf,'rho',X0,Y0);
ux0 = SF_ExtractData(bf,'ux',X0,Y0);
T0 = SF_ExtractData(bf,'T',X0,Y0);
p0 = SF_ExtractData(bf,'p',X0,Y0);
m0_avg = trapz(Y0,Y0.*ux0.*rho0)./trapz(Y0,Y0);

rho0_avg = trapz(Y0.*rho0)./trapz(Y0);
ux0_avg = trapz(Y0.*ux0)./trapz(Y0);
T0_avg = trapz(Y0.*T0)./trapz(Y0);
p0_avg = trapz(Y0.*p0)./trapz(Y0);
data_start.rho_avg = rho0_avg;
data_start.ux_avg = ux0_avg;
data_start.T_avg = T0_avg;
data_start.p_avg = p0_avg;
data_start.m_avg = m0_avg;
data_start.z = X0;
data_start.r = Y0;


%% Values at the end of the pipe
X0 = -0.2; Y0 = [0.0:0.001:0.5];
rho0 = SF_ExtractData(bf,'rho',X0,Y0);
ux0 = SF_ExtractData(bf,'ux',X0,Y0);
T0 = SF_ExtractData(bf,'T',X0,Y0);
p0 = SF_ExtractData(bf,'p',X0,Y0);

rho0_avg = trapz(Y0.*rho0)./trapz(Y0);
ux0_avg = trapz(Y0.*ux0)./trapz(Y0);
T0_avg = trapz(Y0.*T0)./trapz(Y0);
p0_avg = trapz(Y0.*p0)./trapz(Y0);
m0_avg = trapz(Y0.*ux0.*rho0)./trapz(Y0);
data_end.rho_avg = rho0_avg;
data_end.ux_avg = ux0_avg;
data_end.T_avg = T0_avg;
data_end.p_avg = p0_avg;
data_end.m_avg = m0_avg;
data_end.z = X0;
data_end.r = Y0;

data.L = 5.1;
data.D = 1;

p_static_start = 1+data_air.gamma*bf.Ma^2*(data_start.p_avg);
p_total_start = p_static_start*(1+(data_air.gamma-1)/2*data_z0.Ma^2)^(  data_air.gamma/(data_air.gamma-1) )


p_static_end = 1+data_air.gamma*bf.Ma^2*(data_end.p_avg);
p_total_end = p_static_end*(1+(data_air.gamma-1)/2*data_zL.Ma^2)^(  data_air.gamma/(data_air.gamma-1) )


p_total_end/p_total_start

(p_total_end-p_total_start)/(0.5*data_end.rho_avg*data_end.ux_avg^2)

%% Reference dimensionless quantities
% Impose Temperature at infinity
D = 0.01;
T_inf = 288; T_ref = T_inf/data_inf.T_avg;
% Compute U_ref from Ma_ref
M_ref = bf.Ma;
U_ref = M_ref*sqrt(data_air.Rg*data_air.gamma*T_ref);
% Compute rho_ref from Re_ref
Re_ref = bf.Re;
rho_ref = Re_ref*data_air.mu/(U_ref*D);
p_thermo_ref = data_air.Rg*rho_ref*T_ref - data_inf.p_avg*data_ref.rho*data_ref.U^2;
data_ref.p_thermo = p_thermo_ref;
data_ref.p = data_air.Rg*rho_ref*T_ref;
data_ref.rho = rho_ref;
data_ref.T = T_ref;
data_ref.U = U_ref;
data_ref.L = D;
data_ref.Re = Re_ref;
data_ref.Ma = M_ref;
data_ref.p_hydro_wrt_p_thermo = (data_ref.rho*data_ref.U^2/data_ref.p)
data_ref.K = data_ref.p_thermo/data_ref.p;
%% Dimensionless quantities at locations
% inlet of the pipe
data_z0.Ma = data_ref.Ma*(data_start.ux_avg/sqrt(data_start.T_avg));
data_z0.Re = data_ref.Re*(data_start.m_avg);
data_z0.Kn = sqrt(data_air.gamma*pi/2)*data_z0.Ma/data_z0.Re;
% End of the pipe
data_zL.Ma = data_ref.Ma*(data_end.ux_avg/sqrt(data_end.T_avg));
data_zL.Re = data_ref.Re*(data_end.m_avg);
data_zL.Kn = sqrt(data_air.gamma*pi/2)*data_zL.Ma/data_zL.Re;

%% Isentropic expansion
ratio_rho_total = (1+(data_air.gamma-1)/2*data_zL.Ma^2)^(1/(data_air.gamma-1));
ratio_p_total = (1+(data_air.gamma-1)/2*data_zL.Ma^2)^(data_air.gamma/(data_air.gamma-1));
ratio_T_total = (1+(data_air.gamma-1)/2*data_zL.Ma^2);

data_ref.p + data_air.gamma*data_ref.Ma^2*data_inf.p*(data_ref.rho*data_ref.U^2)

static_pressure_farfield = 1 + data_air.gamma*data_ref.Ma^2*data_inf.p_avg*(data_ref.rho*data_ref.U^2/data_ref.p);
static_pressure_z0 = 1 + data_air.gamma*data_ref.Ma^2*data_start.p_avg*(data_ref.rho*data_ref.U^2/data_ref.p);

sim_ratio_T = data_inf.T_avg/data_start.T_avg
sim_ratio_p = static_pressure_farfield/static_pressure_z0
sim_ratio_rho = data_inf.rho_avg/data_start.rho_avg

% isentropic efficiency
eta_Process = (data_air.gamma-1)/data_air.gamma*log(ratio_p_total)/log(sim_ratio_T);

% Isentropic Mach number
M_isentropic_T = sqrt(2/(data_air.gamma-1)*(sim_ratio_T-1));
M_isentropic_p = sqrt(2/(data_air.gamma-1)*(sim_ratio_p^( (data_air.gamma-1)/(data_air.gamma))-1))
M_isentropic_rho = sqrt(2/(data_air.gamma-1)*(sim_ratio_rho^(data_air.gamma-1)-1))

sim_ratio_rho/ratio_rho_total
sim_ratio_p/ratio_p_total
sim_ratio_T/ratio_T_total