%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',8,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%%
sfs=SF_Status();
bf0 = SF_Load('BASEFLOWS',289);
%%
MachList = [0.0:0.025:0.6];
% Save critical Reynolds
RecList_5 = zeros(size(MachList));
RecList_4 = zeros(size(MachList));
RecList_3 = zeros(size(MachList));
RecList_2 = zeros(size(MachList));
% Save critical frequency
omegacList_5 = zeros(size(MachList));
omegacList_4 = zeros(size(MachList));
omegacList_3 = zeros(size(MachList));
omegacList_2 = zeros(size(MachList));
%%
Re = 1322; Ma = 0.5; ncores = 6;
bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores);
shift = 0.0071935+5.134i;
[ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'type','D','m',0,'ncores',6);
RecList_4(21) = 1319;
omegacList_4(21) = imag(ev(1));
RecList_4(22) = 1286;
omegacList_4(22) = imag(ev(1));
%%Save
results.RecList_2=RecList_2;
results.RecList_3=RecList_3;
results.RecList_4=RecList_4;
results.RecList_5=RecList_5;
results.omegacList_2=omegacList_2;
results.omegacList_3=omegacList_3;
results.omegacList_4=omegacList_4;
results.omegacList_5=omegacList_5;

save('DataResults.mat','results');
%%

for index = 10:-1:1
    % Initialise stockage 
    EV_iter = [];
    Re_iter = [];
    omega_iter = [];
    % Guess
    Rec_guess = interp1(results.MachList(index+1:end-1), results.RecList_4(index+1:end-1), results.MachList(index),'linear','extrap');
    omega_guess = interp1(results.MachList(index+1:end-1), results.omegacList_4(index+1:end-1), results.MachList(index),'linear','extrap');
    stepReMax = 30; stepReMin = 10;
    % First computation
    Re0 = Rec_guess+stepReMax; Ma = results.MachList(index); ncores = 6;
    bf=SF_BaseFlow(bf,'Re',Re0,'Mach',Ma,'type','NEW','ncores',ncores);
    shift = 0.2+omega_guess*1i;
    [ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'type','D','m',0,'ncores',6);
    % Unstable? --> Save
    if(real(ev) > 0)
        EV_iter = [EV_iter, real(ev)];
        Re_iter = [Re_iter, Re0];
        omega_iter = [omega_iter, imag(ev(1))];
        shift = 0.3+imag(ev(1))*1i;
    end
    % Iteration negative eig
    Re = Re0;
    while(real(ev) < 0)
        Re = Re+stepReMax;
        bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores);
        shift = 0.2+results.omegacList_4(index+1)*1i;
        [ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'type','D','m',0,'ncores',6);
    end
    % Iteration positive eig
    while(real(ev) > 0)
        Re = Re-stepReMin;
        bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores);
        [ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'type','D','m',0,'ncores',6);
        shift = 0.2+imag(ev(1))*1i;
        omega_iter = [omega_iter, imag(ev(1))];
        EV_iter = [EV_iter, real(ev)];
        Re_iter = [Re_iter, Re];
    end
    
    Rec = interp1(EV_iter, Re_iter, 0.0,'linear','extrap');
    omega_result = interp1(EV_iter, omega_iter, 0.0,'linear','extrap');
    
    results.RecList_4(index) = Rec;
    results.omegacList_4(index) = omega_result;
end

Re = 1670;
bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores);
%[ev5,em5] = SF_Stability(bf,'shift',0.3+3.55i,'nev',1,'type','D','m',0,'ncores',6);
[ev6,em6] = SF_Stability(bf,'shift',0.1+4.26i,'nev',1,'type','D','m',0,'ncores',6);



% bf0=SF_BaseFlow(bf0,'Re',1300,'Mach',0.2,'type','NEW','ncores',ncores);
% bf0=SF_BaseFlow(bf0,'Re',1400,'Mach',0.2,'type','NEW','ncores',ncores);
% bf0=SF_BaseFlow(bf0,'Re',1500,'Mach',0.2,'type','NEW','ncores',ncores);
% bf0=SF_BaseFlow(bf0,'Re',1600,'Mach',0.2,'type','NEW','ncores',ncores);
% bf0=SF_BaseFlow(bf0,'Re',1700,'Mach',0.2,'type','NEW','ncores',ncores);
% 
% bf0=SF_BaseFlow(bf0,'Re',1440,'Mach',0.1,'type','NEW','ncores',ncores);
% [ev,em] = SF_Stability(bf0,'shift',0.2+5.3i,'nev',1,'type','D','m',0,'ncores',6);
% 
% [ev1,em1] = SF_Stability(bf0,'shift',0.2+4.8i,'nev',1,'type','D','m',0,'ncores',6);
% [ev2,em2] = SF_Stability(bf0,'shift',0.2+4.6i,'nev',1,'type','D','m',0,'ncores',6);
% [ev3,em3] = SF_Stability(bf0,'shift',0.2+4.4i,'nev',1,'type','D','m',0,'ncores',6);
% [ev4,em4] = SF_Stability(bf0,'shift',0.2+4.2i,'nev',1,'type','D','m',0,'ncores',6);
% [ev5,em5] = SF_Stability(bf0,'shift',0.2e+4.0i,'nev',1,'type','D','m',0,'ncores',6);
% [ev6,em6] = SF_Stability(bf0,'shift',0.2+3.8i,'nev',1,'type','D','m',0,'ncores',6);
% [ev7,em7] = SF_Stability(bf0,'shift',0.2+3.6i,'nev',1,'type','D','m',0,'ncores',6);
% [ev8,em8] = SF_Stability(bf0,'shift',0.2+3.4i,'nev',1,'type','D','m',0,'ncores',6);
% [ev9,em9] = SF_Stability(bf0,'shift',0.2+3.2i,'nev',1,'type','D','m',0,'ncores',6);
