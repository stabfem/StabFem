%%  Instability of a impinging rounded jet
%
% 
% THIS SCRIPT GENERATES PLOTS FOR THE FORCED STRUCTURES AND THE
% EIGENMODES FOR THE IMPINGING ROUNDED JET
% 
% REFEFENCE : Giannetti & Sierra 
%

%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',8,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%% Create an initial mesh
ffmesh = SF_Mesh('Mesh_rounded.edp','problemtype','axicomp','cleanworkdir','no');
%% Generation of a initial baseflow
Ma = 0.05; ncores = 6; Re = 50;
bf=SF_BaseFlow(ffmesh,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores,'Options','-imaxNewton 500 ');
%% Generation of a baseflow at Re=1600
Ma = 0.3; ncores = 6; ReList = [50:50:1600];
for Re=ReList
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW','ncores',ncores);
end
%% Plot
figure;
SF_Plot(bf,'ux','xlim',[-5,5],'ylim',[0,10],'Boundary','on',...
    'BDLabels',[2,21],'BDColors','k');

%% Eigenvalue problem
[ev,em] = SF_Stability(bf,'shift',0.25+4.9i,'nev',5,'type','D','m',0,'ncores',6);
%%

bf0=SF_Load('BASEFLOWS',39);
bfSt{1} = bf0;

bf = SF_Adapt(bf0,'anisomax',1,'Hmax',1);
bf=SF_BaseFlow(bf0,'Re',bf0.Re,'Mach',bf0.Ma,'type','NEW','ncores',ncores);

bf=SF_BaseFlow(bf,'Re',bf0.Re,'Mach',0.6,'type','NEW','ncores',ncores,'Options','-viscosityTemperature 1 -tolNewton 1e-8');
bf=SF_BaseFlow(bf,'Re',bf0.Re,'Mach',0.65,'type','NEW','ncores',ncores,'Options','-viscosityTemperature 1 -tolNewton 1e-8');
bf=SF_BaseFlow(bf,'Re',bf0.Re,'Mach',0.675,'type','NEW','ncores',ncores,'Options','-viscosityTemperature 1 -tolNewton 1e-8');


