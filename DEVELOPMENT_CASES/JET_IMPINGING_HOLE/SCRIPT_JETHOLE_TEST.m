%%  JET FLOW IMPACTING ON A PERFORATED PLATE
%
% This is a first test to compute baseflow and impedance using StabFem


%% Chapter 0 : Initialization
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4);
SF_DataBase('open','./TEST/');
close all;
postprocessonly = true;

if ~postprocessonly
    %% Chapter 1 : mesh/bf
    ffmesh = SF_Mesh('Mesh.edp'); % initial mesh
    
    % create bf for very slow Re and use continuation to raise Re
    bf = SF_BaseFlow(ffmesh,'solver','Newton_Axi.edp','Re',10);
    bf = SF_Adapt(bf,'Hmax',0.5);
    bf = SF_BaseFlow(bf,'Re',30);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_Adapt(bf,'Hmax',0.5);
    bf = SF_BaseFlow(bf,'Re',300);
    bf = SF_BaseFlow(bf,'Re',600);
    bf = SF_Adapt(bf,'Hmax',0.5);
    bf = SF_BaseFlow(bf,'Re',700);
    bf = SF_BaseFlow(bf,'Re',1000);
    bf = SF_Adapt(bf,'Hmax',0.5);
    bf = SF_BaseFlow(bf,'Re',1000);
    
    %% adapt to forced flow and recompute
    Params = [5 27 1.25 .5 5 17]; % choice of parameters to use complex mapping + stretching
    foA = SF_LinearForced(bf,3,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
    bf = SF_Adapt(bf,foA,'Hmax',0.5);
    bf = SF_BaseFlow(bf,'Re',1000);
    
else
    bf = SF_Load('BASEFLOWS',8);
end

%% plot BF
figure; ax1 = axes('Position',[0.1 0.5 0.8 0.4]);
SF_Plot(bf,'ux','boundary','on','colorbar','eastoutside');
text(35,10,'$u_x$','FontSize',20,'interpreter','latex');
ax2 = axes('Position',[0.1 0.13 0.8 0.4]);
SF_Plot(bf,'vort','symmetry','xm','colorrange',[-3 3],'boundary','on','colormap','redblue','colorbar','eastoutside');
text(35,-5,'$\omega_\varphi$','FontSize',20,'interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600; pos(4)=pos(3)*.7;set(gcf,'Position',pos);

%% zoom around hole
figure;
ax1 = axes('Position',[0.1 0.5 0.8 0.4]);
SF_Plot(bf,'ux','boundary','on','xlim',[-6 1],'ylim',[0 3]);
text(2.3,2,'$u_x$','FontSize',20,'interpreter','latex');
ax2 = axes('Position',[0.1 0.13 0.8 0.4]);
SF_Plot(bf,'vort','symmetry','xm','colorrange',[-3 3],'boundary','on','colormap','redblue','xlim',[-6 1],'ylim',[-3 0]);
text(2.3,-1,'$\omega_\varphi$','FontSize',20,'interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600; pos(4)=pos(3)*.7;set(gcf,'Position',pos);



%% Loop for Impedance (Re = 1000)
if ~postprocessonly
    Params = [1 27 1. .3 5 17];
    omegarange = [0:0.05:5];
    fo = SF_LinearForced(bf,omegarange,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
    
    %% Loop for Impedance (Re = 1500)
    bf = SF_BaseFlow(bf,'Re',1500);
    fo2 = SF_LinearForced(bf,omegarange,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
    
    %% Loop for Impedance (Re = 1750)
    bf = SF_BaseFlow(bf,'Re',1750);
    fo3 = SF_LinearForced(bf,omegarange,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
    
else
    fo = SF_Load('IMPEDANCES',1);
    fo2 = SF_Load('IMPEDANCES',2);
    fo3 = SF_Load('IMPEDANCES',3);
end

%% Plot
figure(12); theplotsymbol='b';
plot(fo.omega,real(fo.Z),[theplotsymbol,'-'],fo.omega,-imag(fo.Z)./fo.omega,[theplotsymbol,'--']);hold on;
figure(13);
plot(real(fo.Z),imag(fo.Z),[theplotsymbol,'-']); %title(['Nyquist diagram for Re = ',num2str(bf.Re)] );

figure(12); hold on; theplotsymbol='r';
plot(fo2.omega,real(fo2.Z),[theplotsymbol,'-'],fo.omega,-imag(fo2.Z)./fo.omega,[theplotsymbol,'--']);hold on;
figure(13);hold on;
plot(real(fo2.Z),imag(fo2.Z),[theplotsymbol,'-']);

figure(12); hold on; theplotsymbol='g';
plot(fo3.omega,real(fo3.Z),[theplotsymbol,'-'],fo3.omega,-imag(fo3.Z)./fo3.omega,[theplotsymbol,'--']);hold on;
figure(13);hold on;
plot(real(fo3.Z),imag(fo3.Z),[theplotsymbol,'-']);


figure(12);title('Impedance ; Re (-) and Im (--) parts')
ylim([-.15;.25]);plot(fo.omega,0*real(fo.Z),'k:','LineWidth',1);
legend('Re=1000','','Re=1500','','Re=1750','','');
xlabel('\omega');ylabel('Z_r, -Z_i/\omega');

figure(13);title('Nyquist curves')
xlim([-.15;.25]);ylim([-.6;.2]);
plot([-1e10 1e10],[0,0],'k:','LineWidth',1);plot([0,0],[-1e10 1e10],'k:','LineWidth',1);
legend('Re=1000','Re=1500','Re=1750');
xlabel('Z_r');ylabel('Z_i');



%% Compute structure for Re = 1750, omega = 2. (corresponding to pure hydro instability)
if ~postprocessonly
    Params = [2 27 1.25 .3 5 17]; % choice of parameters to use complex mapping + stretching
    omega = 2.;
    foA = SF_LinearForced(bf,omega,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
else
    foA = SF_Load('FORCEDFLOWS','last');
end
%% Plot
figure; ax1 = axes('Position',[0.1 0.5 0.8 0.4]);
SF_Plot(foA,'p','colorrange',[-2 2],'boundary','on','colorbar','eastoutside');
text(35,10,'$p$','FontSize',20,'interpreter','latex');
ax2 = axes('Position',[0.1 0.13 0.8 0.4]);
SF_Plot(foA,'vort','symmetry','xm','colorrange',[-3 3],'boundary','on','colormap','redblue','colorbar','eastoutside');
text(35,-5,'$\omega_\varphi$','FontSize',20,'interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600; pos(4)=pos(3)*.7;set(gcf,'Position',pos);



%% zoom around hole
figure;
ax1 = axes('Position',[0.1 0.5 0.8 0.4]);
SF_Plot(foA,'p','colorrange',[-2 2],'boundary','on','xlim',[-6 1],'ylim',[0 3]);
text(2.3,2,'$p$','FontSize',20,'interpreter','latex');
ax2 = axes('Position',[0.1 0.13 0.8 0.4]);
SF_Plot(foA,'vort','symmetry','xm','colorrange',[-3 3],'boundary','on','colormap','redblue','xlim',[-6 1],'ylim',[-3 0]);
text(2.3,-1,'$\omega_\varphi$','FontSize',20,'interpreter','latex');
pos = get(gcf,'Position'); pos(3) = 600; pos(4)=pos(3)*.7;set(gcf,'Position',pos);
