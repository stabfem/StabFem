%% Impedance of a constriction in a long pipe
%
% This program is a first test to compute the impedance of a constriction in a pipe.
% For discussion with Lionel et al.
% 
% [[PUBLISH]] on stabfem website by DF, 21/01/2022

%% Initialization of StabFem
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('open','./WORK_Test');
%SF_core_setopt('PlotBFOptions',{'vort','xlim',[-.5 2],'ylim',[-1 1],'colorrange',[-10 10],'colormap','redblue'}); 
%SF_core_setopt('PlotModeOptions',{'p','xlim',[-.5 1.5],'ylim',[-.5 .5],'colorrange','cropcenter','colormap','redblue','logsat',1});
%SF_core_setopt('BFSolver','Newton_Axi.edp');
%SF_core_setopt('StabSolver','Stab_Axi.edp')


%% generating initial mesh

MP.Rh = 1; MP.Lh = 2; MP.Lext = 300;MP.Rext=5;MP.Lcav=20;MP.Rcav = 5;
ffmesh = SF_Mesh('Mesh_OneHole_Pipe.edp','Options',MP);
figure; SF_Plot(ffmesh);

%% computing baseflow and adapting mesh

bf = SF_BaseFlow(ffmesh,'solver','Newton_Axi.edp','Re',100); % first time use mesh  and must specify solver
bf = SF_Adapt(bf,'Hmax',0.5);
bf = SF_BaseFlow(bf,'Re',100); % next times initialize with previous one

bf = SF_BaseFlow(bf,'Re',200); 
bf = SF_Adapt(bf,'Hmax',1);

bf = SF_BaseFlow(bf,'Re',400);
bf = SF_Adapt(bf,'Hmax',1);

bf = SF_BaseFlow(bf,'Re',600);
bf = SF_Adapt(bf,'Hmax',1);

%bf = SF_BaseFlow(bf,'Re',700);
%bf = SF_Adapt(bf,'Hmax',1);

% eventually reaching 800
bf = SF_BaseFlow(bf,'Re',800);
% Last adaptation uses mask
Mask = SF_Mask(bf,[-4 10 0 5 0.05]);
bf = SF_Adapt(bf,Mask,'Hmax',1,'anisomax',3); % final adaptation with mask
bf = SF_BaseFlow(bf,'Re',800);% recompute on final mesh


%% Plot the baseflow for Re = 800
figure; title('several views of the base flow ');
subplot(2,3,[1 2,3]);SF_Plot(bf,'ux','colormap','ice','xlim',[-(MP.Lcav+MP.Lh),MP.Lext]);
hold on;SF_Plot(bf,'ux','contour','only','clevels',[0,0],'xlim',[-(MP.Lcav+MP.Lh),MP.Lext],'ylim',[0 5]);
%subplot(3,3,[4 5 6]);SF_Plot(bf,'mesh');
subplot(2,3,4);SF_Plot(bf,'ux','xlim',[-3 3],'ylim',[0 5],'colormap','ice');
hold on;SF_Plot(bf,'ux','contour','only','clevels',[0,0],'xlim',[-3 3],'ylim',[0 5]);
%hold on;SF_Plot(bf,'psi','contour','only','xlim',[-3 3],'ylim',[0 5]);
subplot(2,3,5);SF_Plot(bf,'ux','xlim',[MP.Lext-5,MP.Lext],'ylim',[0 5],'colormap','ice');
hold on;SF_Plot(bf,'ux','contour','only','clevels',[0,0],'xlim',[MP.Lext-5,MP.Lext],'ylim',[0 5]);
subplot(2,3,6);
yline = [0:.01:5];probe=SF_ExtractData(bf,MP.Lext-5,yline);plot(probe.ux,yline);xlabel('ux(L-5,y)');ylabel('y');
%hold on;SF_Plot(bf,'psi','contour','only','xlim',[MP.Lext-5,MP.Lext],'ylim',[0 5]);


%% simpler view
SF_Plot(bf,'ux','xlim',[-2 4],'ylim',[0 5],'colormap','ice');
hold on;SF_Plot(bf,'ux','contour','only','clevels',[0,0],'xlim',[-3 3],'ylim',[0 5]);

%% Testing impedance without complex mapping


%% Figure for one mode
omega = 2;
foA = SF_LinearForced(bf,omega,'solver','LinearForcedAxi_m0.edp');
%%
figure;
SF_Plot(foA,'vort','boundary','on','colormap','redblue','colorrange','cropcentered','xlim',[-3 10],'ylim',[0 5],...
            'boundary','on','bdlabels',2,'bdcolors','k','cbtitle','\omega''_r','logsat',1,'mesh','off');

%% loop on omega
omegatab = [0 :.1: 5];
loopA = SF_LinearForced(bf,omegatab,'solver','LinearForcedAxi_m0.edp');
%%
figure; plot(loopA.omega,real(loopA.Z),'r-',loopA.omega,-imag(loopA.Z)./loopA.omega,'r--');
xlabel('\omega');ylabel('Z');legend('Z_R','-Z_I/\omega','location','east');grid on
        
%% Now testing impedance with complex mapping

%Params = [5 1e300 1. .5 5 1e30]; % choice of parameters to use complex mapping + stretching
%omega = 1;
%foA = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

%% WARNING COMPLEX MAPPING IS MANDATORY ABOVE RE = 2000 ! 