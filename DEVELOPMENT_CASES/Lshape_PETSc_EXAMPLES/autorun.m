function value = autorun(verbosity)

% Autorun function for StabFem. 
% This function will produce sample results for the case EXAMPLE_Lshape
%
% USAGE : 
% autorun -> automatic check
% autorun(1) -> produces the figures used for the manual



 isfigures=0;
 
if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');

value=0;



% Generation of the mesh
Ndensity =40;
ffmesh=SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity});


disp('##### autorun test 1 : Steady temperature at a given point');

[heatS,Partition] = SF_Launch('Lshape_Steady_parallel.edp','Mesh',ffmesh,'ncores',2,'Datafile',{'Data.txt','Partition.txt'});

% importation and plotting of data not associated to a mesh : temperature along a line
Ycut = 0.25;Xcut = [0:.01:1];
heatCut = SF_ExtractData(heatS,'T',Xcut,Ycut);

critere1 = heatCut(50);
critere1REF = 0.034019400000000;

SFerror(1) = abs(critere1/critere1REF-1)

disp('##### autorun test 2 : NS flow ');

Flow = SF_Launch('Lshape_NavierStokes_parallel.edp','Mesh',ffmesh,'ncores',2)
Flow.Fx
FxRef = Flow.Fx

SFerror(2) = abs(Flow.Fx/FxRef-1)


value = sum(SFerror>1e-2)

end

%[[AUTORUN:SHORT]]
