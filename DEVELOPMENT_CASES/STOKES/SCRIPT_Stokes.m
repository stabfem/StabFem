%%  Instability and impedance of a jet a single hole configuration
%   Compressible setting
%  
% REFEFENCE: JFM -- Acoustic instability prediction of the 
% flow through a circular aperture in a thick platevia an 
% impedance criterion (2021)
%
%

%% Chapter 0 : Initialization of StabFem Drivers
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%%
ffmesh = SF_Mesh('Mesh.edp','Params',[1 40 5],'problemtype','2dstokes');
bf0 = SF_BaseFlow(ffmesh,'Re',1);
%MaskThin = SF_Mask(bf.mesh,[0.0,5.0,-1e-2,1e-3,2e-5]);

% MaskLeft = SF_Mask(bf0.mesh,[0.44,0.46,-1e-2,1e-2,5e-5]);
% MaskRight = SF_Mask(bf0.mesh,[0.54,0.56,-1e-2,1e-2,5e-5]);
% MaskCenter = SF_Mask(bf0.mesh,[0.3,0.7,-1e-2,1e-2,1e-4]);
% MaskFull = SF_Mask(bf0.mesh,[0.0,1.0,-1e-2,1e-2,5e-4]);
% 
% bfM = SF_Adapt(bf0,MaskLeft,MaskRight,MaskCenter,MaskFull,'Hmax',0.02,'anisomax',1.0,'Nbvx',3e5);
% bf0 = SF_BaseFlow(bfM.mesh,'Re',1);

%%
L=1; DeltaX = 1e-4; DeltaY = 1e-3;
X = [0.0:DeltaX:L]; Y = 0;
p_wall = SF_ExtractData(bf0,'p',X,Y);
ux_wall = SF_ExtractData(bf0,'ux',X,Y);
uy_wall = SF_ExtractData(bf0,'uy',X,Y);
tauxx_wall = SF_ExtractData(bf0,'tauxx',X,Y);
tauxy_wall = SF_ExtractData(bf0,'tauxy',X,Y);
tauyy_wall = SF_ExtractData(bf0,'tauyy',X,Y);


%%

close all
figure;  hold on;
%plot(X,tauxx_wall,'k-');
semilogy(X,abs(tauxy_wall),'r-'); 
semilogy(X,abs(tauyy_wall-p_wall),'b-'); 
figure; hold on;
plot(X,p_wall,'k-');
plot(X,ux_wall,'r-');
semilogy(X,abs(uy_wall),'b-');


