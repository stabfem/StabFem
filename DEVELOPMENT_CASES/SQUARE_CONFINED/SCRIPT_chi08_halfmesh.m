% Example of using StabFem for flow around a confined square 
%
% This case uses a half-mesh.

%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',2,'workdir','./Work_chi08/');


%% Computation of a base flow and adapted mesh

BETA = 0.8;
Xmin = -5;
Xmax = 20;
ffmesh = SF_Mesh('Mesh_SquareConfined.edp','Params',[BETA Xmin Xmax],'problemtype','2D');
bf=SF_BaseFlow(ffmesh,'Re',1);

for Re = [10 50 70 100 130 150 200]
    bf=SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf);
end

%% adaptation to BF, 2 modes (steady and unsteady) and a mask
evguess1 = 0.21; % previously determined 
evguess2 = 0.19701+6.15884i; % idem
[ev1,em1] = SF_Stability(bf,'nev',1,'shift',evguess1,'type','A'); 
[ev2,em2] = SF_Stability(bf,'nev',1,'shift',evguess2,'type','A'); 
bf = SF_Adapt(bf,em1,em2);
[ev1,em1] = SF_Stability(bf,'nev',1,'shift',evguess1,'type','A'); 
[ev2,em2] = SF_Stability(bf,'nev',1,'shift',evguess2,'type','A'); 
Mask = SF_Mask(bf.mesh,[-2 4 0 2 .2]); % to enforce grid step 0.2 in wake region
bf = SF_Adapt(bf,em1,em2,Mask,'Hmax',5);


%% Plot baseflow and eigenmode

figure;SF_Plot(bf,'mesh');
figure;
SF_Plot(bf,'vort','xlim',[-2 4],'colorrange',[-100 100],'title','Base Flow for Re = 200');
hold on;
SF_Plot(bf,'psi','contour','on','clevels',[ -.8 -.5 -.2 -.1 -.05 .05 0 .1 .2 .5 .8],'cstyle','patchdashedneg','xystyle','off','xlim',[-2 4]);

[ev,em] = SF_Stability(bf,'nev',1,'shift',evguess2,'type','D'); 
figure;SF_Plot(em(1),'ux','xlim',[-2 4],'ylim',[0,.5/BETA],'title',['Eigenmode for Re = 200 ; lambda = ',num2str(ev)]);

pause(0.1);

%% Plot full spectrum
[ev,em] = SF_Stability(bf,'nev',30,'shift',.5+5i,'type','D','plotspectrum','yes'); 
[ev,em] = SF_Stability(bf,'nev',30,'shift',.5+2i,'type','D','plotspectrum','yes'); 
pause(0.1);


%% If you don't trust the adapted mesh, do the following :
% bf = SF_Split(bf)
% [evREFINED,em] = SF_Stability(bf,'nev',1,'shift',ev,'type','D'); 
% ev 
% ev_REFINED 
% there should be only a few percent differences for the interesting modes




%% Loop over Re for stability computations

% first loop to catch the stationary znd low-frequency modes (using nev = 10)
Re_TAB = [200:-5:60];
shift = 0.21;
for j= 1:length(Re_TAB)
    Re = Re_TAB(j);
    bf=SF_BaseFlow(bf,'Re',Re);
    if(j==1) sort ='LR' ; else sort = 'cont'; end   % for first computation sort by increasing real part ; for next ones sort by continuation
    [evS,eM] = SF_Stability(bf,'nev',10,'shift',shift,'sort',sort,'sym','A');
    evS_TAB(:,j) = evS;
end  


% second loop to catch the high-frequency mode
shift = 0.19701+6.15884i;
for j = 1:length(Re_TAB)
    Re = Re_TAB(j)
    bf=SF_BaseFlow(bf,'Re',Re);
    if(j>=2) shift = 'cont'; end % switch to continuation mode 
    [evI,emI] = SF_Stability(bf,'nev',1,'shift',shift,'sort',sort,'sym','A');
    evI_TAB(:,j) = evI;
end 

%% Plot stability branches

figure(10);hold off;
for j = 1:4
    plot(Re_TAB,real(evS_TAB(j,:)),'-');hold on;
end
plot(Re_TAB,real(evI_TAB(1,:)),'--');
plot(Re_TAB,0*Re_TAB,'k:')
xlabel('Re' ); ylabel('\lambda_r')
ylim([-.1,.35]);

figure(11);hold off;
plot(Re_TAB,abs(10*imag(evS_TAB(1,:))),'-');hold on;    
plot(Re_TAB,imag(evI_TAB(1,:)),'--'); hold on;

xlabel('Re' ); ylabel('\lambda_i ; 10 \lambda_i')
pause(0.1);



% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
