function [bf,out] = SF_Stability_LoopBisRe(baseflow,varargin)
%
% Construction of a "branch" lambda(Re) with a loop over Re.
%
% Usage : EV =  SF_Stability_LoopRe(bf,Re_range,guess_ev, [Params, Values] )
%
%  [EV,Rec,Omegac] =  SF_Stability_LoopRe(bf,Re_range,guess_ev, [Params, Values] ) 
%
% Re_range is an array of values, typically [Restart:Restep:Reend]
% guess_ev is an approximate value of the eigenvalue at Re=Re_Range(1)
% (will be used as guess for first step, then continuation mode will be used)
%
% in three-output mode Rec and Omegac are the threshold values possibly
% detected on the interval.
%
% The next parameters are couple of [Param, Values] accepted by SF_Stability
% (do not specify the shift and nev; nev will be set to 1)
%
% Example : EV = SF_Stability_LoopRe(bf,[40 : 10 : 100],'m',1,'type','D')
%
% Option 'plot' allows to plot results. Specify  either 'yes' or a color.
%
% This program is part of the StabFem project distributed under gnu licence
% Copyright D. Fabre, october 9 2018
%

p = inputParser;
addParameter(p, 'm', 0, @isnumeric);
addParameter(p, 'shiftGuess', 0.3, @isnumeric);
addParameter(p, 'ReRange', [baseflow.Re*0.95:baseflow.Re*0.05:baseflow.Re*1.05], @isnumeric);
addParameter(p, 'coordType', "axi", @ischar); % planar or axi
addParameter(p, 'cont', "none", @ischar); % none or arclength 
SFcore_addParameter(p, baseflow,'Mach', 0.01, @isnumeric); % Mach
SFcore_addParameter(p, baseflow,'Omegax', 0.0, @isnumeric); % Rotation rate
addParameter(p, 'ncores', 1, @isnumeric);
addParameter(p, 'U2Max', 1.0, @isnumeric);
addParameter(p, 'U1Max', 0.0,@isnumeric);
addParameter(p, 'bctype', 2,@isnumeric);
addParameter(p, 'step', 10,@isnumeric);

parse(p, varargin{:});

% Parsed inputs
m = p.Results.m;
guess_ev = p.Results.shiftGuess;
Re_Range = p.Results.ReRange;
coordType = p.Results.coordType;
ncores = p.Results.ncores;
U2Max = p.Results.U2Max;
U1Max = p.Results.U1Max;
bctype = p.Results.bctype;
cont = p.Results.cont;

EV = NaN*ones(size(Re_Range));
guessI = guess_ev;


% first step using guess
bf=SF_BaseFlow(baseflow,'Re',Re_Range(1),'Options',...
                     ['-bctype ',num2str(bctype), ...
                     ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                     'ncores',ncores);
if(coordType == "planar")
    K(1) = k_Range(1);
    [ev,em] = SF_Stability(bf,'k',K(1),'m',m,'shift',guessI,'nev',10);% we take nev = 10 to make sure the first computation will be good... -> now made outside
    [ev,em] = SF_Stability(bf,'k',K(1),'m',m,'shift',ev(1),'nev',1);% to initiate the cont mode
else
    [ev,em] = SF_Stability(bf,'m',m,'shift',guessI,'nev',5,'ncores',ncores);% we take nev = 10 to make sure the first computation will be good... -> now made outside
    [ev,em] = SF_Stability(bf,'m',m,'shift',ev(1),'nev',1,'ncores',ncores);% to initiate the cont mode
end
EV(1)= ev;

% then loop...



if(cont == "arclength")
    while ( bf.Re < max(Re_Range) && bf.Re > min(Re_Range) && step > 1e-3)
        bf=SF_BFContinue(bf,'step',step,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);
        if(bf.iter < 0)
            disp("Not converged. Arc-length step reduced by a factor 2");
            step = 0.5*step;
            bf=SF_BFContinue(bf,'step',step,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);
             if(bf.iter < 0)
                disp("Not converged. Reducing again by a factor 10");
                step = 0.1*step;
                bf=SF_BFContinue(bf,'step',step,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);
                if bf.iter<0
                    disp('Arclength continuation failed')
                    break
                end
             end
        end
        step = min(1.4*step,stepMax); % raise the step for next iteration
    end
else
    for i=2:length((Re_Range))
        Re = Re_Range(i);
        bf=SF_BaseFlow(bf,'Re',Re,'Options',...
                             ['-bctype ',num2str(bctype), ...
                             ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                             'ncores',ncores);
        [ev,em] = SF_Stability(bf,'m',m,'nev',1,'shift','cont','ncores',ncores);
        EV(i) = ev;
        if(real(EV(i))*real(EV(i-1))<0 && ( abs(real(EV(i))) > 1e-4 || abs(real(EV(i-1))) > 1e-4 ) ) % Th Bolzano. Stop bisection in Re.
           break;
        end
        disp(['Re = ',num2str(Re),' : lambda = ',num2str(ev)]);
    end
end

    RePLOT = Re_Range; %[Re_Range(1:Ic-1) Rec Re_Range(Ic:end)];
    EVPLOT = EV; %[EV(1:Ic-1) 1i*omegac EV(Ic:end)];

% determines a threshold if required
    Rec = [];
    Icc = find(real([EV(1) EV]).*real([EV EV(end)])<0);omegac=[];Rec=[];
    if(isempty(Icc))
        Rec = interp1(real(EV),Re_Range,0,'spline','extrap');
        omegac = interp1(real(EV),imag(EV),0,'spline','extrap');
        status = 0;
    else
        for j=1:length(Icc)
            Ic = Icc(j);
            omegac = [omegac imag((EV(Ic-1)*real(EV(Ic))-EV(Ic)*real(EV(Ic-1)))/(real(EV(Ic))-real(EV(Ic-1))))]; 
            Rec = [Rec (Re_Range(Ic-1)*real(EV(Ic))-Re_Range(Ic)*real(EV(Ic-1)))/(real(EV(Ic))-real(EV(Ic-1)))];
            EVPLOT = [EVPLOT(1:Ic-1+j-1) , 1i*omegac, EVPLOT(Ic+j-1:end)]; % to draw the curves including threshold point
            RePLOT = [RePLOT(1:Ic-1+j-1) , Rec, RePLOT(Ic+j-1:end)];
            status = 1;
        end
    end
    out.EV = EV;
    out.Rec = Rec;
    out.omegac = omegac;
    out.status = status;
end

