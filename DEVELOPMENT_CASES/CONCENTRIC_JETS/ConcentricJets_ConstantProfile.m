%% StabFem Demo script for concentric jets WITH CONSTANT VELOCITY PROFILE

% by David (may 7 2020) from a previous version from Javier ; redesigned 22/05
%
% This case is for constant velocity profile, no pipes, U1 = 1 and U2=1.
%
% This case is for comparison with NEK5000 computations for Re =125.

%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',2,'ffdatadir','./WORK/'); % NB use verbosity=2 for autopublish mode ; 4 during work
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Chapter 1 : Generate a mesh and base flow
% Parameters
Xmax=200; YMax=20; U1Max=1.0; U2Max=1.0; R1=0.5; distance=4.5; D2=1.0;
bctype = 0; % select 0 for constant and 1 for Poiseuille
MappingParams = [-10,30,-5,5,-0.3,5,-0.3,5];
% Mesh generation
ffmesh = SF_Mesh('Mesh_ConcentricJets_NoPipe.edp','params',...
        [R1 distance D2 Xmax YMax],'problemtype','axixr',...
        'cleanworkdir','yes');
figure;SF_Plot(ffmesh,'mesh','title','Initial mesh');

% Generation of an initial solutioon
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',0);
bf = SF_Adapt(bf);

%%
% Generation of intial guess up to Re=150

ReTab = [10,25,50,75,100];
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re);
    % Generation of a mask to refine a given region
    MaskReg = [-10.0 20.0 -1.0 8.0 0.2]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',2,'Thetamax',30);
end

%% Plots of mesh and base flow (various plots) for Re = 125


bf = SF_BaseFlow(bf,'Re',125);
figure;subplot(2,1,1);SF_Plot(bf,'mesh','title','Adapted mesh');
subplot(2,1,2);SF_Plot(bf,'mesh','title','Adapted mesh (zoom)','xlim',[-1 20],'ylim',[0, 8]);
pause(.1); 

bf.Unorm = sqrt(bf.ux.^2+bf.ur.^2);
figure;SF_Plot(bf,'Unorm','xlim',[0 30],'ylim',[0 10],'title','Velocity norm');

%
figure;SF_Plot(bf,'ux','xlim',[0 10],'ylim',[0, 10],'title','axial velocity and streamfunction');hold on; 
SF_Plot(bf,'psi','contour','only','xlim',[0 10],'ylim',[0, 10],'clevels',[0 0.05 0.1 0.125 1 3 4 5.62 5.7] )

figure;  SF_Plot(bf,'p','xlim',[0 10],'ylim',[0 10],'colorrange',[-.1 .1],'colormap','redblue','title','pressure field and velocity vectors');
hold on; SF_Plot(bf,{'ux','ur'},'xlim',[0 10],'ylim',[0 10]);

%%
% plot of Ux along a vertical line
Xpos = 1;
Y=[0:0.01:YMax]; uxline=SF_ExtractData(bf,'ux',Xpos,Y);urline=SF_ExtractData(bf,'ur',Xpos,Y);
psiline = SF_ExtractData(bf,'psi',Xpos,Y);
figure; plot(uxline,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity at location x=1');
figure; plot(urline,Y);xlabel('Ur(r,1)');ylabel('r');title('radial velocity at location x=1');
figure; plot(psiline,Y);xlabel('Psi(r,1)');ylabel('r');title('streamfunction at location x=1');

%% look at spectrum : nothing unstable here

[ev,em] = SF_Stability(bf,'m',0,'shift',0.05+0.04i,'nev',15,'PlotSpectrum',true);

% 
% %[[PUBLISH]] -> tag for automatic publication
% 
