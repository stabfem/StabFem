%%  StabFem Demo script for concentric jets WITH TANH VELOCITY PROFILE
%
% This case corresponds to the configuration of Canton et al.


%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this option is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Chapter 1 : Generate a mesh and base flow
% Parameters
Xmax=100; YMax=20; U1Max=1.0; U2Max=1.0; R1=0.5; distance=0.1; D2=.4; Lpipe = 10;
bctype = 2; % select 0 for constant and 1 for Poiseuille and 2 for Tanh

% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'problemtype','axixr','cleanworkdir','yes');
figure;subplot(2,1,1);SF_Plot(ffmesh,'mesh','title','Initial mesh');
subplot(2,1,2);SF_Plot(ffmesh,'mesh','title','Initial mesh','xlim',[-2 2],'ylim',[0 2]);

% Generation of an initial solutioon
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',2);
bf = SF_BaseFlow(bf,'Re',2,'bctype',2);

% plots
bf = SF_Adapt(bf,'anisomax',4,'nbjacoby',3);
figure; SF_Plot(bf,'ux','xlim',[-1 2],'ylim',[0 2]);
Xpos = 1; Y=[0:0.01:1.5]; 
uxline=SF_ExtractData(bf,'ux',Xpos,Y);
figure(4); plot(uxline,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity at location x=1');hold on; 
pause(0.1);


%% Generation of initial guess up to Re=1420

ReTab = [10,30,100,200,400,700,1000];
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re);
    if bf.iter==-1
       disp(['Divergence in Newton for Re =',num2str(Re)]);
       break;
    end
    % Generation of a mask to refine a given region
    MaskReg = [-2.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',2,'anisomax',4,'nbjacoby',3,'recompute',false);
    Y=[0:0.01:1.5]; Xpos=1; uxline=SF_ExtractData(bf,'ux',Xpos,Y);
    figure(4); plot(uxline,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity at location x=1');hold on; 
    pause(0.1);
    
end
legend('Re=1','Re=10','Re=30','Re=100','Re=200','Re=400','Re=700','Re=1000')
%% Plots of mesh and base flow (various plots) for Re = 1000

figure;subplot(2,1,1);SF_Plot(bf,'mesh','title','Adapted mesh');
subplot(2,1,2);SF_Plot(bf,'mesh','title','Adapted mesh (zoom)','xlim',[-1 20],'ylim',[0, 8]);
pause(.1); 

bf.Unorm = sqrt(bf.ux.^2+bf.ur.^2);
figure;SF_Plot(bf,'Unorm','xlim',[-2 2],'ylim',[0 2],'title','Velocity norm');

figure;SF_Plot(bf,'ux','xlim',[-2 2],'ylim',[0, 2],'title','axial velocity and streamfunction');hold on; 
SF_Plot(bf,'psi','contour','only','xlim',[-2 2],'ylim',[0, 2],'clevels',[-0.01 , 0:0.05 :0.6] )

figure;  SF_Plot(bf,'p','xlim',[-2 2],'ylim',[0 2],'colorrange',[-.1 .1],'colormap','redblue','title','pressure field and velocity vectors');
hold on; SF_Plot(bf,{'ux','ur'},'xlim',[-2 2],'ylim',[ 0 2 ]);




%% Eigenvalue for Re = 1420, m=0
%

% Compute BF for Re = 1420 and adapted mesh

MappingParams = [-10,30,-5,5,-0.3,5,-0.3,5];
bf = SF_BaseFlow(bf,'Re',1200);
bf = SF_BaseFlow(bf,'Re',1300);
MaskReg = [-2.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
Mask = SF_Mask(bf.mesh,MaskReg);
bf = SF_Adapt(bf,Mask,'Hmax',2,'anisomax',10,'nbjacoby',10,'recompute',false);
bf = SF_BaseFlow(bf,'Re',1420);

[ev,em] = SF_Stability(bf,'m',0,'shift',1+5.8i,'nev',15,'PlotSpectrum',true,'MappingDef','box','MappingParams',MappingParams);
[evA,emA] = SF_Stability(bf,'m',0,'shift',1-5.8i,'nev',3,'type','A','MappingDef','box','MappingParams',MappingParams);
[sen] = SF_Sensitivity(bf,em(1), emA(1),'Type','S');
figure;  SF_Plot(emA(1),'ux','xlim',[-2 2],'ylim',[0 3],'title','Adjoint Eigenvector for Re = 1420, m=0');
figure;  SF_Plot(sen,'sensitivity','xlim',[-1 2],'ylim',[0 1],'title','Sensitivity for Re = 1420, m=0');

% Adapt mesh to adjoint mode

MaskReg = [-2.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
Mask = SF_Mask(bf.mesh,MaskReg);
bf = SF_Adapt(bf,Mask,emA(1),'Hmax',2,'anisomax',5,'nbjacoby',3,'recompute',false);
bf = SF_BaseFlow(bf,'Re',1420);


% Then recompute on adapted mesh

MappingParams = [-10,30,-5,5,-0.3,5,-0.3,5];
[ev,em] = SF_Stability(bf,'m',0,'shift',1+6i,'nev',10,'MappingDef','box','MappingParams',MappingParams,'plotspectrum',true);
[evA,emA] = SF_Stability(bf,'m',0,'shift',1-6i,'nev',10,'type','A','MappingDef','box','MappingParams',MappingParams,'plotspectrum',true);
[sen] = SF_Sensitivity(bf,em(1), emA(1),'Type','S');
figure;  SF_Plot(emA(1),'ux','xlim',[-2 2],'ylim',[0 3],'title','Adjoint Eigenvector for Re = 1420, m=0');
figure;  SF_Plot(sen,'sensitivity','xlim',[-1 2],'ylim',[0 1],'title','Sensitivity for Re = 1420, m=0');

%% Stability curves in the range [ 1500 - 1300 ] and threshold detection 



ReRange = [1500 : -50 : 1300];% going backwazrds is better
shiftinit = 1+6i;
bf = SF_BaseFlow(bf,'Re',ReRange(1));

[ev,em] = SF_Stability(bf,'shift',shiftinit,'nev',3); %to get the right starting point for the loop
SF_Stability_LoopRe(bf,ReRange,ev(1),'m',0,'plot',true);

%% Bonus : Demonstration of usage of parameters nbjacoby and anisomax for mesh adaptation
bf = SF_Load('MESH+BF','last');
bf6 = SF_Adapt(bf,'anisomax',5,'nbjacoby',6,'recompute',false);
bf3 = SF_Adapt(bf,'anisomax',5,'nbjacoby',3,'recompute',false);
bf0 = SF_Adapt(bf,'anisomax',5,'nbjacoby',0,'recompute',false);

bf10 = SF_Adapt(bf,'anisomax',10,'nbjacoby',3,'recompute',false);
bf5 = SF_Adapt(bf,'anisomax',5,'nbjacoby',3,'recompute',false);
bf2 = SF_Adapt(bf,'anisomax',2,'nbjacoby',3,'recompute',false);

figure;subplot(3,2,1);SF_Plot(bf6,'mesh','title',['mesh nbjacoby 6, Nver=',num2str(bf6.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);
subplot(3,2,2);SF_Plot(bf3,'mesh', 'title',['mesh nbjacoby 3, Nver=',num2str(bf3.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);
subplot(3,2,3);SF_Plot(bf0,'mesh','title',['mesh nbjacoby 0, Nver=',num2str(bf0.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);
subplot(3,2,4);SF_Plot(bf10,'mesh','title',['mesh nbjacoby 3, anisomax 10 Nver=',num2str(bf10.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);
subplot(3,2,5);SF_Plot(bf5,'mesh', 'title',['mesh nbjacoby 3, anisomax 5 Nver=',num2str(bf5.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);
subplot(3,2,6);SF_Plot(bf2,'mesh','title',['mesh nbjacoby 3, anisomax 0 Nver=',num2str(bf2.mesh.np)],'xlim',[-0.5 0.5],'ylim',[0.5 1.5]);


%% SUMMARY of all what has been done so far using SF_Status
%
% Here find a demonstration of how to use SF_Status to extract data from
% the database

sfs = SF_Status;

% Note that all thresholds detected while using SF_Stability_LoopRe are
% stored here :

sfs.THRESHOLDS

% you can use this to perform a parametric analysis and draw the marginal
% curves. See for instance the example
% STABLE_CASE/rotating_Cylinder/Cylinder_WithSmallRotation.m to see how to
% do this !



% 
% %[[NOPUBLISH]] -> tag for automatic publication
% 





