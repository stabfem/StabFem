%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%% Chapter 1: Initialise mesh at U_i=0, U_o=1
% Parameters
ncores=4;
Xmax=100; YMax=50; U1Max=2.0; U2Max=1.0; % first value
R1=0.5; distance=0.7; D2=1.0; Lpipe = 5; bctype = 2; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'problemtype','axixr','cleanworkdir','no');
bf = SF_BaseFlow(ffmesh,'Re',1,'Options',['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);

% Generation of an initial solution
[bf] = SF_Adapt(bf,'Hmax',2,'anisomax',1,'nbjacoby',3);
bf = SF_BaseFlow(bf,'Re',1,'Options',['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);

%% Create mesh at Rec
Rec=350;
ReTab = [10,30,100,150,200:100:Rec];


for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                     ['-bctype ',num2str(bctype), ...
                     ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                     'ncores',ncores);
    if bf.iter==-1
        disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
        break;
    end
    % Generation of a mask to refine a given region
    MaskReg = [-10.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',5,'anisomax',3);
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                     ['-bctype ',num2str(bctype), ...
                     ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                     'ncores',ncores);
end

bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                 ['-bctype ',num2str(bctype), ...
                 ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                 'ncores',ncores);

[ev1,em1] = SF_Stability(bf,'m',2,'ncores',ncores','shift',0.1,'nev',1);
%[ev0,em0] = SF_Stability(bf,'m',1,'ncores',ncores','shift',0.1,'nev',1);


%%
L_List = [0.7,0.6,0.5,0.4,0.375,0.35,0.325,0.3,0.25,0.225,0.2,0.175,0.15,0.125];
Rec_List = [308.5,384.75,490,731,816,918,1049,1214,1711,2100,2660,3483,4823,7140];
%%
L_ListExt=[0.125:0.0125:4.5];
close all;
fig=figure;
plot(L_List,Rec_List,'k');
plt=Plot();
xlabel('$L$','interpreter','Latex');
ylabel('$Re_c$','interpreter','Latex');
plt.Colors = {[0,0,0]};
plt.LineWidth = 3;
exportgraphics(fig,'Evolution_m2_deltau_0p5.pdf','ContentType','vector');


%hold on; plot(L_ListExt,200*L_ListExt.^-1.4);

%%
load('Steady_L0p5_m2')
close all;
fig=figure;
plot(Uinner_tab,Recrit_tab(1:length(Uinner_tab)),'k');
plt=Plot();
xlabel('$\delta_u$','interpreter','Latex');
ylabel('$Re_c$','interpreter','Latex');
plt.Colors = {[0,0,0]};
plt.LineWidth = 3;
exportgraphics(fig,'Mode_L0p5_m=2.pdf','ContentType','vector');
