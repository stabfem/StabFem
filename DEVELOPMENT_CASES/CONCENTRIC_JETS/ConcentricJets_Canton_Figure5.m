%%  StabFem Demo script for concentric jets WITH TANH VELOCITY PROFILE
%
% This case corresponds to the configuration of Canton et al.
% This scripts aims to reproduce figure 5 of the reference article : 
% base flow for Re = 1000 and several values of U2/U1


%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK_Canton_Fig5/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Compute base flow for each of the cases
% Store computed baseflow in a matlab list


%% First generate a mesh 

% Parameters
Xmax=100; YMax=20; 
U1Max=1.0; 
U2Max=1.0; % first value
R1=0.5; distance=0.1; D2=.4; Lpipe = 10;
bctype = 2; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'cleanworkdir','no');
bf = SF_BaseFlow(ffmesh,'solver','Newton_Axi.edp','Re',1,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)]);

% Generation of an initial solution
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',bctype ,'U2',U2Max,'U1',U1Max);
bf = SF_BaseFlow(bf,'Re',1,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)]);

%% Raise Re to 1000 for U2/U1 = 0.5

ReTab = [10,30,100,200:100:1400];
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)],'ncores',2);
    if bf.iter==-1
        disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
        break;
    end
    % Generation of a mask to refine a given region
    MaskReg = [-2.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',2,'anisomax',3,'nbjacoby',3);
    bf = SF_BaseFlow(bf,'Re',Re,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)]);
end
bfList = [bf];
Re=1420;
bf = SF_BaseFlow(bf,'Re',Re,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)],'ncores',2);
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','shift',0.1+6.0*1i,'nev',5,'type','A','m',0);
[evP,emP] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',1,'ncores',2);
[ev,em] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',1);
Re=2000;
bf = SF_BaseFlow(bf,'Re',Re,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)],'ncores',2);
[evP,emP] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',1,'ncores',2);
[ev,em] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',1);
Re=2000;
bf = SF_BaseFlow(bf,'Re',Re,'U2',U2Max,'U1',U1Max,'Options',['-bctype ',num2str(bctype)],'ncores',2);
[evP,emP] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',1,'ncores',2);
[evP2,emP2] = SF_Stability(bf,'shift',0.1+6.0*1i,'nev',5,'type','A','m',2,'ncores',2);
%% Then vary U2/U1 with Re = 1000 
    
UMaxList = [0.5,0.75,1.0,1.25,1.5,1.75,2.0]; 
NCases = length(UMaxList);

for i = 2:NCases
    U2max = UMaxList(i);
     bf = SF_BaseFlow(bf,'Re',Re,'U2',U2max); 
        if bf.iter==-1
            disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
            break;
        end
     Mask = SF_Mask(bf.mesh,MaskReg);
     bf = SF_Adapt(bf,Mask,'Hmax',2,'anisomax',3,'nbjacoby',3);
     bfList = [bfList bf];
end

%% Contour plots
figure;
subplot(2,2,1);SF_Plot(bfList(1),'ux','title','R_u = 0.5',...
                'xlim',[-0.1 0.6],'ylim',[0.3 0.8],'ColorMap','parula');
hold on;
subplot(2,2,1);SF_Plot(bfList(1),'psi','contour','only','xlim',[-0.1 0.6],...
                'ylim',[0.3 0.8],'clevels',[0.09:0.00025:0.095],'CColor','white' );

subplot(2,2,2);SF_Plot(bfList(3),'ux', 'title','R_u = 1',...
                'xlim',[-0.1 0.6],'ylim',[0.3 0.8],'ColorMap','parula');
hold on;
subplot(2,2,2);SF_Plot(bfList(3),'psi','contour','only','xlim',[-0.1 0.6],...
                'ylim',[0.3 0.8],'clevels',[0.09:0.00025:0.095],'CColor','white'  );
            
subplot(2,2,3);SF_Plot(bfList(5),'ux','title','R_u = 1.5',...
                'xlim',[-0.1 0.6],'ylim',[0.3 0.8],'ColorMap','parula');
hold on;
subplot(2,2,3);SF_Plot(bfList(5),'psi','contour','only','xlim',[-0.1 0.6],...
                'ylim',[0.3 0.8],'clevels',[0.09:0.00025:0.095],'CColor','white'  );
            
subplot(2,2,4);SF_Plot(bfList(7),'ux','title','R_u = 2',...
                'xlim',[-0.1 0.6],'ylim',[0.3 0.8],'ColorMap','parula');
hold on;
subplot(2,2,4);SF_Plot(bfList(7),'psi','contour','only','xlim',[-0.1 0.6],...
                'ylim',[0.3 0.8],'clevels',[0.09:0.00025:0.095],'CColor','white'  );
            
%% Velocity profiles Figure 6
XposList = [10,20,30,40,50]; Y=[0:0.001:2]; 
NPos = length(XposList); 
uxProf = zeros(NPos,length(Y)); urProf = zeros(NPos,length(Y));
for index = 1:NPos
    uxProf(index,:)=SF_ExtractData(bfList(2),'ux',XposList(index),Y);
    urProf(index,:)=SF_ExtractData(bfList(2),'ur',XposList(index),Y);
end
figure; plot(uxProf,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity');
figure; plot(urProf,Y);xlabel('Ur(r,1)');ylabel('r');title('radial velocity'); 


%% SUMMARY OF ALL WE HAVE DONE SO FAR
%

SF_Status;

%
% [[NOPUBLISH]]  NOTE BY DAVID : pb with stabulity ncore = 2 ffvectorget
% 




            