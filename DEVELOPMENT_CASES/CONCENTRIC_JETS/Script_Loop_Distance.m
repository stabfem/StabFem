%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Parameters 
Xmax=200; YMax=200; 
R1=0.5; D2=1.0; Lpipe = 5; bctype = 2; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
%% Parameters of the loop
distance_List = [0.5:0.1:1.2,1.225:0.025:1.5,1.6:0.1:4.5]; NDistance = length(distance_List);
EVList = {};
RecList = zeros(1,NDistance); omegacList = zeros(1,NDistance);
ReListH = {};
omegaEstimated = 0.0;
RecEstimated=300;
ncores=4;
mComp = 2;
tolReal = 5e-5;

%%
RecList = zeros(1,NDistance); 
omegacList = zeros(1,NDistance); 

RecList_upper_upper = zeros(1,NDistance); 
omegacList_upper_upper = zeros(1,NDistance); 

%% delta_u = 0.5
U1Max=2.0; U2Max=1.0; % first value

for iindex = 1:NDistance
    % Define the distance 
    distance = distance_List(iindex);
    % Define parameters
    paramMESH = [R1 distance D2 Lpipe Xmax YMax];
    % Generation of an initial mesh
    ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp',...
    'params',paramMESH,'problemtype','axixr','cleanworkdir','no');
    % Generation of an initial solution
    bf = SF_BaseFlow(ffmesh,'Re',1,'Options',...
    ['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),...
    ' -U1 ', num2str(U1Max)]);
    % First mesh adaptation
    [bf] = SF_Adapt(bf,'Hmax',2,'anisomax',1,'nbjacoby',3);
    % Recompute solution
    bf = SF_BaseFlow(bf,'Re',1,'Options',...
    ['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),...
    ' -U1 ', num2str(U1Max)]);

    
    % Generation of the baseflow at the estimated Rec
    ReTab = [10,30,60,linspace(80,RecEstimated,5),RecEstimated];
    
	for Re = ReTab
        bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
        if bf.iter==-1
            disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
            break;
        end
        % Generation of a mask to refine a given region
        MaskReg = [-10.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
        Mask = SF_Mask(bf.mesh,MaskReg);
        % Adaptation with a Mask
        [bf] = SF_Adapt(bf,Mask,'Hmax',5,'anisomax',2);
        bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
    end

    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);

    % Computation of the stability
    [ev,em] = SF_Stability(bf,'shift',0.3+omegaEstimated*1i,'nev',5,...
    'type','D','m',mComp,'ncores',ncores);

    evPos=sort(ev,'descend','ComparisonMethod','real');
    condition = sum(real(ev)>tolReal); % Because of the small spurious modes at 0
    
    if(abs(real(evPos(1)))>tolReal)
        EVList{iindex} = [evPos(1)];
        ReListH{iindex} = [bf.Re];
    else
        EVList{iindex} = [];
        ReListH{iindex} = [];
    end
    
    if(condition>0)
        while(condition>0)
            Re = 0.99*Re;
            bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
            [ev,em] = SF_Stability(bf,'shift',0.1+omegaEstimated*1i,'nev',1,...
            'type','D','m',mComp,'ncores',ncores);
            evPos=sort(ev,'descend','ComparisonMethod','real');
            if(abs(real(evPos(1)))>tolReal)
                EVList{iindex} = [EVList{iindex},evPos(1)];
                ReListH{iindex} = [ReListH{iindex},Re];
            end
            omegaEstimated = imag(evPos(1));
            condition = sum(real(ev)>tolReal);
        end
    else
        while(condition==0)
            Re = 1.01*Re;
            bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
            [ev,em] = SF_Stability(bf,'shift',0.1+0*1i,'nev',1,...
            'type','D','m',mComp,'ncores',ncores);
             evPos=sort(ev,'descend','ComparisonMethod','real');
             if(abs(real(evPos(1)))>tolReal)
                EVList{iindex} = [EVList{iindex},evPos(1)];
                ReListH{iindex} = [ReListH{iindex},Re];
            end
            omegaEstimated = imag(evPos(1));
            condition = sum(real(ev)>tolReal);         
        end
    end
 
% Check if there is only one in the interpolation list
if (length(ReListH{iindex})==1)
    Re = 0.995*Re;
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                 ['-bctype ',num2str(bctype), ...
                 ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                 'ncores',ncores);
    [ev,em] = SF_Stability(bf,'shift',0.1+omegaEstimated*1i,'nev',1,...
    'type','D','m',mComp,'ncores',ncores);
    evPos=sort(ev,'descend','ComparisonMethod','real');
    EVList{iindex} = [EVList{iindex},evPos(1)];
    ReListH{iindex} = [ReListH{iindex},Re];
end

Rec = interp1(real(EVList{iindex}),ReListH{iindex},0.0,'cubic','extrap');
omegac = interp1(real(EVList{iindex}),imag(EVList{iindex}),0.0,'cubic','extrap');
RecList_upper(iindex) = Rec;
omegacList_upper(iindex) = omegac;
RecEstimated = 1.01*interp1(distance_List(1:iindex),RecList_upper(1:iindex),distance_List(iindex+1),'linear','extrap');
RecEstimated = max(50,RecEstimated);
omegaEstimated=omegac;
end

RecList = RecList_upper;
save('DATA_m_2_deltau_0p5.mat','distance_List','RecList','omegacList','ReListH','EVList');

%% delta_u = 0.75
U1Max=1.5; U2Max=1.0; % first value
omegaEstimated = 0.0;
RecEstimated=525;

for iindex = 34:NDistance
    % Define the distance 
    distance = distance_List(iindex);
    % Define parameters
    paramMESH = [R1 distance D2 Lpipe Xmax YMax];
    % Generation of an initial mesh
    ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp',...
    'params',paramMESH,'problemtype','axixr','cleanworkdir','no');
    % Generation of an initial solution
    bf = SF_BaseFlow(ffmesh,'Re',1,'Options',...
    ['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),...
    ' -U1 ', num2str(U1Max)]);
    % First mesh adaptation
    [bf] = SF_Adapt(bf,'Hmax',2,'anisomax',1,'nbjacoby',3);
    % Recompute solution
    bf = SF_BaseFlow(bf,'Re',1,'Options',...
    ['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),...
    ' -U1 ', num2str(U1Max)]);

    
    % Generation of the baseflow at the estimated Rec
    ReTab = [10,30,60,linspace(80,RecEstimated,5),RecEstimated];
    
	for Re = ReTab
        bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
        if bf.iter==-1
            disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
            break;
        end
        % Generation of a mask to refine a given region
        MaskReg = [-10.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
        Mask = SF_Mask(bf.mesh,MaskReg);
        % Adaptation with a Mask
        [bf] = SF_Adapt(bf,Mask,'Hmax',5,'anisomax',2);
        bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
    end

    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);

    % Computation of the stability
    [ev,em] = SF_Stability(bf,'shift',0.3+omegaEstimated*1i,'nev',5,...
    'type','D','m',mComp,'ncores',ncores);

    evPos=sort(ev,'descend','ComparisonMethod','real');
    condition = sum(real(ev)>tolReal); % Because of the small spurious modes at 0
    
    if(abs(real(evPos(1)))>tolReal)
        EVList{iindex} = [evPos(1)];
        ReListH{iindex} = [bf.Re];
    else
        EVList{iindex} = [];
        ReListH{iindex} = [];
    end
    
    if(condition>0)
        while(condition>0)
            Re = 0.99*Re;
            bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
            [ev,em] = SF_Stability(bf,'shift',0.1+omegaEstimated*1i,'nev',1,...
            'type','D','m',mComp,'ncores',ncores);
            evPos=sort(ev,'descend','ComparisonMethod','real');
            if(abs(real(evPos(1)))>tolReal)
                EVList{iindex} = [EVList{iindex},evPos(1)];
                ReListH{iindex} = [ReListH{iindex},Re];
            end
            omegaEstimated = imag(evPos(1));
            condition = sum(real(ev)>tolReal);
        end
    else
        while(condition==0)
            Re = 1.01*Re;
            bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                         ['-bctype ',num2str(bctype), ...
                         ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                         'ncores',ncores);
            [ev,em] = SF_Stability(bf,'shift',0.1+0*1i,'nev',1,...
            'type','D','m',mComp,'ncores',ncores);
             evPos=sort(ev,'descend','ComparisonMethod','real');
             if(abs(real(evPos(1)))>tolReal)
                EVList{iindex} = [EVList{iindex},evPos(1)];
                ReListH{iindex} = [ReListH{iindex},Re];
            end
            omegaEstimated = imag(evPos(1));
            condition = sum(real(ev)>tolReal);         
        end
    end
 
% Check if there is only one in the interpolation list
if (length(ReListH{iindex})==1)
    Re = 0.995*Re;
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                 ['-bctype ',num2str(bctype), ...
                 ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                 'ncores',ncores);
    [ev,em] = SF_Stability(bf,'shift',0.1+omegaEstimated*1i,'nev',1,...
    'type','D','m',mComp,'ncores',ncores);
    evPos=sort(ev,'descend','ComparisonMethod','real');
    EVList{iindex} = [EVList{iindex},evPos(1)];
    ReListH{iindex} = [ReListH{iindex},Re];
end

Rec = interp1(real(EVList{iindex}),ReListH{iindex},0.0,'cubic','extrap');
omegac = interp1(real(EVList{iindex}),imag(EVList{iindex}),0.0,'cubic','extrap');
RecList(iindex) = Rec;
omegacList_upper(iindex) = omegac;
if(length(RecList(1:iindex)) > 1)
    RecEstimated = 1.01*interp1(distance_List(1:iindex),RecList(1:iindex),distance_List(iindex+1),'linear','extrap');
end
RecEstimated = max(50,RecEstimated);
omegaEstimated=omegac;
end

save('DATA_m_2_deltau_0p75.mat','distance_List','RecList','omegacList','ReListH','EVList');

