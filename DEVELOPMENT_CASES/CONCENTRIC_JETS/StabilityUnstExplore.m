function [EVList,EMList] = StabilityUnstExplore(bf,varargin)
% Function to explore unstable modes of the spectrum
    p = inputParser;
    addParameter(p, 'n', @isnumeric); % for special mode
    addParameter(p, 'm', 0, @isnumeric); % for special mode
    addParameter(p, 'shiftImagStart', 0.0, @isnumeric); % for special mode
    addParameter(p, 'shiftImagEnd', 3.0, @isnumeric); % for special mode
    addParameter(p, 'shiftReal', 0.3, @isnumeric); % for special mode
    addParameter(p, 'nev', 10, @isnumeric); % for special mode
    addParameter(p, 'MappingDef', 'none'); % Array of parameters for the cases involving mapping
    addParameter(p, 'MappingParams', 'default'); % Array of parameters for the cases involving mapping

    parse(p, varargin{:});
% Now the right parameters are in p.Results
    n = p.Results.n;
    m = p.Results.m;
    shiftImagRange = linspace(p.Results.shiftImagStart,p.Results.shiftImagEnd,n);
    shiftReal = p.Results.shiftReal;
    nev = p.Results.nev;
    MappingDef = p.Results.MappingDef;
    MappingParams = p.Results.MappingParams;

    EVList = []; EMList = []; % Lists of eigenvalues/modes
% Compute n eigenvalue problems
    for i=1:n
        shift = shiftReal + 1i*shiftImagRange(i);
        [ev,em] = SF_Stability(bf,'m',m,'shift',shift,'nev',nev,...
                             'MappingDef',MappingDef,'MappingParams',MappingParams);
        EVList = [EVList,ev];
        EMList = [EMList,em];
    end
end