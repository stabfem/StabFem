%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%% Chapter 1: Initialise mesh at U_i=0, U_o=1
% Parameters
ncores=4;
Xmax=100; YMax=50; U1Max=0.0; U2Max=1.0; % first value
R1=0.5; distance=0.5; D2=1.0; Lpipe = 5; bctype = 2; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'problemtype','axixr','cleanworkdir','no');
bf = SF_BaseFlow(ffmesh,'Re',1,'Options',['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);

% Generation of an initial solution
[bf] = SF_Adapt(bf,'Hmax',2,'anisomax',1,'nbjacoby',3);
bf = SF_BaseFlow(bf,'Re',1,'Options',['-bctype ',num2str(bctype), ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)]);

%% Create mesh at Rec
Rec=610;
ReTab = [10,30,100,150,200:50:Rec];


for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                     ['-bctype ',num2str(bctype), ...
                     ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                     'ncores',ncores);
    if bf.iter==-1
        disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
        break;
    end
    % Generation of a mask to refine a given region
    MaskReg = [-10.0 10.0 .0 1.5 0.1]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',5,'anisomax',2);
    bf = SF_BaseFlow(bf,'Re',Re,'Options',...
                     ['-bctype ',num2str(bctype), ...
                     ' -U2 ', num2str(U2Max),' -U1 ', num2str(U1Max)],...
                     'ncores',ncores);
end


%%
Uinner_tab = [0.0:0.02:0.18,0.19:0.001:0.2,0.201:0.001:0.238,0.24:0.004:0.396,0.4:0.01:1.52,1.55:0.05:2.0]; 
Uouter = 1.0; % Constant Uouter
bctype = 2; % Tanh inlet
Recrit_tab = nan*ones(size(Uinner_tab)); 
Recrit_tab(32) = 605;
Re_RangeS = [Recrit_tab*1.05:-Recrit_tab*0.01:Recrit_tab*0.95];
omegaS_tab = nan*ones(size(Uinner_tab));
omegaS_tab(1) = 0.0;
omegasSguess = 0.0;
status_tab = nan*ones(size(Uinner_tab));

for i=1:length(Uinner_tab)
    % Update parameter
    Uin = Uinner_tab(i);
    % Compute threshold
    [bf,out] = SF_Stability_LoopBisRe(bf,'ReRange',Re_RangeS,...
    'shiftGuess',0.05+omegasSguess*1i,'m',2,...
    'U1Max',Uin,'U2Max',Uouter,'ncores',ncores,'bctype',bctype);
    
    % In case of bisection out of the interval
    while(out.status == 0)
        Re_RangeS = [out.Rec*1.02:-out.Rec*.01:0.98*out.Rec]; 
        omegasSguess = out.omegac;
        [bf,out] = SF_Stability_LoopBisRe(bf,'ReRange',Re_RangeS,...
        'shiftGuess',0.05+omegasSguess*1i,'m',2,...
        'U1Max',Uin,'U2Max',Uouter,'ncores',ncores,'bctype',bctype);
    end
    % Save current values
    EVS = out.EV;
    Recrit_tab(i) = out.Rec;
    omegaS_tab(i) = out.omegac;
    status_tab(i) = out.status;
    if(i<2) % guess = start value
        ResSguess = Recrit_tab(1); omegasSguess = omegaS_tab(1);
        Re_RangeS = [ResSguess*1.1:-ResSguess*.025:0.9*ResSguess];
    elseif(i<length(Uinner_tab)) % guess = extrapolation using two previous points
        ResSguess = interp1(Uinner_tab(1:i),Recrit_tab(1:i),Uinner_tab(i+1),'linear','extrap');
        Re_RangeS = [ResSguess*1.05:-ResSguess*.025:0.9*ResSguess];
        omegasSguess = interp1(Uinner_tab(1:i),omegaS_tab(1:i),Uinner_tab(i+1),'linear','extrap');
    end
end

save('Steady_L0p5_m2.mat','Recrit_tab','omegaS_tab','status_tab','bf','Uinner_tab','bctype','Uouter');