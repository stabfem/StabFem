h = .1;                     // Characteristic length of a mesh element
// outer borders
 Point(1) = {-5, -1, 0, h};   // Point construction
 Point(2) = {5, -1, 0, h};
 Point(3) = {5, 1, 0, h};
 Point(4) = {-5, 1, 0, h};
 Line(11) = {1,2};            //Lines
 Line(12) = {2,3};
 Line(13) = {3,4};
 Line(14) = {4,1};
 Curve Loop(21) = {11,12,13,14};   // A Boundary

// blunt body
 Point(5) = {-.5, .5, 0, h};   // Point construction
 Point(6) = {.5, .5, 0, h};
 Point(7) = {.5, -.5, 0, h};
 Point(8) = {-.5, -.5, 0, h};
 Line(15) = {5,6};            //Lines
 Line(16) = {6,7};
 Line(17) = {7,8};
 Line(18) = {8,5};
 Curve Loop(22) = {15,16,17,18};   // A Boundary

// mesh assembly
 Plane Surface(31) = {21,22};     // A Surface

// labels
 Physical Surface(31) = {31};  // Setting a label to the Surface 
 Physical Curve(1) = {14};
 Physical Curve(3) = {12};
 Physical Curve(4) = {11,13};
 Physical Curve(2) = {15,16,17,18};

