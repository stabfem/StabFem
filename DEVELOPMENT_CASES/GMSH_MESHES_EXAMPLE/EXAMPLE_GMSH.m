%% Example on how to import a mesh from gmsh for StabFem
%
% The present directory contains two examples :
%  - square.msh2 for a very simple square domain
%  - bluntbody.msh2 for a blunt body in a pipe
%
% Note that after generation with gmsh, your mesh must be exported 
% under format "version 2 ASCII" with extension ".msh2". 
% 
% translation of the mesh is done in program "Mesh_GMSH.edp" 
% (located in the folder SOURCES_FREEFEM/ of the project). 
% See more explanations in this program and in the geometry 
% definition files square.geo and bluntbody.geo used by gmsh.

addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',4);
SF_DataBase('create','./WORK/');

%% A first very simple example : square
ffmesh = SF_Mesh('GMSH_Mesh.edp','Options',{'file','square.msh2'});
figure; SF_Plot(ffmesh);


%% A second example : blunt body in pipe
ffmesh = SF_Mesh('GMSH_Mesh.edp','Options',{'file','bluntbody.msh2'});
%%
figure; SF_Plot(ffmesh,'boundary','on','bdlabels',1);

%% generating a base flow 
bf = SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',1);
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',50);
bf = SF_Adapt(bf); % always better !
bf = SF_BaseFlow(bf,'Re',50);
figure; SF_Plot(bf,'ux');

%% stability analysis
[ev,em]= SF_Stability(bf,'solver','Stab_2D.edp','nev',10,'shift',1i+.2,'plotspectrum',true);
figure; SF_Plot(em(1),'ux');

%% NB 
% Here boundary conditions are slip conditions on upper/lower walls 
% because label attributed to these boundaeries is 4. Use 2 for no-slip.  

%% [[PUBLISH]]

