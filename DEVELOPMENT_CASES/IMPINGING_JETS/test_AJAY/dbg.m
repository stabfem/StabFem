%%
%
% First we set a few global variables needed by StabFem
%
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',3,'ffdatadir','./WORK/');
%
%
SF_Status;
