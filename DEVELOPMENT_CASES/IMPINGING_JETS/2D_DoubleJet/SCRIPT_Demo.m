%%  LINEAR Stability ANALYSIS of the wake of a cylinder with STABFEM  
%[[PUBLISH]]
%
%  this scripts demonstrates how to use StabFem to study the instability
%  in the wake of double cartesian impinging jet.
%  LINEAR STABILITY ANALYSIS (LSA)
%
%  # Generation of an adapted mesh
%  # Base-flow properties
%  # Stability curves St(Re) and sigma(Re)
%
%%
%
% First we set a few global variables needed by StabFem
%
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',2,'ffdatadir','./WORK/');

%% ##### CHAPTER 1 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE

%  # Parameters of the double impinging jet
H=5; Rc=4; Lpipe=10; meshRef=5; Rout=200;

% Mesh adaptation
ffmesh = SF_Mesh('mesh_DoubleJet.edp','Params',[H Rc Lpipe Rout],'problemtype','2D');
bf = SF_BaseFlow(ffmesh,'Re',1);   
bf = SF_Adapt(bf,'Hmax',meshRef);

ReAdaptMax = 80;

for Re = [10:10:ReAdaptMax] 
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'Hmax',meshRef);
end

%% CHAPTER 1b : DRAW FIGURES for mesh and base flowfigureformat='png';
figureformat='png';
system('mkdir FIGURES');

%  plot the mesh (full size)
figure();SF_Plot(bf,'mesh','xlim',[-10 H],'ylim',[0 200]);
title('Initial mesh (full size)');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Mesh_Full',figureformat);

%  plot the mesh (zoom)
figure();SF_Plot(bf,'mesh','xlim',[-1.5 H],'ylim',[0 35+Rc]);
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/Mesh',figureformat);

%   plot the base flow for Re = 80
figure();
SF_Plot(bf,'vort','xlim',[-10 H],'ylim',[0 35+Rc],'cbtitle','\omega_z','colormap','redblue','colorrange',[-2 2],'Boundary','on','BDColors','k');
hold on;SF_Plot(bf,'psi','contour','only','clevels',[-.02 0 .2 1 2 5],'xlim',[-10 H],'ylim',[0 35+Rc]);
box on;  set(gca,'FontSize', 18); saveas(gca,'FIGURES/BaseFlowRe80',figureformat);


%% ##### CHAPTER 1 : COMPUIING FINAL MESH, DIRECT/ADJOINT AND STRUCT SEN
[evSA,emSA] = SF_Stability(bf,'nev',5,'shift',0.1+0.03i,'k',0.5,'type','D','sym','A');
[evSS,emSS] = SF_Stability(bf,'nev',5,'shift',0.1+0.03i,'k',0.5,'type','D','sym','S');
bf = SF_Adapt(bf,emSA(1),'Hmax',meshRef);

[evSAD,emSAD] = SF_Stability(bf,'nev',5,'shift',0.1+0.0i,'k',0.0,'type','D','sym','A');
[evSAA,emSAA] = SF_Stability(bf,'nev',5,'shift',0.1+0.0i,'k',0.0,'type','A','sym','A');
emSAS = SF_Sensitivity(bf,emSAD(1),emSAA(1));

%% CHAPTER 2b : DRAW FIGURES for direct/adjoint eigenmode and Structural Sensitivity
figure();SF_Plot(emSAD(1),'ux','xlim',[-10 H],'ylim',[0 35+Rc],'colormap','redblue','colorrange','cropcentered',...
    'boundary','on','bdlabels',2,'bdcolors','k');
box on; set(gca,'FontSize', 18); saveas(gca,'FIGURES/EigenModeRe80_AdaptemSA',figureformat);  %

figure();SF_Plot(emSAA(1),'ux','xlim',[-10 H],'ylim',[0 35+Rc],'colormap','redblue','colorrange','cropcentered','boundary','on','bdlabels',2,'bdcolors','k');
box on;  set(gca,'FontSize', 18); saveas(gca,'FIGURES/EigenModeAdjRe80',figureformat);

figure();SF_Plot(emSAS,'S','xlim',[-10 H],'ylim',[0 35+Rc],'colormap','ice','boundary','on','bdlabels',2,'bdcolors','k');

box on;
pause(0.1);


