%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/','ErrorIfDiverge',false);
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
SF_core_setopt('ErrorIfDiverge') = 0;
%% Chapter 1 : builds base flow/mesh
% creation of an initial mesh
H = 7.6; Lpipe = 10; Rout = 80; % ,nb first case was done with Rout = 120 !
ffmesh = SF_Mesh('mesh_ImpactingJet.edp','Params',[H Lpipe Rout],'problemtype','2D');
bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',5);
%
for Re = [10 30 45 60 70 80]
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'anisomax',2,'Hmax',2);
    bf = SF_BaseFlow(bf,'Re',Re);
end



Re = bf.Re;
ReMax = 89; ReMin = 70; % this is the termination of the loop. Remin is in case the curve does not loop back.
stepMax = 2;
nev = 5;
indexEV = 0;
step = stepMax; 
EnerList = []; betaList = []; ReList = [];  RpList = []; 
EVA = zeros(nev,1); EVA0 = zeros(nev,1); EVS = zeros(nev,1); EVS0 = zeros(nev,1);
%load('BASEFLOW.mat');
while ( bf.Re < ReMax && bf.Re > ReMin && abs(step) > 1e-5)
    bf=SF_BFContinue(bf,'step',step);
    if(bf.iter < 0)
        disp("Not converged. Arc-length step reduced by a factor 2");
        step = 0.25*step;
        bf=SF_BFContinue(bf,'step',step);
         if(bf.iter < 0)
            disp("Not converged. Reducing again by a factor 10");
            step = 0.1*step;
            bf=SF_BFContinue(bf,'step',step);
            if bf.iter<0
                disp('Arclength continuation failed')
                break
            end
         end
    end
    
    step = sign(step)*min(abs(1.4*step),abs(stepMax)); % raise the step for next iteration
    Re = bf.Re;
    EnerList = [EnerList,bf.Energy];
    betaList = [betaList,bf.beta];
    ReList = [ReList, bf.Re];
    RpList = [RpList, bf.Rp];

    [evA0,emA0] = SF_Stability(bf,'nev',5,'shift',0.1+0.3i,'sym','A');
    [evA,emA] = SF_Stability(bf,'nev',5,'shift',0.1+0.03i,'k',0.375,'type','D','sym','A');
    [evS,emS] = SF_Stability(bf,'nev',5,'shift',0.1+0.04i,'k',0.425,'type','D','sym','S');
    [evS0,emS0] = SF_Stability(bf,'nev',5,'shift',0.1+0.0i,'k',0.0,'type','D','sym','S');

    indexEV = indexEV + 1;
    EVA0(:,indexEV) = evA0;
    EVA(:,indexEV) = evA;
    EVS(:,indexEV) = evS;
    EVS0(:,indexEV) = evS0;
    %figure(45);plot(ReList,betaList,'*'); pause(0.1); hold on;
end

figure; plot(ReList,betaList,'*');
figure; hold on;
plot(ReList(2:end),real(EVA0(1,:)),'-');  plot(ReList(2:end),real(EVA(1,:)),'--');
plot(ReList(2:end),real(EVS0(1,:)),':');  plot(ReList(2:end),real(EVS(1,:)),'.-');

[evA,emA] = SF_Stability(bf,'nev',5,'shift',0.1+0.03i,'k',0.375,'type','D','sym','A');
[evS,emS] = SF_Stability(bf,'nev',5,'shift',0.1+0.04i,'k',0.425,'type','D','sym','S');
[evA_A,emA_A] = SF_Stability(bf,'nev',5,'shift',0.1+0.03i,'k',0.375,'type','A','sym','A');
[evS_A,emS_A] = SF_Stability(bf,'nev',5,'shift',0.1+0.04i,'k',0.425,'type','A','sym','S');

CodA.k_1 = 0.375; CodA.k_2 = 0.425;
[wnlAS, meanflowAS, mode1A, mode2S, modeHHB] = SF_WNL(bf,emA(1),'Adjoint',emA_A(1),...
    'modeB',emS(1),'modeBAdjoint',emS_A(1),'mModeA',CodA.k_1,'mModeB',CodA.k_2,...
    'NormalForm','HopfHopf5','Normalization','E');
save('Results.mat');
