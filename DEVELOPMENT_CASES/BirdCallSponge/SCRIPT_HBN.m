%%  Harmonic balance continuation of the double hole tone configuration
%
% 
% THIS SCRIPTS SERVES AS AN EXAMPLE OF THE PARALLEL EXECUTION 
% OF HBM WITH N HARMONICS
% 
%
%

%% Chapter 0 : Initialization 
% String filename of HBN with 2 harmonics
strFileName = 'convergedHBNSponge.mat';
load(strFileName);
% Load StabFEM drivers
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
%%
% ReList = [350:5:400];
% omegaNList = [imag(modeList(1).lambda),imag(modeList(3).lambda),imag(modeList(5).lambda),imag(modeList(7).lambda)];
% AEnergy1 = [0.0589862,0.0625044,0.0659934,0.0692001];
% AEnergy2 = [0.0132572,0.0147569,0.0163071,0.0177859];
% ReInt=[337:1:340];

%% Reynolds loop for HBalance
nprocs=6;
ReList = [348:4:400];
NModes=2;
for Re=ReList
    omegaNL=interp1(ReInt,omegaNList,Re,'makima','extrap');
    A1=interp1(ReInt,AEnergy1,Re,'makima','extrap');
    A2=interp1(ReInt,AEnergy2,Re,'makima','extrap');
    modeHBN = modeList(end-1:end); modeHBN(1).lambda=omegaNL*1i;
    meanflow=mflowList(end); 


    [mflow,mHBN] = SF_HBN(meanflow,modeHBN,'Re',Re,'Ma',Ma,...
                            'symmetry','N','symmetryBF','N',...
                            'NModes',NModes,'PCtype','bjacobi',...
                             'Aguess',[A1,A2],'ncores',nprocs);

% Save parametric values
    mflowList = [mflowList,mflow];
    modeList = [modeList,mHBN];
    ReInt = [ReInt,Re];
    omegaNList = [omegaNList,imag(mHBN(1).lambda)]
    AEnergy1 = [AEnergy1,mHBN(1).AEnergy];
    AEnergy2 = [AEnergy2,mHBN(2).AEnergy];
    
    save(['HBM',num2str(NModes),'Re',num2str(Re),'Ma',num2str(Ma),'.mat'],'mflowList','modeList','ReInt','omegaNList','AEnergy1','AEnergy2')
end