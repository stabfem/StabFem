%% Flow around a wing profile : example for a NACA0012 profile
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4,'ffdatadir','WORK');

%% Generation of a mesh
thickness=5e-3;
mesh = SF_Mesh('Mesh_DragonFly.edp','problemtype','2d',...
               'Options',{'R0',20,'np',128,'npext',0.25,'thickness',thickness});

figure;
SF_Plot(mesh,'title','Mesh (Zoom)','xlim',[0 1],'ylim',[-.1 .1],'Boundary','on','BDLabels',[2]);

%%
bf = SF_BaseFlow(mesh,'Re',1,'alpha',0);
bf = SF_BaseFlow(mesh,'Re',10,'alpha',0);
bf = SF_BaseFlow(mesh,'Re',100,'alpha',0);
MaskInner = SF_Mask(bf.mesh,[-0.2,1,-0.1,0.1,2e-3]);
bfM = SF_Adapt(bf,'Hmax',2,'anisomax',3);
bf = SF_BaseFlow(bfM,'Re',100,'alpha',0);
bf = SF_BaseFlow(bf,'Re',200,'alpha',0);
bf = SF_BaseFlow(bf,'Re',500,'alpha',0);
bf = SF_BaseFlow(bf,'Re',750,'alpha',0);
bf = SF_BaseFlow(bf,'Re',1000,'alpha',0);
ReList = [1250:250:6000];
for index_Re = ReList
    bf = SF_BaseFlow(bf,'Re',index_Re,'alpha',0,'Omegax',0);
end

SpecShift = [0.0:0.1:5.0];
for index_Spec = 1:length(SpecShift)
    shift = 0.3 + 1i*SpecShift(index_Spec);
    [ev,em] = SF_Stability(bf,'shift',shift,'nev',3,'type','D');
end
figure; pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(2,1,1);
SF_Plot(bf,'ux','xlim',[-2 10],'ylim',[-2 2],...
    'Boundary','on','BDLabels',[2],'BDColors','k');
subplot(2,1,2);
SF_Plot(bf,'vort','xlim',[-2 10],'ylim',[-2 2],'ColorRange',[-10,10],...
    'Boundary','on','BDLabels',[2],'BDColors','k');

%% Chapter 3 : Launch a DNS
%
% We do 5000 time steps and produce snapshots each 10 time steps.
% We use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_TS_Launch.m SF_TS_Launch.m> 
% which launches the Freefem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp>

Niter = 10000;   % total number of time steps
iout = 10;   % will generate snapshots each iout time steps
dt = 0.00025;   % time step 
DNSOptions = {'Re',6000,'Niter',Niter,'dt',dt};
startfield = SF_Add({bf,em(1)},'coefs',[1,0.01],'NameParams',['Re'],'Params',[bf.Re]); % creates startfield = bf+0.01*em

folder = SF_TS_Launch('TimeStepper_2D.edp','Init',startfield,'Options',DNSOptions,'wait',true);
ts = SF_TS_Check('WORK/TimeStepping/RUN000012/')
