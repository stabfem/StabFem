%% Flow around a wing profile : example for a NACA0012 profile
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('open','WORK');

%% Generation of a mesh
thickness=5e-3;
mesh = SF_Mesh('Mesh_DragonFly.edp','problemtype','2d',...
               'Options',{'R0',20,'np',128,'npext',0.25,'thickness',thickness});

figure;
SF_Plot(mesh,'title','Mesh (Zoom)','xlim',[0 1],'ylim',[-.1 .1],'Boundary','on','BDLabels',[2]);

%%
bf = SF_BaseFlow(mesh,'Re',100,'alpha',7);
MaskInner = SF_Mask(bf.mesh,[-0.2,1,-0.1,0.1,2e-3]);
bfM = SF_Adapt(bf,'Hmax',2,'anisomax',3);
bf = SF_BaseFlow(bfM,'Re',100,'alpha',7);
bf = SF_BaseFlow(bf,'Re',300,'alpha',7);
bf = SF_BaseFlow(bf,'Re',800,'alpha',7);
bf = SF_BaseFlow(bf,'Re',1500,'alpha',7);
bf = SF_BaseFlow(bf,'Re',2000,'alpha',7);
MaskInner = SF_Mask(bf.mesh,[-0.2,1,-0.1,0.1,2e-3]);
bfM = SF_Adapt(bf,'Hmax',2,'anisomax',3);
%bf = SF_BaseFlow(bfM,'Re',2000,'alpha',7);

%% initial condition is computed for alpha = 7.05 to initiate DNS
bf = SF_BaseFlow(bfM,'Re',2000,'alpha',7.05);

%% launch DNS
folder = SF_TS_Launch('TimeStepper_2D.edp','init',bf,'wait',true,'Options',{'Niter',20000,'dt',0.0005,'iout',1000,'alpha',7,'Re',2000});
%% import results and plot
DNS = SF_TS_Check;

Snap = SF_Load('TimeStepping',1,5000);
%%
figure; SF_Plot(Snap,'vort','colorrange',[-50 50],'boundary','on');
hold on; SF_Plot(Snap,{'ux','uy'});
%%
figure(4); subplot(2,1,1);plot(DNS(1).t(2:end),DNS(1).Fy(2:end));xlabel('t');ylabel('Fy');
subplot(2,1,2);plot(DNS(1).t(2:end),DNS(1).Fx(2:end));xlabel('t');ylabel('Fx');
figure(5);hold on; plot(DNS(1).Fx(2:end),DNS(1).Fy(2:end));xlabel('Fx');ylabel('Fy');

%% relaunch DNS to do another 10000 time steps (results wl
folder = SF_TS_Launch('TimeStepper_2D.edp','init',Snap,'wait',false,'Options',{'Niter',5000,'dt',0.0005,'iout',200,'alpha',7,'Re',2000});
DNS = SF_TS_Check;
%%
figure(4); subplot(2,1,1); hold on; plot(DNS(6).t(2:end),DNS(6).Fy(2:end));xlabel('t');ylabel('Fy');
subplot(2,1,2);hold on; plot(DNS(6).t(2:end),DNS(6).Fx(2:end));xlabel('t');ylabel('Fx');
figure(5);hold on; plot(DNS(6).Fx(2:end),DNS(6).Fy(2:end));xlabel('Fx');ylabel('Fy');

%% New DNS Re = 4000
folder = SF_TS_Launch('TimeStepper_2D.edp','init',Snap,'wait',false,'Options',{'Niter',10000,'dt',0.0004,'iout',200,'alpha',7,'Re',4000});

%%
DNSlast = SF_TS_Check(11);
figure(14); subplot(2,1,1); hold on; plot(DNSlast.t(2:end),DNSlast.Fy(2:end));xlabel('t');ylabel('Fy');
subplot(2,1,2);hold on; plot(DNSlast.t(2:end),DNSlast.Fx(2:end));xlabel('t');ylabel('Fx');
figure(16);hold on; plot(DNSlast.Fx(2:end),DNSlast.Fy(2:end));xlabel('Fx');ylabel('Fy');



%% RERElaunch DNS
folder = SF_TS_Launch('TimeStepper_2D.edp','init',Snap,'wait',true,'Options',{'Niter',15000,'dt',0.0005,'iout',200,'alpha',7,'Re',2000});

%% import results and plot
DNS = SF_TS_Check;
figure(20);subplot(2,1,1);plot(DNS(12).t(2:end),DNS(12).Fx(2:end));xlabel('t');ylabel('Fx');hold on; plot(DNS(11).t(2:end),DNS(11).Fx(2:end));xlabel('t');ylabel('Fx');
subplot(2,1,2);;plot(DNS(12).t(2:end),DNS(12).Fy(2:end));xlabel('t');ylabel('Fx');hold on; plot(DNS(11).t(2:end),DNS(11).Fy(2:end));xlabel('t');ylabel('Fy');



%% launch DNS Re = 6000
folder = SF_TS_Launch('TimeStepper_2D.edp','init',Snap,'wait',true,'Options',{'Niter',50000,'dt',0.00025,'iout',1000,'alpha',7,'Re',6000});
