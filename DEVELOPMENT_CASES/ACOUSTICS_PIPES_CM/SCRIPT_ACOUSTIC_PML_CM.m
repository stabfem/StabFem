%% Acoustic field in a pipe with harmonic forcing at the bottom
%
%  This scripts demonstrates the efficiency of StabFem for a linear acoustics problem
%
%   Problem : find the velocity potential $\phi$ such as :
%
%  * $\Delta \phi + k^2 \phi = 0$
%  * $u_z = \partial_z \phi = 1$ along $\Gamma_{in}$
%  * Sommerfeld radiation condition on $\Gamma_{out}$ 
%  ( $k = \omega c_0$ is the acoustic wavenuber) 
%
% 
%  Variational formulation :
%
%  $$ \int \int_\Omega \left( \nabla \phi \cdot \nabla \phi^* + k^2 \phi \phi^*\right) dV 
%  + \int_{\Gamma_{in}} \phi^* dS
%  + \int_{\Gamma_{out}} (i k +1/R) \phi \phi^* dV
%  = 0 $$   


%%%
%%% THIS SCRIPT IS TO BE REDESIGNED AS AN ILLUSTRATION OF THE USE OF CM/PML
%%% 

%% initialisation
close all
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',2);
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
set(0,'defaultAxesFontSize',26)


%% Chapter 1 : building an adapted mesh
ffmeshInit = SF_Mesh('Mesh_1.edp','Params',10,'problemtype','acousticaxi');
Forced = SF_LinearForced(ffmeshInit,'omega',1);
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage
Forced2 = SF_LinearForced(ffmesh,'omega',2);
Forced1 = SF_LinearForced(ffmesh,'omega',.5);
ffmesh = SF_Adapt(ffmesh,Forced1,Forced2,'Hmax',2); % Adaptation du maillage




%% 
%plot the mesh :

 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 


%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 1;
Forced = SF_LinearForced(ffmesh,'omega',omega)

% This is done using the driver  SF_LinearForced. 
% If the argument 'omega' is a single value, the result of the function is
% a flowfield object containing the forced structure (see reference manual to see how it is done).

% NB : here the driver SF_LinearForced.m lauches the FreeFem++ solver 'LinearForcedAcoustic.edp'
% (hyperlinks to these files to be inserted here)



figure();
SF_Plot(Forced,'p.im','boundary','on','colormap','redblue','cbtitle','Re(p'')',...
                'YLim',[-10,20]);
hold on;
SF_Plot(Forced,'p.re','boundary','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'') and Re(p'')',...
                'YLim',[-10,20]);


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'html/AcousticTube.gif';
SF_Plot(Forced,'p','boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',1);
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'p','boundary','on','contour','on','clevels',[-2 :.5 :2],...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',Amp); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<AcousticTube.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = [-10 :.1 :10];
Uyaxis = SF_ExtractData(Forced,'uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'p',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''z)','Im(u''z)','Re(p'')','Im(p'')');
pause(0.1);



%% Chapter 3 : loop over k to compute the impedance $Z(k)$ 
IMP = SF_LinearForced(ffmesh,[0.005:.005:2],'plot','no')

% This is again done using the driver  SF_LinearForced. 
% If the argument 'omega'  is an array of values, the program returns as
% output a structure 'IMP' containing arrays for omega and Z (and possibly
% other customizable fields ; see reference manual)


%% 
% Plot $Z(k)$ 
figure;
k=1;
plot(IMP.omega,real(IMP.Z),'b',IMP.omega,imag(IMP.Z),'b--');
hold on;

title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%%
% plot in semilog
figure;
plot(IMP.omega,abs(IMP.Z),'k--');
xlabel('b'); ylabel('|Z|');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
title(['Impedance $|Z|$'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

pause(0.1);

%%
% plot reflection coefficient

figure;
semilogy(IMP.omega,IMP.R,'b--');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$R_i$','Interpreter','latex','FontSize', 30);
title(['Reflection coefficient'],'Interpreter','latex','FontSize', 30)
leg = legend('Sommerfeld','CM','PML');
leg.FontSize = 20;
set(findall(gca, 'Type', 'Line'),'LineWidth',2);


%{
%% Chapter 4 : trying better kind of boundary conditions : PML, CM

%  Warning : this part requires the SLEPC library which may not be
%  available on all systems.
%  If Slepc is available please first change value of macro EIGENSOLVER in SF_Custom.idp


IMPPML = SF_LinearForced(ffmesh,[0.01:.01:2],'BC','PML','plot','no');
IMPCM = SF_LinearForced(ffmesh,[0.01:.01:2],'BC','CM','plot','no');
IMP = SF_LinearForced(ffmesh,[0.01:.01:2],'BC','SOMMERFELD','plot','no');

%%
% Plot Z(k) real and imaginary parts
figure;
plot(IMP.omega,real(IMP.Z),'b',IMP.omega,imag(IMP.Z),'b--','DisplayName','Sommerfeld');
hold on;
plot(IMPCM.omega,real(IMPCM.Z),'r',IMPCM.omega,imag(IMPCM.Z),'r--','DisplayName','CM');
plot(IMPPML.omega,real(IMPPML.Z),'k',IMPPML.omega,imag(IMPPML.Z),'k--','DisplayName','PML');
title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
leg=legend('Sommerfeld','CM','PML');
leg.FontSize = 20;
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%%
% Plot |Z(k)| in semilog

figure;
semilogy(IMP.omega,abs(IMP.Z),'b--','DisplayName','CM');
hold on;
semilogy(IMPCM.omega,abs(IMPCM.Z),'r--','DisplayName','CM');
semilogy(IMPPML.omega,abs(IMPPML.Z),'k--','DisplayName','CM');
xlabel('b'); ylabel('|Z|');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
title(['Impedance $|Z|$'],'Interpreter','latex','FontSize', 30)
leg=legend('Sommerfeld','CM','PML');
leg.FontSize = 20;
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

pause(0.1);

%%
% plot reflection coefficient

figure;
semilogy(IMP.omega,IMP.R,'b--','DisplayName','Sommerfeld');
hold on;
semilogy(IMPCM.omega,IMPCM.R,'r--','DisplayName','CM');
semilogy(IMPPML.omega,IMPPML.R,'k--','DisplayName','PML');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$R_i$','Interpreter','latex','FontSize', 30);
title(['Reflection coefficient'],'Interpreter','latex','FontSize', 30)
leg = legend('Sommerfeld','CM','PML');
leg.FontSize = 20;
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

%}

% 
% k = [0.01:0.01:2.0];
% Z0 = 1/(2*pi);
% R = 1;
% L = 10;
% ZL = Z0*(k.^2*R^2/4 + 1i*k*0.35*R);
% Zin = Z0*(ZL.*cos(k*L)+1i*Z0*sin(k*L))./(1i*ZL.*sin(k*L)+Z0*cos(k*L))
% plot(k,-real(Zin),'k',k, -imag(Zin), 'k--');
% hold on;
% plot(IMP.k,real(IMP.Z),'b',IMP.k,imag(IMP.Z),'b--','DisplayName','Sommerfeld');
% 
% plot(k,real(Zin),'k',IMPPML.k,real(IMPPML.Z),'b');
% plot(k,-imag(Zin),'k',IMPCM.k,imag(IMPML.Z),'b');


% [[NOPUBLISH]] 