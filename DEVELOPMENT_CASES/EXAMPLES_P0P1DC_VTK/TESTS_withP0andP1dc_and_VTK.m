%% Tutorial example demonstrating the basic functionalities of the StabFem Software
% 
% this example shows how to import finite-element datasets using P0 and
% P1dc formats ; imported either as main data (from .txt file) and as aux
% data (from .ff2m file).
%
% Additonnally a few tests using paraview are included.
%

addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('create','WORK_VTK');

Ndensity =15;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity});
viscosity = 1;Umax = 1;

tot = SF_Launch('Lshape_Steady_P1dc.edp','Mesh',ffmesh,'Store','Test')

%% 
% Check plotting of P0, P1, P1dc, P2 data
% These ones are passed as auxiliary data (in .ff2m file)
%

%%
figure(1);subplot(2,2,1);SF_Plot(tot,'Tp0');subplot(2,2,2);SF_Plot(tot,'Tp1');
subplot(2,2,3);SF_Plot(tot,'Tp1dc');subplot(2,2,4);SF_Plot(tot,'Tp2');

yline = [0:0.01:1]; slice = SF_ExtractData(tot,0.45,yline);
figure(2); plot(yline,slice.Tp0,yline,slice.Tp1+.01,yline,slice.Tp1dc+.02,yline,slice.Tp2+.03)

%
% Note that when using SF_ExtractData P2 data are downgraded to P1... to be fixed
%

%%
% 
% Same for P0, P1 this time from main file (.txt)

figure(3);subplot(2,2,1);SF_Plot(tot,'Q0');subplot(2,2,2);SF_Plot(tot,'Q1');
%figure(3);subplot(2,2,1);SF_Plot(tot,'Q2');subplot(2,2,2);SF_Plot(tot,'Q2DC');subplot(2,2,3);SF_Plot(tot,'Q1DC');

yline = [0:0.01:1]; slice = SF_ExtractData(tot,0.45,yline);
figure(4); plot(yline,slice.Q0,yline,slice.Q1+.01)
%figure(4); plot(yline,slice.Q2DC,yline,slice.Q2+.01,yline,slice.Q1DC+.02)




%% 
% Check plotting of P2dc data

tot = SF_Launch('Lshape_Steady_P2dc.edp','Mesh',ffmesh,'Store','Test')

%%
% These ones are passed as auxiliary data (in .ff2m file)
%



%%
%figure(1);subplot(2,2,1);SF_Plot(tot,'Tp0');subplot(2,2,2);SF_Plot(tot,'Tp1');
%subplot(2,2,3);SF_Plot(tot,'Tp1dc');subplot(2,2,4);SF_Plot(tot,'Tp2');

%yline = [0:0.01:1]; slice = SF_ExtractData(tot,0.45,yline);
%figure(2); plot(yline,slice.Tp0,yline,slice.Tp1+.01,yline,slice.Tp1dc+.02,yline,slice.Tp2+.03)

%
% Note that when using SF_ExtractData P2 data are downgraded to P1... to be fixed
%

%%
% 
% this time from main file (.txt)

%figure(3);subplot(2,2,1);SF_Plot(tot,'Q0');subplot(2,2,2);SF_Plot(tot,'Q1');
figure(3);subplot(2,2,1);SF_Plot(tot,'Q2');subplot(2,2,2);SF_Plot(tot,'Q2DC');subplot(2,2,3);SF_Plot(tot,'Q1DC');

yline = [0:0.01:1]; slice = SF_ExtractData(tot,0.45,yline);
%figure(4); plot(yline,slice.Q0,yline,slice.Q1+.01)
figure(4); plot(yline,slice.Q2DC,yline,slice.Q2+.01,yline,slice.Q1DC+.02)


%% 
% Trying to read the binary file

fid = fopen('WORK_VTK/Data.btxt','rb');
fid = fopen('WORK_VTK/Test/FFDATA_000001.btxt','rb');
[a,b] = fread(fid,'*float')
fclose(fid);

% there should be 787 data there are 2802 ??? 

%% Summary

SF_Status

% Note that .vtu, .vtk, .btxt files have been stored in the database.
% To do now : a direct launching with paraview

