%% Tutorial example demonstrating the basic functionalities of the StabFem Software
% 
% This example is a first attempt to generate figures using parallel output.
%
% The program Lshape_Steady_parallel.edp has been modified to generate
% files by each core.
%
% To be continued :
% - Configure database to store everything
% - Find a way to implement this "transparently" with SF_Plot

addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',6);
SF_DataBase('create','WORK_Parallel');

Ndensity =15;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity});
viscosity = 1;Umax = 1;



%%  Tests for parallel formats
% 

heatS = SF_Launch('Lshape_Steady_parallel.edp','Mesh',ffmesh,'ncores',2,'Datafile','Data.txt','Store','TESTPARALLEL');

mesh0 = SFcore_ImportMesh('mesh_2_0000_0.msh')
data0 = SFcore_ImportData(mesh0,'Data_2_0000_0.txt')

mesh1 = SFcore_ImportMesh('mesh_2_0000_1.msh') 
data1 = SFcore_ImportData(mesh1,'Data_2_0000_1.txt')

figure; subplot(1,3,1);SF_Plot(data0,'T');
subplot(1,3,2);SF_Plot(data1,'T');
subplot(1,3,3);SF_Plot(data1,'T');hold on;SF_Plot(data0,'T');


% Mesh files 
%
% mesh_2_0000_0.ff2m
% mesh_2_0000_0.msh
% mesh_2_0000_0_connectivity.ff2m
% mesh_2_0000_1.ff2m -> Identical. Use a single one ?
% mesh_2_0000_1.msh
% mesh_2_0000_1_connectivity.ff2m
% 
% Data files
%
% Data_2_0000_0.ff2m
% Data_2_0000_0.txt
% Data_2_0000_0.vtk
% Data_2_0000_0.vtu
% Data_2_0000_1.ff2m -> Identical. Use a single one ?
% Data_2_0000_1.txt
% Data_2_0000_1.vtk
% Data_2_0000_1.vtu



%figure; SF_Plot(Partition,'color','title','Mesh partition')



%% Summary

SF_Status

