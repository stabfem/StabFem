function value = autorun(varargin)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE
%

close all;

if(nargin==0)
    verbosity=0;
  else
    verbosity= varargin{1};
end


ffbin = SF_core_getopt('freefemexecutable')
disp('1')
double(ffbin(end))
disp('2')

tic 
['"',ffbin,'"', ' Test_nohup.edp']
system(['"',ffbin,'"', ' Test_nohup.edp']);
a1=toc

tic
['start ' ,'"',ffbin,'"', ' Test_nohup.edp ']
system(['start ' ,'"',ffbin,'"', ' Test_nohup.edp ']);
a2= toc

a2/a1

value = (a2/a1>.5)


end
%[[AUTORUN:SHORT]]