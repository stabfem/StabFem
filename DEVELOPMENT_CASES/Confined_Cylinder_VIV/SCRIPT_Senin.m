
%% Comparison of several solvers for flow around a cylinder in a chanel
%
% We compare several solvers for a quasi-static displacement and
% a harmonically imposed displacement.


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);
SF_core_arborescence('cleanall');
mkdir('FIGURES')

%% Chapter 1 : Base flow (symmetrical) for Re = 25 ; initial mesh

    chi = 2/3.; Xmin = -5; Xmax = 7; % ,nb first case was done with Rout = 120 !
    ns = 80; % density for a fine mesh
    ffmesh = SF_Mesh('Mesh_CylinderConfined.edp','Params',[chi,Xmin,Xmax,ns],'problemtype','2D');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
  
%% 
% a few plots
figure; subplot(2,1,1); SF_Plot(bf,'mesh','xlim',[-.8 1.5]);
subplot(2,1,2); SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-.8 1.5]);

Yline = [0 :.001 :.5];
Uinlet = SF_ExtractData(bf,'ux',-4,Yline);
plot(Uinlet,Yline); %to check the parabolic profile


%% QS calculations for Re = 1 on INITIAL MESH

disp('Re = 1');
bf = SF_BaseFlow(bf,'Re',1);
omega = 1e-10;
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y') 

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')





%% QS calculations for Re = 25 ON INITIAL MESH 
disp('Re = 25');

bf = SF_BaseFlow(bf,'Re',25);

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y')

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')



 
 
 %% Adapted mesh
 bf = SF_BaseFlow(bf,'Re',1); 
 Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
 bf = SF_Adapt(bf,Mask,'Hmax',.1);

figure; subplot(2,1,1); SF_Plot(bf,'mesh','xlim',[-.8 1.5]);
subplot(2,1,2); SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-.8 1.5]);

%% QS calculations for Re = 1 on ADAPTED MESH

disp('Re = 1');
bf = SF_BaseFlow(bf,'Re',1);
omega = 1e-10;
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y') 

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')


%% QS calculations for Re = 25 ON ADAPTED MESH 
disp('Re = 25');

bf = SF_BaseFlow(bf,'Re',25);

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y')

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')

%% Forced structures for omega = 3, Re = 25

omega = 3; % we use the solver for linear harmonic problem as in Sabino et al, with a very small frequency.


% Relative velocities in relative frame

ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y') ;
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 
LineRR = SF_ExtractData(ffRR,'all',0,Yline);
figure(45);hold off;plot(Yline,real(LineRR.uya),'--r',Yline,imag(LineRR.uya),'-..r')
figure(46);hold off;plot(Yline,real(LineRR.uxa),'--r',Yline,imag(LineRR.uxa),'-..r')

ffRR.Fy

% Absolute velocities in relative frame 
ffAR = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame R -Normalize Y') ;
ffAR.Fy

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 
LineALE = SF_ExtractData(ffRR,'all',0,Yline);
figure(45);hold off;plot(Yline,real(LineALE.uya),'--r',Yline,imag(LineALE.uya),'-..r')
figure(46);hold off;plot(Yline,real(LineALE.uxa),'--r',Yline,imag(LineALE.uxa),'-..r')


% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)

ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y') ;
Uyl = SF_ExtractData(ffAA,'uy',0,Yline);Uyrl = SF_ExtractData(ffAA,'uyr',0,Yline);
LineAA = SF_ExtractData(ffAA,'all',0,Yline);
figure(45);hold on;plot(Yline,real(LineAA.uy),'--g',Yline,imag(LineAA.uy),'-..g')
legend('Uyabs,r','Uyabs,i')
figure(46);hold on;plot(Yline,real(LineAA.ux),'--g',Yline,imag(LineAA.ux),'-..g')
legend('Uxabs,r','Uxabs,i')

ffAA.Fy


 %%
 %% QS calculations for Re = 200 ON ADAPTED MESH 
disp('Re = 200');

bf = SF_BaseFlow(bf,'Re',100);
bf = SF_BaseFlow(bf,'Re',150);
bf = SF_BaseFlow(bf,'Re',200);
Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
bf = SF_Adapt(bf,Mask,'Hmax',.1);
omega = 1e-10;
% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y')

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

% Absolute velocities in absolute frame 
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y')


%% Adapt base flow and recompute

bf = SF_Adapt(bf,ffALE,'Hmax',0.15);
ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y');

    

%% SUMMARY
SF_Status


% [[PUBLISH]]

