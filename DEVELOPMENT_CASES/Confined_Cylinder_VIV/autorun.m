function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a SPRING-MOUNTED cylinder with STABFEM  

if(nargin==0) 
    verbosity=0; 
end

%if(verbosity==0)
%if ~SF_core_detectlib('SLEPc-complex')
%  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
%  value = -1;
%  return;
%end
%end


SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');
SF_core_arborescence('cleanall');


%% ##### CHAPTER 1 : EIGENVALUES FOR modes EM and FM

chi = 2/3.; Xmin = -5; Xmax = 7; % ,nb first case was done with Rout = 120 !
ns = 80; % density for a fine mesh
ffmesh = SF_Mesh('Mesh_CylinderConfined.edp','Params',[chi,Xmin,Xmax,ns],'problemtype','2D');
bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_BaseFlow(bf,'Re',25);
  
omega = 3.
% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'Options','-Vel R -Frame R -Normalize Y')

% Absolute velocities in relative frame
ffAR = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame R -Normalize Y')

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 

ffAA = SF_LinearForced(bf,'omega',omega,'Options','-Vel A -Frame A -Normalize Y');

FyValues = [ffRR.Fy ,ffAR.Fy, ffALE.Fy, ffALEe.Fy, ffAA.Fy]
FyRefs = [-90.1395 - 9.2604i, -90.1395 - 9.2604i, -90.6767 - 9.3306i, -90.6691 - 9.3305i, -90.7494 - 9.3002i]

SFerror(1) = max(abs(FyValues./FyRefs-1))

bf=SF_Split(bf);
bf = SF_BaseFlow(bf);
FxRef = 18.4054
bf.Fx

SFerror(2) = abs(bf.Fx/FxRef-1)

sfs = SF_Status;

%% BILAN
value = sum((SFerror>1e-2))

end
