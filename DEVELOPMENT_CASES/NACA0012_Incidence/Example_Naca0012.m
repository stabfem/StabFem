%% Flow around a wing profile : example for a NACA0012 profile
%
% WIP : characterization of onset of unsteady mode for variable incidence.
%

%% Starting, opening database and setting options
close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);

SF_DataBase('open','WORK_NACA0012');
SF_core_setopt('PlotBFOptions',{'vort','xlim',[-.5 2],'ylim',[-1 1],'colorrange',[-10 10],'colormap','redblue'}); 
SF_core_setopt('PlotModeOptions',{'p','xlim',[-.5 1.5],'ylim',[-.5 .5],'colorrange','cropcenter','colormap','redblue','logsat',1});
%SF_core_setopt('BFSolver','Newton_2D_SUPG.edp')
%SF_core_setopt('StabSolver','Stab_2D_SUPG.edp')
SF_core_setopt('BFSolver','Newton_2D.edp')
SF_core_setopt('StabSolver','Stab_2D.edp')


%% Generation of a mesh
mesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',0,'P',0,'XX',12});

%% Generate baseflow and increase Re up to 9000
alpha = 0;
bf = SF_BaseFlow(mesh,'alpha',alpha,'Re', 100);
for Re = [300 1000 2000 4000 7000 9000]
    bf = SF_BaseFlow(bf,'alpha',alpha,'Re', Re);
    bf = SF_Adapt(bf);
end
bf = SF_BaseFlow(bf,'alpha',alpha,'Re', Re);

%% PLOT
figure ; SF_Plot(bf,'MESH','xlim',[-1 2],'ylim',[-1 1]);
figure ; SF_PlotBF(bf);

%% Characterization of base flow : surfaces

[xi,yi,xe,ye]= Naca_Surfaces(0,0,12);
Intrados = SF_ExtractData(bf,xi,yi-1e-5);
Extrados = SF_ExtractData(bf,xe,ye+1e-5);
figure; plot(xi,Intrados.vort,xe,Extrados.vort);

%% characterization of bf : wake
Y = linspace(-.1,.1,501);
Ux065 = SF_ExtractData(bf,'ux',0.65,Y);Ux07 = SF_ExtractData(bf,'ux',0.7,Y);Ux075 = SF_ExtractData(bf,'ux',0.75,Y);
Ux08 = SF_ExtractData(bf,'ux',0.8,Y);Ux085 = SF_ExtractData(bf,'ux',0.85,Y);Ux09 = SF_ExtractData(bf,'ux',0.9,Y);
figure; plot(Ux065,Y,'k',Ux07,Y,'g',Ux07,Y,'b',Ux075,Y,'r',Ux08,Y,'g',Ux085,Y,'c',Ux09,Y,'m');xlim([-.03,.05]);ylim([-.03,.03])
legend('-.1','-.05','0','.05','.1','.15')
%%
Y = linspace(-.2,.2,501);
Uxw05 = SF_ExtractData(bf,'ux',1.25,Y);Uxw1 = SF_ExtractData(bf,'ux',1.75,Y);Uxw2 = SF_ExtractData(bf,'ux',2.75,Y);
Uxw5 = SF_ExtractData(bf,'ux',5.75,Y);Uxw10 = SF_ExtractData(bf,'ux',10.75,Y);Uxw20 = SF_ExtractData(bf,'ux',20.75,Y);
figure; plot(Uxw05,Y,'k',Uxw1,Y,'g',Uxw2,Y,'b',Uxw5,Y,'r',Uxw10,Y,'g',Uxw20,Y,'c');
%xlim([-.03,.05]);ylim([-.03,.03])
legend('.5','1','2','5','10','20','location','northwest')
%%
figure(45);plot(Y,1-Uxw10,Y,0.175*exp(-Y.^2/0.005),Y,1-Uxw5,Y,0.175*sqrt(2)*exp(-Y.^2/(0.005*.5)));


%% Eigenvalues : testing with complex mapping

%% Eigenvalues : testing with complex mapping

Params = [1.25 55 .1 .3 5 55];
[ev,em] = SF_Stability(bf,'shift',1+14.5i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','mappingdef','jet','mappingparams',Params,'Options','-isGradDiv 0 -isSUPG 0');
%[ev,em] = SF_Stability(bf,'shift',1+12i,'nev',10,'PlotSpectrum',bf.Re);

%% test with stabilisers on/off
%[ev,em] = SF_Stability(bf,'shift',1+14i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','Options','-isGradDiv 0 -isSUPG 0');
%[ev,em] = SF_Stability(bf,'shift',1+14i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','Options','-isGradDiv 1 -isSUPG 0');
%[ev,em] = SF_Stability(bf,'shift',1+14i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','Options','-isGradDiv 0 -isSUPG 1');

%% Refine mesh
bf = SF_Adapt(bf,em(1));bf = SF_BaseFlow(bf);
[ev,em] = SF_Stability(bf,'shift',1+14.5i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','mappingdef','jet','mappingparams',Params,'Options','-isGradDiv 0 -isSUPG 0');

bf = SF_Adapt(bf,em(1));bf = SF_BaseFlow(bf);
[ev,em] = SF_Stability(bf,'shift',1+14.5i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','mappingdef','jet','mappingparams',Params,'Options','-isGradDiv 0 -isSUPG 0');

%%
Params = [1.5 55 .1 -.5 5 55];% for direct
[ev,em] = SF_Stability(bf,'shift',1+14.5i,'nev',10,'PlotSpectrum',bf.Re,'Type','D','mappingdef','jet','mappingparams',Params,'Options','-isGradDiv 0 -isSUPG 0');


%bf = SF_Adapt(bf,'anisomax',4);
%bf = SF_BaseFlow(bf);
%[ev,em] = SF_Stability(bf,'shift',1+12i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','Options','-isGradDiv 1 -isSUPG 1');
%[ev,em] = SF_Stability(bf,'shift',1+12i,'nev',10,'PlotSpectrum',bf.Re,'Type','A','Options','-isGradDiv 0 -isSUPG 0');



%% Loop over Re to locate thresholf for alpha = 0
bfAns = bf;
 for Re = [6250 :-250: 5500]
     bf = SF_BaseFlow(bf,'Re',Re)
     [ev,em] = SF_Stability(bf,'shift',1+14i,'nev',10,'PlotSpectrum',true,'type','A','threshold','on','sort','LR','mappingdef','jet','mappingparams',Params,'Options','-isGradDiv 0 -isSUPG 0');
 end
 
%% plot
Retab = sfs.EIGENVALUES.Re(420:end); lambdatab = sfs.EIGENVALUES.lambda(420:end);
figure; subplot(2, 1, 1); plot(Retab,real(lambdatab),'+');ylim([-.2 .2]);xlabel('Re');ylabel('\lambda_r');grid on
subplot(2, 1, 2); plot(Retab(real(lambdatab)>-.05),-imag(lambdatab(real(lambdatab)>-.05)),'+');grid on;xlabel('Re');ylabel('\lambda_i');
saveas(gca,'lambdaRe_alpha0','png');
%  %% repeating for other values of alpha
% emcont = 10i+2;
% for alpha = 0.2 :0.2 : 2
%     bf = bfAns; % this object is the bf for Re = 7000 at previous value of alpha
%     bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Re',7000,'alpha',alpha,'Store','MISC');
%     bf = SF_Adapt(bf);
%     bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Re',7000,'alpha',alpha);
%     bfAns = bf;
%     [ev,em] = SF_Stability(bf,'shift',emcont,'nev',10,'PlotSpectrum',true,'type','A','sort','LR');
%     emcont = ev(1);
%     for Re = [6900 :-100: 5000]
%          bf = SF_BaseFlow(bf,'Re',Re,'alpha',alpha);
%         [ev,em] = SF_Stability(bf,'shift','cont','nev',10,'PlotSpectrum',true,'type','A','threshold','on','sort','LR');
%         if real(ev(1))<0
%             break
%         end
%     end
% end



%% CHAPTER XX : QS computations

%% Test small incidence (finite diff)
%
alfdeg = 0.05;
alf = alfdeg*pi/180;
bf0 = SF_Load('BASEFLOWS',6);
bfp = SF_BaseFlow(bf0,'alpha',alfdeg);
bfm = SF_BaseFlow(bf0,'alpha',-alfdeg);

Ma = (bfp.Mz-bfm.Mz)/(2*alf)
Fya = (bfp.Fy-bfm.Fy)/(2*alf)

QS = SF_Launch('QS_Wing.edp','Baseflow',bf,'Datafile','QS.txt','Store','QS')
QS.MZa
QS.Fya

%% Imp and QS version Diogo->SF 
%RES = SF_Launch('LinearForced_2D_PitchingWing.edp','Baseflow',bf0,...
%                'Options',{'omegamin',1e-4,'omegamax',1e-3,'Nomega',2},'DataFile','LinearForcedStatistics.ff2m','Store','IMP');

%QS = SF_Launch('QS_2D_PitchingWing.edp','Baseflow',bf0,'DataFile','QS_Coefs.ff2m'); 
 

%% Loop to compute QS properties 
bf = SF_Load('BASEFLOWS',8);
Rerange = logspace(log(10000)/log(10),log(100)/log(10),21);
for Re = Rerange
    bf= SF_BaseFlow(bf,'Re',Re);
    QS =SF_Launch('QS_Wing.edp','Baseflow',bf,'Datafile','QS.txt','Store','QS');
end

%%
for ii=46:66
    bf= SF_Load('BASEFLOWS',ii);
    QS =SF_Launch('QS_Wing.edp','Baseflow',bf,'Datafile','QS.txt','Store','QS');
end

%%
qs = SF_Status('QS');
figure;semilogx(   [qs.QS.Re], [qs.QS.Xf]+.25);xlabel('Re');ylabel('X_f'); grid on;
set(gcf,'Position',[100 100 500 300])
saveas(gca,'Naca0012_FoyerRe','png')
%%
figure;semilogx(   [qs.QS.Re], [qs.QS.Mza]);hold on;grid on;
semilogx(   [qs.QS.Re], [qs.QS.Mza]+(.25-.186)*[qs.QS.Fya]);
semilogx(   [qs.QS.Re], [qs.QS.Mza]+(.25-.1)*[qs.QS.Fya]);
semilogx(   [qs.QS.Re], [qs.QS.Mza]+(.25)*[qs.QS.Fya]);
xlabel('Re');ylabel('M_\alpha');legend('Xc=0.25','Xc=0.186','Xc=0.1','Xc=0');
set(gcf,'Position',[100 100 500 300])
saveas(gca,'Naca0012_MaRe','png')
%%
figure;semilogx(   [qs.QS.Re], [qs.QS.Fya]);grid on
xlabel('Re');ylabel('L_\alpha')%,F_y_,_\alpha') ; 
%hold on; plot([qs.QS.Re], [qs.QS.La])
%legend('L_\alpha')%,'F_y_,_\alpha');
set(gcf,'Position',[100 100 500 300])
saveas(gca,'Naca0012_LaRe','png')
hold on; plot([qs.QS.Re], [qs.QS.La])