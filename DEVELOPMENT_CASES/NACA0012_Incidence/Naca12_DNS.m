
%% Starting, opening database and setting options
close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);

SF_DataBase('open','WORK_NACA0012_alpha0');
SF_core_setopt('PlotBFOptions',{'vort','xlim',[-.5 2],'ylim',[-1 1],'colorrange',[-10 10],'colormap','redblue'}); 
SF_core_setopt('PlotModeOptions',{'p','xlim',[-.5 1.5],'ylim',[-.5 .5],'colorrange','cropcenter','colormap','redblue','logsat',1});
%SF_core_setopt('BFSolver','Newton_2D_SUPG.edp')
%SF_core_setopt('StabSolver','Stab_2D_SUPG.edp')
SF_core_setopt('BFSolver','Newton_2D.edp')
SF_core_setopt('StabSolver','Stab_2D.edp')

bf = SF_Load('BASEFLOWS',13)
%%
sfsall = SF_TS_Check;


%% Launch DNS
DNSOptions = {'Re',6000,'Niter',5000,'dt',0.0005,'iout',100};
Startfield = SF_Add({bf,em(1),em(10)},'Coefs',[1 .001 .001]);
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',Startfield,'Options',DNSOptions,'wait',false);
%%
Startfield = SF_Load('TimeStepping',4,5000);
DNSOptions = {'Re',6000,'Niter',100000,'dt',0.0005,'iout',1000};
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',Startfield,'Options',DNSOptions,'wait',false);

%% relaunch in same folder
Startfield = SF_Load('TimeStepping',5,10000);
DNSOptions = {'Re',6000,'Niter',100000,'dt',0.0005,'iout',1000};
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',Startfield,'Options',DNSOptions,'samefolder',true,'wait',false);

%% relaunch again
Startfield = SF_Load('TimeStepping',5,14000);
DNSOptions = {'Re',6000,'Niter',100000,'dt',0.00002,'iout',1000};
folder = SF_TS_Launch('TimeStepper_2D_Sponge.edp','Init',Startfield,'Options',DNSOptions,'samefolder',true,'wait',false);


%% Start with white noise
bf = SF_Load('BASEFLOWS',13)
Startfield = bf;
Startfield.uy = Startfield.uy+1e-3*randn(size(Startfield.uy));
DNSOptions = {'Re',6000,'Niter',100000,'dt',0.0005,'iout',1000};
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',Startfield,'Options',DNSOptions,'wait',false);

%% another one starting with a different eigenmode
em = SF_Load('EIGENMODES',87); % this one with omega = 12.4
Startfield = SF_Add({bf,em},'Coefs',[1 .001]);
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',Startfield,'Options',DNSOptions,'wait',false);
%% restarting this one
Startfield = SF_Load('TimeStepping',7,10000);
DNSOptions = {'Re',6000,'Niter',100000,'dt',0.00025,'iout',1000};
folder = SF_TS_Launch('TimeStepper_2D_Sponge.edp','Init',Startfield,'Options',DNSOptions,'wait',false,'samefolder',true);


%%
sfsall = SF_TS_Check;

%% plot
sfs = SF_TS_Check(5);
figure(200); plot(sfs.t,sfs.v2,sfs.t,sfs.v4,sfs.t,sfs.v10);hold on;
xlabel('t');ylabel('v(x,0,t)');legend('x=2','x=4','x=10')
xlim([0 7.12]);

