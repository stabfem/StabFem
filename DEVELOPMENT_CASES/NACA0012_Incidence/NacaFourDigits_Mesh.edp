// Mesh generator for four-digits NACA wing profiles
// nomenclature is NACA_MPXX
// with parameters (passed as optional arguments) :
//     M  : camber
//     P  : position of max camber
//     XX : thickness
//     R0 : dimension of mesh
//     np : density of mesh close to profile
//     next : density in external parts of mesh
//
//  EXEMPLE of USAGE : considering NACA2412 profile
//  1/ launching FreeFem directly :
//  > FreeFem++ NacaFourDigits_Mesh.edp -M 2 -P 4 -XX 12 
//  2/ using StabFem driver :
//  >> ffmesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',2,'P',4,'XX',12});
//
// This program is part of the StabFem project distributed under GNU licence.

include "StabFem.idp";

verbosity = 0;
wait=0;
real Wing=99,beta;
int  np=64*2;      
real pis180=pi/180.; // angle conversion
real eps=0.001;      // for the derivative 
real R0=50;         // size of teh domain 

cout << "Optional paramzeters detected : " << endl;
real M = getARGV("-M",0);
cout << " Camber parameter M : " << M << endl;
real P = getARGV("-P",4);
cout << " position of max. camber parameter P : " << P << endl;
real XX = getARGV("-XX",12);
cout << " thickness parameter XX : " << XX << endl;
real Xc = getARGV("-Xc",0.25); 
cout << " Location of frame origin along profile Xc = " << XX << endl;

R0 = getARGV("-R0",50);
cout << endl << " Overall dimension of mesh R0 = " << R0 << endl;
real Xmax = getARGV("-Xmax",R0);
cout << endl << " Downstream dimension of mesh Xmax = " << Xmax << endl;
np = getARGV("-np",100);
cout << "mesh density np = " << np << endl;
real next = getARGV("-next",0.5);
cout << "externalmesh density next = " << next << endl;



// def intrados/extrados
func real yt(real x){
    return 0.05*XX*(0.2969*sqrt(x)-0.1260*x 
       - 0.3516*(x^2)+0.2843*(x^3)-0.1036*(x^4));}
       
//func real intra(real x){
//  return   -0.05*XX*(0.2969*sqrt(x)-0.1260*x 
//       - 0.3516*(x^2)+0.2843*(x^3)-0.1036*(x^4));}

M = 0.01*M;
P = 0.1*P;

// def mean line
func real yp(real x){
if (x<P){return M/P^2*(2*P*x-x^2) ;}
else  { return M/(1-P)^2*((1-2*P)+2*P*x-x^2);};
};

// angle 
func real theta(real x)
{
if (x<P){return atan(2*M/P^2*(P-x)) ;}
else  { return atan(2*M/(1-P)^2*(P-x)) ;}
};


// strectching for optimal distribution of mesh points along profile       
func real stretch(real x){real C = .65; return (x^2-C*x^3)/(1-C);}       
       
// external an zoomed domains
// A. for a circular domain
//border C(t=0,2*pi) { x=R0*cos(t); y=R0*sin(t); label = 1;} 
// B. for a rectangular domain
border in(t=R0,-R0) { x=-R0; y=t; label = 1;} 
border sub(t=-R0,Xmax) { x=t; y=-R0; label = 1;}
border out(t=-R0,R0) { x=Xmax; y=t; label = 3;} 
border sup(t=Xmax,-R0) { x=t; y=R0; label = 1;}
// C. for a D-shaped domain
border inD(t=pi/2,3*pi/2) { x=R0*cos(t); y=R0*sin(t); label = 1;}  
border subD(t=0,R0) { x=t; y=-R0; label = 1;}
border outD(t=-R0,R0) { x=R0; y=t; label = 3;} 
border supD(t=R0,0) { x=t; y=R0; label = 1;}

real Rint = 4;
border c(t=0,2*pi) { x=Rint* cos(t)+.5-Xc; y= Rint*sin(t); label = 99;}       

cout << "leading edge : " << yp(0) << " ; " << yp(stretch(0))+yt(stretch(0))*cos(theta(stretch(0))) << endl;
real t = 0.001;
cout << yp(stretch(t))+yt(stretch(t))*cos(theta(stretch(t))) << " " << yp(stretch(t))-yt(stretch(t))*cos(theta(stretch(t))) << endl;
cout << "trailing edge : " << yp(1) << " ; " << yp(stretch(1))+yt(stretch(1))*cos(theta(stretch(1)))  << endl;

border Splus(t=0,1){ x = stretch(t)-Xc-yt(stretch(t))*sin(theta(stretch(t))); y = yp(stretch(t))+yt(stretch(t))*cos(theta(stretch(t))); label=2;} 
border Sminus(t=1,0){ x =stretch(t)-Xc+yt(stretch(t))*sin(theta(stretch(t))); y = yp(stretch(t))-yt(stretch(t))*cos(theta(stretch(t))); label=2;} 

   
// mesh generation   
//mesh th= buildmesh(C(np/2)+Splus(np)+Sminus(np));
//mesh Zoom = buildmesh(c(np)+Splus(np)+Sminus(np));sup(next)+in(next)+sub(next)+sub(next)+Splus(np)+Sminus(np)


plot(sup(2*R0*next)+in(2*R0*next)+sub(2*R0*next)+out(2*R0*next)+c(2*pi*Rint*next*2)+Splus(np)+Sminus(np));

// mesh generation   
mesh th= buildmesh(sup(next*R0)+in(next*R0)+out(next*R0)+sub(next*R0)+c(2*pi*Rint*next*2)+Splus(np)+Sminus(np));
mesh Zoom = buildmesh(c(np)+Splus(np)+Sminus(np));

plot(th);

// SAVE THE MESH in mesh.msh file 
savemesh(th,ffdatadir+"mesh.msh");


// FIRST AUXILIARY FILE for Stabfem : SF_Init.ff2m
{
            ofstream file(ffdatadir+"SF_Init.ff2m"); 
      file << "Defininition of problem type and geometrical parameters for StabFem. Problem type : " << endl;
      file << "2DComp" << endl;
      file << "Format :  (this list may contain geometrical parameters such as domain dimensions, etc..)" << endl;
      file << "real R real Xmin real Xmax real Rmax" << endl;
      file <<  R0  << endl << 1 << endl << 1 << endl << 1  << endl;
}


// SECOND AUXILIARY FILE  for Stabfem : mesh.ff2m
  SFWriteMesh(ffdatadir+"mesh.ff2m",th,"initial")

// AUXILIARY FILE NEEDED TO PLOT P2 and vectorized DATA
IFMACRO(SFWriteConnectivity)
	SFWriteConnectivity(ffdatadir+"mesh_connectivity.ff2m",th);
ENDIFMACRO

