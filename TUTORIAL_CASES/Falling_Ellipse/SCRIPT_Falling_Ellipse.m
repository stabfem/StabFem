%% Movement of a 2D elliptical body falling in a viscous fluid

%%
% This example is adapted from the example "NS-falling-ellipse-adapt-moving.edp" 
% from the FreeFem++ repository.

%% Chapter 0 : initialization
%
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
if (SF_core_getopt('isoctave')) 
  warning(' This script may take a very long time if using octave because reading .txt files and generating movie is very slow')
end

SF_DataBase('create','./WORK/');

%% Chapter 1 : launching the DNS

%%
% First setting the paremeters.
DNSParams.dt = 0.1; DNSParams.Niter = 200; DNSParams.theta = 20;

%%
% Launching simulation
Run1 = SF_TS_Launch('TimeStepper_NS_Falling_Ellipse.edp','Options',DNSParams);

%%
% Importing results
[TimeStats,Snapshots] = SF_TS_Check(Run1);


%% Chapter 2 : postprocessing
% Plotting the trajectory
figure;  plot(TimeStats.Xe,TimeStats.Ye);
title('trajectory of the ellipse');xlabel('x');ylabel('y');

%%
% Generating a movie
h = figure;
mkdir('html');
filename = 'html/SCRIPT_Falling_Ellipse_movie.gif';
for i=1:length(Snapshots)
    SF_Plot(Snapshots(i),'uy','colorrange',[-1.5 1.5],'title',['t = ',num2str(Snapshots(i).t)],'boundary','on');
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<SCRIPT_Falling_Ellipse_movie.gif>>
%

%% Summary

sfs = SF_Status

%%
% [[PUBLISH]]
