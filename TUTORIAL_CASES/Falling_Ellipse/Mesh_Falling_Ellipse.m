// mesh generator for a falling ellipse
// This program is adapted from "NS-falling-ellipse-adapt-moving.edp"
// and redesigned to run through StabFem.

include "StabFem.idp"

// Parameters
real L= 5, H= 20;
real hsize = 0.5;
real tx=0,ty=H-1;
real theta = getARGV("-theta",20.);
cout << " Initial inclination angle of the ellipse : " << theta << endl;
real bR = getARGV("-bR",1);
real sR = getARGV("-sR",.5);
cout << " Minor and major semiaxses : bR = " << bR << " ; sR = " << sR << endl;
// NB here only three parameters "theta,bR,sR" are settable through getARGV.
// You may modify this example for more control !

theta=pi*180.*theta; // radians
real Le = pi*(sR+bR);
real[int] XX=[-L/2,L/2, L/2,-L/2];
real[int] YY=[0,0, H,H];
border Cote(t=0,1;i) {
	int i1 = (i+1)%4;
   x = XX[i]*(1-t)+ XX[i1]*t;
   y = YY[i]*(1-t)+ YY[i1]*t;
   label =1;
}

int Ne = 2*Le / hsize;
border ellipse(t=0,2*pi){
	real xx = bR*cos(t), yy = sR*sin(t);
	label =0;
	real st = sin(theta), ct = cos(theta);
	x = tx + ct*xx- st*yy;
	y = ty + st*xx+ ct*yy;
}
int[int]  NN=[L/hsize,H/hsize, L/hsize, H/hsize];

mesh Th = buildmesh(Cote(NN)+ellipse(Ne));
plot(Th,wait=1);

savemesh(Th,ffdatadir+"mesh.msh");   /* Save mesh in .msh format */
