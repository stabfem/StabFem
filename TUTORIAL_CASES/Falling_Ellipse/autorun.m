
function value = autorun(verbosity)

% Autorun function for StabFem. 
% This function will produce sample results for the case Falling_Ellipse
%
% USAGE : 
% autorun -> automatic check
% autorun(1) -> produces the figures used for the manual


if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_DataBase('create', './WORK_AUTORUN/');

value=0;
%%
DNSParams.dt = 0.1; DNSParams.Niter = 20; DNSParams.theta = 20;
Run1 = SF_TS_Launch('TimeStepper_NS_Falling_Ellipse.edp','Options',DNSParams);
[TimeStats,Snap] = SF_TS_Check(Run1);

Xe = TimeStats.Xe(end)
XeREF = -0.0867



SFerror(1) = abs(Xe/XeREF-1)

value = sum(SFerror>1e-2)
end
%[[AUTORUN:SHORT]]