%% Learn by examples 2 : heat exchanger

%%
% This example is taken from the "learning by examples" section from the
% FreeFem++ manual. 
% We recommend that you study it after reading about it in the freefem manual, 
% and after looking at the previous example <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Membrane/SCRIPT_membrane.html SCRIPT_membrane.m>.
%
% Compared to this very first example, the present one introduces two additional functionalities :
% - Transmission of parameters through 'Options/getARGV' .
% - Usage of DataBase


%% Initialization
close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);

SF_DataBase('create','./WORK/');
%% 
% In this second example, a DataBase directory is initiated in folder
% './WORK/'.
% All result files will be placed in this folder and subfolders.
% Compare with previous example <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Membrane/SCRIPT_membrane.html SCRIPT_membrane.m> to see the difference.


%% Generation of the mesh
Ndensity =50;
ffmesh = SF_Mesh('HeatExchanger_Mesh.edp','Options',{'meshdensity',Ndensity})

%% 
% Note that here an option 'meshdensity' is passed to the mesh generator. 
% This is equivalent to lanching "FreeFem++ HeatExchanger_Mesh.edp -meshdensity 50"
% The parameter  is detected through "getARGV" within the FreeFem program.
% (see program
% <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Heat_Exchanger/HeatExchanger_Mesh.edp HeatExchanger_Mesh.edp>)
%
% See FreeFem doc about this.

%% Plot mesh
figure(1);SF_Plot(ffmesh,'mesh','title','Mesh for Heat Exchanger Problem','boundary','on');
pause(0.1);

%% Problem : HC
a1 = 1.;
Field = SF_Launch('HeatExchanger.edp', 'Mesh', ffmesh, 'Options', {'a1', a1},'store','Heat');
%%
% This is equivalent to launching "FreeFem++ HeatExchanger.edp -a1 1."
%
% Here again an option 'a1' is passed to the solver. It is also detected
% through "getARGV" within the FreeFem program.
% (see program
% <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Heat_Exchanger/HeatExchanger.edp HeatExchanger.edp>)

%
% Note the option 'store','Heat' which tells the driver that the resulting
% files will have to be displaced into folder 'Heat' of the database.

%% Plot the solution
figure(2);SF_Plot(Field,'T','title', 'Temperature of Heat Exchanger','contour', 'on');
hold on; plot([-1 1],[.5,.5],'r--')
hold off;

%% Demonstation of database 
% 
sfs = SF_Status

%%
% we can see that the files 'mesh.msh' and 'Data.txt' have been renamed and
% displaced in folders 'MESHES' and 'Heat'.

%%
% [[PUBLISH]] -> tag for publication on website
