%% Tutorial example demonstrating how to use StabFem for a time-stepping calculation (BASIC)
% 
% This tutorial script is designed as a support for chapter 4 of the
% StabFem manual available <https://gitlab.com/stabfem/StabFem/-/jobs/artifacts/master/file/99_Documentation/MANUAL/main.pdf?job=makepdf here> 
% 
% We show how to compute the solution of a time-dependant through time-stepping and
% how to import, plot, and organise the results in a database.
%
% This example is based on the "Thermal Conduction" example in the FreeFem manual.
% The mesh generator is
% <https://gitlab.com/stabfem/StabFem/blob/master/TUTORIAL_CASES/Unsteady_Thermal_Conduction/Mesh_ThermalConduction.edp Mesh_ThermalConduction.edp>
% and the freefem solver is
% <https://gitlab.com/stabfem/StabFem/blob/master/TUTORIAL_CASES/Unsteady_Thermal_Conduction/TimeStepper_ThermalConduction.edp TimeStepper_ThermalConduction.edp>
%  
% In this example the database management is disabled so that the steps of
% the computation can be followed easily. On the other hand in this mode
% only one computation can be launched, if you launch a new on the previous
% results will not be kept.
% 
% After studying this example it is recommended to go to the next example 
% <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Unsteady_Thermal_Conduction/SCRIPT_ThermalConduction_Advanced.html SCRIPT_ThermalConduction_Advanced.m> 
% which shows how to manage several simulations using the database.

%% Initialization
close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4); 

SF_DataBase('disable');
% this is to disable the database management for this simple tutorial.

%% Chapter 1 : creating mesh 
%
% As usual, a mesh is created by calling a  FreeFem++ mesh-creator program.
ffmesh = SF_Mesh('Mesh_ThermalConduction.edp');


%% 
% Plot this mesh
figure; SF_Plot(ffmesh);

%% Chapter 2 : Launching a simple time-stepping simulation

%% 
% This is done in two steps : a/ launching the code
Run1 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1});

%%
% Before importing the results let's check the content of the working
% directory to see the files created:

ls

%%
% Note that the FreeFem++ solver will create a series of files
% 'Snapshot_#######.txt / .ff2m' corresponding to the snapshots 
% plus a couple of file 'TimeStats.txt/.ff2m' corresponding to
% time-statistics.
%
% Note that in the no-database mode, all output files are written in working directory './' 

%%
% b/ Checking the contents
[stats,snapshots] = SF_TS_Check(Run1)

%% 
% The results are imported as object which can be used simply for plotting
% and postprocessing.


%% Plotting a few snapshots;

figure; 
subplot(3,1,1); SF_Plot(snapshots(1),'T','title','Field at t=1','contour','on');
subplot(3,1,2); SF_Plot(snapshots(2),'T','title','Field at t=2','contour','on');
subplot(3,1,3); SF_Plot(snapshots(4),'T','title','Field at t=4','contour','on');

%% Plotting time-series of T(0.5,3) 
figure; plot(stats.t,stats.u3);xlabel('t');ylabel('T(3,.5)');


%%
% [[PUBLISH]] -> this tag is here to tell gitlab to automatically generate a web page from this script 
 
