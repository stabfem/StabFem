
function value = autorun(verbosity)

% Autorun function for StabFem. 
% This function will produce sample results for the case EXAMPLE_Lshape
%
% USAGE : 
% autorun -> automatic check
% autorun(1) -> produces the figures used for the manual

if ~SF_core_getopt('isoctave')&&~isempty(getenv('CI_RUNNER_TAGS'))
  SF_core_log('w', 'Autorun skipped with Matlab when using runner');
  value = -1;
  return;
end


 isfigures=0;
 
if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_DataBase('create', './WORK_AUTORUN/');

value=0;


%% Chapter 1 : creating mesh
ffmesh = SF_Mesh('Mesh_ThermalConduction.edp')


%% Chapter 2 : Launching a simple time-stepping simulation
disp('#### Test 1');
folder = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',true);
[stats,snapshots] = SF_TS_Check(folder);

REF = 50.5100
stats.u3(10)

SFerror(1)= abs(stats.u3(10)/REF-1)


%
%% Chapter 3 : Starting a second time-stepping in a new folder (default case), with a diffent value of physical parameter alpha
disp('#### Test 2');
folder2 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.25});
stats2 = SF_TS_Check(folder2);

REF = 38.2451
stats2.u3(10)
SFerror(2) = abs(stats2.u3(10)/REF-1)


%% Chapter 3bis : Restarting the first simualtion, doing 100 more time steps, writing results in same folder
disp('#### Test 3');
tic 
folder3 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.1},'samefolder',true);
toc
stats3 = SF_TS_Check(folder);

REF = 50.5100
stats3.u3(10)
SFerror(3) = abs(stats3.u3(10)/REF-1)

%% Chapter 4 ; Now in non-interactive mode. 
tic
folder = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',false);
toc

system('ps -ax | grep Free')

pause(10);

[stats4,snapshots4] = SF_TS_Check(folder);
REF = 50.5100
stats4.u3(10)
SFerror(4) = abs(stats4.u3(10)/REF-1)


%% chapter 5 : testing emergency stop

folder = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',5000,'alpha',.1},'wait',false);
system('ps -ax | grep Free')
pause(1);
SF_TS_Stop(folder);
pause(1);
[stats5,snapshots5] = SF_TS_Check(folder);

stats5

SFerror(5) = ~(isfield(stats5,'t')&&(length(stats5.t)<1000))

%% Checking the whole content
ss = SF_TS_Check

value = sum(SFerror>1e-3)
end
%[[AUTORUN:SHORT]]