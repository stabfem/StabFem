%% Tutorial example demonstrating how to use StabFem for a time-stepping calculation (ADVANCED)
% 
% This tutorial script is designed as a support for chapter 4 of the
% StabFem manual available <https://gitlab.com/stabfem/StabFem/-/jobs/artifacts/master/file/99_Documentation/MANUAL/main.pdf?job=makepdf here> 
% 
% We show how to compute the solution of a time-dependant through time-stepping and
% how to import, plot, and organise the results in a database.
%
% This example is based on the "Thermal Conduction example in the FreeFem manual".
%  
% In a previous example called  <https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Unsteady_Thermal_Conduction/SCRIPT_ThermalConduction_Basic.html SCRIPT_ThermalConduction_Basic.m>  the basic
% usage was demonstrated. 
% 
% In this second example we show more advanced options :
% * Using database manager to monitor several time-stepping simulations,
% * Using non-interactive mode to launch very long simulations and check results in a second step. 
%   
% The mesh generator is
% <https://gitlab.com/stabfem/StabFem/blob/master/TUTORIAL_CASES/Unsteady_Thermal_Conduction/Mesh_ThermalConduction.edp Mesh_ThermalConduction.edp>
% and the freefem solver is
% <https://gitlab.com/stabfem/StabFem/blob/master/TUTORIAL_CASES/Unsteady_Thermal_Conduction/TimeStepper_ThermalConduction.edp TimeStepper_ThermalConduction.edp>



%% Initialization
close all;
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB']);

SF_Start('verbosity',4); 
% higest levels of verbosity results in more messages.
% recommended values are 2 (minimal notice) and 4 (notice + freefem output). 6 and more is debug mode.

SF_DataBase('open','./WORK/'); 
% this is to initialize the database management in a folder called 'WORK'. 
% NB you may use 'create' instead of 'open' to clean up

%% Chapter 1 : creating mesh
ffmesh = SF_Mesh('Mesh_ThermalConduction.edp');

%% 
% Plot this mesh
figure; SF_Plot(ffmesh);

%% Chapter 2 : Launching a simple time-stepping simulation
Run1 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',true);
%%
% Contrary to the basic example where DataBae management was disabled, the
% output results are not written in the current working folder, but in a
% subfolder 'TimeStepping/Run1' of the 'Database' folder. 
%
% Let's check this :
dir('WORK/TimeStepping/RUN000001')

% Now importing the contents just as in previous basic example. 
[stats,snapshots] = SF_TS_Check(Run1);

%% Plotting a few snapshots;

figure; 
subplot(3,1,1); SF_Plot(snapshots(1),'T','title','Field at t=1','contour','on');
subplot(3,1,2); SF_Plot(snapshots(2),'T','title','Field at t=2','contour','on');
subplot(3,1,3); SF_Plot(snapshots(4),'T','title','Field at t=4','contour','on');

%% Plotting time-series of T(0.5,3) 
figure; plot(stats.t,stats.u3);xlabel('t');ylabel('T(3,.5)');


%% Chapter 3 : Starting a second time-stepping in a new folder, with a diffent value of physical parameter alpha
Run2 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.25});
stats2 = SF_TS_Check(Run2);

%% Chapter 3bis : Restarting the first simualtion, doing 100 more time steps, writing results in same folder
Run3 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.1},'samefolder',true);
stats3 = SF_TS_Check(Run3);

%% Chapter 4 : lanching a simulation in non-interactive mode
Run4 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',false);
%%
% In this way the simulation will be launched as a remote job, so that you
% can continue to work in Matlab/Octave. 
% 
% Here the simulations will take less than 10 seconds. For demonstration,
% let's wait for 10 seconds and check the status of the simulation:

pause(10);
[stats4,snapshots4] = SF_TS_Check(Run4);


%% Chapter 5 : how to stop a simulation.
% Suppose you lauch a simulation in the way : 
Run5 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',1e4,'alpha',.1},'wait',false);

% ... and then fter 1 second realize OOPS TOO MANY TIME STEPS ! Here is the emergency stop procedure :
pause(1);
SF_TS_Stop(Run5);

%now checking what has been done :
pause(1);
[stats5,snapshots5] = SF_TS_Check(Run5);


%% Checking the whole content
ss = SF_TS_Check

% plot
figure;
plot(ss(1).t,ss(1).u3,'r',ss(2).t,ss(2).u3);
legend('run 1 : alpha = .1','run 2 : alpha = 0.25 restarted from run 1')

% [[PUBLISH]] -> this tag is here to tell the git runner to automatically generate a web page from this script 
 
