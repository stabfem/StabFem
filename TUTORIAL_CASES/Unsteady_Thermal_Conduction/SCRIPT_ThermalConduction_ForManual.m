%% Tutorial example demonstrating how to use StabFem for a time-stepping simulations

%% Initialization
close all;
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB']);
SF_Start('verbosity',3); 
SF_DataBase('create','./WORK/'); 

%% Chapter 1 : creating mesh
ffmesh = SF_Mesh('Mesh_ThermalConduction.edp');

%% Chapter 2 : Launching a simple time-stepping simulation
% a/ Launching simulation
Run1 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',true);
% b/ importing 
[stats,snapshots] = SF_TS_Check(Run1)

% Starting a second time-stepping in a new folder, with a diffent value of physical parameter alpha
Run2 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.25});
stats2 = SF_TS_Check(Run2);

% Chapter 3bis : Restarting the first simualtion, doing 100 more time steps, writing results in same folder
Run3 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Init',snapshots(end),'wait',true,'Options',{'Niter',100,'alpha',.1},'samefolder',true);
stats3 = SF_TS_Check(Run3);

% Chapter 4 : lanching a simulation in non-interactive mode
Run4 = SF_TS_Launch('TimeStepper_ThermalConduction.edp','Mesh',ffmesh,'Options',{'Niter',100,'alpha',.1},'wait',false);
% (wait for 10 seconds and come back...)
pause(10);
[stats4,snapshots4] = SF_TS_Check(Run4);


%% Checking the whole content
ss = SF_TS_Check

% plot
figure;
plot(ss(1).t,ss(1).u3,'r',ss(2).t,ss(2).u3);
legend('run 1 : alpha = .1','run 2 : alpha = 0.25 restarted from run 1')

