#!/bin/bash
# This example shows how to launch a TS simulation directly from bash, 
# in a way allowing postprocessing with StabFem.
# This example assumes that the example "SCRIPT_ThermalConduction_Advanced.m" has been run, 
# that 4 runs are already present, and will launch a fifth one in folder RUN000005

mkdir ./WORK/TimeStepping/RUN000005
echo 'macro ffdatadir() "./WORK/TimeStepping/RUN000005/" //EOM' > SF_AutoInclude.idp
echo 'macro themeshfilename() "./WORK/MESHES/FFMESH_000001.msh" //EOM' >> SF_AutoInclude.idp
echo 'macro theinitfilename() "./WORK/TimeStepping/RUN000001/Snapshot_00000100.txt" //EOM' >> SF_AutoInclude.idp
FreeFem++-mpi -nw -v 0 TimeStepper_ThermalConduction.edp  -Niter 100 -alpha 0.25  -i0 100 -t0 10 
echo 'Simulation launched directly from shell, outside StabFem' > ./WORK/TimeStepping/RUN000001/Simulation.log
echo 5 > ./WORK/TimeStepping/.counter # advised to increment this counter for good operation 