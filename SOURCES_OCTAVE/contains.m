
function retval = contains (input1, input2)
%% Function not available in the Octave core and added here

  retval = ~isempty(strfind(input1,input2));

end

