%% Initialization
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',4)

close all;



%% Building a mesh (with symmetry conditions)
Rint = 1;
Rout = 1.13662;
L = 1;
density= 50;
BC = 5; % 5 for symmetry boundary conditions at z = 0, L
mesh = SF_Mesh('Mesh_TaylorCouette.edp','Params',[Rint Rout L density BC],'problemtype','axixrswirl');
figure; SF_Plot(mesh);



%% compute a base flow
Re = 120; % Reynolds based on angular velocity of inner cylinder and gap.
bf = SF_BaseFlow(mesh,'Re',1); % first "guess" at low Re
bf = SF_BaseFlow(bf,'Re',Re);


%% plotting the base flow

figure; SF_Plot(bf,'uphi');

Rtab = [Rint:.002:Rout];
Uphitab = SF_ExtractData(bf,'uphi',0,Rtab);
Uphitheo = (Rtab./Rout-Rout./Rtab)/(Rint/Rout-Rout/Rint);
figure; plot(Rtab,Uphitab,'b',Rtab,Uphitheo,':r');
legend('Uphi(r)','theory');

%% Stability computation
% For Re = 120 we expect stability
[ev,em] = SF_Stability(bf,'m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);


% for Re = 130 we expect instability
bf = SF_BaseFlow(bf,'Re',130);
[ev,em] = SF_Stability(bf,'m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);
% ev contains the eigenvalues
% em is an array containing thez eigenvectors
% to plot them :
%%
figure; SF_Plot(em(1),'uphi');hold on;SF_Plot(em(1),{'ux','ur'}); 


%% Chapter 2 : let us try with "wall" lateral BC
Rint = 1;
Rout = 1.13662;
L = 1;
density= 50;
BC = 22; % 22 for "wall"
mesh = SF_Mesh('Mesh_TaylorCouette.edp','Params',[Rint Rout L density BC],'problemtype','axixrswirl');
figure; SF_Plot(mesh);

%% compute a base flow
Re = 100; % Reynolds based on angular velocity of inner cylinder and gap.
bf = SF_BaseFlow(mesh,'Re',1); % first "guess" at low Re
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',50);
bf = SF_BaseFlow(bf,'Re',75);
bf = SF_BaseFlow(bf,'Re',Re);

%% plotting the base flow

figure; SF_Plot(bf,'uphi');hold on; SF_Plot(bf,{'ux','ur'})


%% Stability computation
% For Re = 100 we expect stability
[ev,em] = SF_Stability(bf,'m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);


%% let us try to continue towards Re = 130
% try smaller steps ??? 

bf = SF_BaseFlow(bf,'Re',105);
bf = SF_BaseFlow(bf,'Re',110);
bf = SF_BaseFlow(bf,'Re',120);
bf = SF_BaseFlow(bf,'Re',130);


%% Stability computation
% For Re = 130 we expect instability
[ev,em] = SF_Stability(bf,'m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);
figure; SF_Plot(em(1),'uphi');hold on;SF_Plot(em(1),{'ux','ur'}); 


%% Chapter 3 : let us try with "periodic" lateral BC
Rint = 1;
Rout = 1.13662;
L = 1;
density= 50;
BC = 41; % 41 for "periodic"
mesh = SF_Mesh('Mesh_TaylorCouette.edp','Params',[Rint Rout L density BC],'problemtype','axixrswirl');
figure; SF_Plot(mesh);

%% compute a base flow
Re = 100; % Reynolds based on angular velocity of inner cylinder and gap.
bf = SF_BaseFlow(mesh,'Re',1); % first "guess" at low Re
bf = SF_BaseFlow(bf,'Re',Re);

%% plotting the base flow

figure; SF_Plot(bf,'uphi');hold on; SF_Plot(bf,{'ux','ur'})



%% Stability computation
[ev,em] = SF_Stability(bf,'m',1,'shift',0.1,'nev',20,'PlotSpectrum',true);
figure; subplot(2,1,1);
SF_Plot(em(1),'uphi');hold on;SF_Plot(em(1),{'ux','ur'}); 
subplot(2,1,2);
SF_Plot(em(2),'uphi');hold on;SF_Plot(em(1),{'ux','ur'}); 

%bf = SF_Adapt(bf,em(1)) % but this remains to be debugged in periodic case!

