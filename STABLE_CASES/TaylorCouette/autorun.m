function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case TaylorCouette
%
%[[AUTORUN:PERIODIC]]
if(nargin==0) 
   verbosity=0;
end

if SF_core_getopt('isoctave')
    value = -1;
    return
end

if SF_core_getopt('ffversion')<4
    disp('SKIPPED BECAUSE DETECTED FREEFEM VERSION 3');
    value = -1;
    return
end

if ~SF_core_detectlib('SLEPc-complex')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end



SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
SF_core_setopt('BFSolver','Newton_AxiSWIRL.edp');

SF_core_arborescence('cleanall')

value = 0;




%% Chapter 3 : let us try with "periodic" lateral BC
Rint = 1;
Rout = 1.13662;
L = 1;
density= 50;
BC = 41; % 41 for "periodic"
mesh = SF_Mesh('Mesh_TaylorCouette.edp','Params',[Rint Rout L density BC]);
figure; SF_Plot(mesh);

%% compute a base flow
Re = 130; % Reynolds based on angular velocity of inner cylinder and gap.
bf = SF_BaseFlow(mesh,'Re',1,'solver','Newton_AxiSWIRL.edp'); % first "guess" at low Re
bf = SF_BaseFlow(bf,'Re',Re,'Omegax',0); % NB omegax is rotation rate of outer cylinder ; the other inner is 1

bf.Torque
TorqueREF = 0.0585

SFerror(1) = abs(bf.Torque/TorqueREF-1)

%bfA = SF_Adapt(bf,'Splitin2',true); % test this option here
%bf = SF_BaseFlow(mesh,'Re',1); % first "guess" at low Re
%bf = SF_BaseFlow(bf,'Re',Re);
%bfB = SF_Adapt(bf,'Splitbedge',true);

%bf = SF_BaseFlow(mesh,'Re',1); % first "guess" at low Re
%bf = SF_BaseFlow(bf,'Re',Re);
%bfC = SF_Adapt(bf,'keepbackvertices',true,'Hmax',0.05,'recompute',false);
%figure; subplot(4,1,1); SF_Plot(bf,'mesh');subplot(4,1,2); SF_Plot(bfA,'mesh');subplot(4,1,3); SF_Plot(bfB,'mesh');subplot(4,1,4); SF_Plot(bfC,'mesh');

%% Stability computation
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);
bf = SF_Adapt(bf,em(1),'Hmax',0.1);%,'splitin2',true) 
bf = SF_BaseFlow(bf,'solver','Newton_AxiSWIRL.edp'); % normally it should not be necessary to specify the solver ???

[ev,em] = SF_Stability(bf,'m',0,'shift',0.1,'nev',20,'PlotSpectrum',true);


evREF = 0.115789

SFerror(2) = abs(ev(1)/evREF-1)

figure; SF_Plot(bf,'mesh')

value = sum(abs(SFerror)>1e-2)

%end
