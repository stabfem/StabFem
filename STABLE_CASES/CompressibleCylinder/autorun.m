function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a cylinder with STABFEM  
%  - Base flow computation
%  - Linear stability
%  - WNL
%  - Harmonic Balance
%
% USAGE : 
% autorun -> automatic check (non-regression test). 


if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('eigensolver','SLEPC');

if(verbosity==0)
if ~SF_core_detectlib('SLEPc-complex')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end
if strcmp(SF_core_getopt('platform'),'mac')
  SF_core_log('w', 'Autorun skipped because you are on a mac');
  value = -1;
  return;
end
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
%SF_core_arborescence('cleanall');

format long;


%% Chapter 0 : reference values for non-regression tests
np_REF = 2967;
Fx_REF =  0.695888000000000;
ev_REF =   0.013466100000000 + 0.735083000000000i;
omegac_REF = 0.730835;
wnlLambdar_REF = 8.864900000000000;
EnergyA1_REF = 0.260391000000000;
EnergyA2_REF = 0.016009400000000;


%% ##### CHAPTER 1 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE

disp('##### autorun test 1 : mesh and BASE FLOW');
bf = SmartMesh; 
bf=SF_BaseFlow(bf,'Re',50,'Mach',0.1,'solver','Newton_2D_Comp_Sponge.edp');
% here use 'S' for mesh M2 (converged results for all quantities except for A_E , but much faster
% or 'D' for mesh M4 (converged results for all quantities, but much slower)
bf.Fx
bf.mesh.np

SFerror(1) = abs(bf.Fx/Fx_REF-1)  %+abs(bf.mesh.np/np_REF-1)


%%  CHAPTER 2 : linear mode for Re=50


disp('##### autorun test 2 : LINEAR MODE');
bf=SF_BaseFlow(bf,'Re',50);

bf.mesh = SF_SetMapping(bf.mesh, 'MappingType', 'box', 'MappingParams',...
                       [-20,30,-20,20,-0.3,5,-0.3,5]); % Complex mapping

[ev,em] = SF_Stability(bf,'shift',+.75i,'nev',1,'type','D');

ev
SFerror(2) = abs(ev/ev_REF-1)

%%  CHAPTER 3 : Threshold
Ma=0.1;
bf=SF_BaseFlow(bf,'Re',46.5,'Mach',Ma);
[ev,em] = SF_Stability(bf,'shift',0.05+0.725i,'nev',5,'type','D','Symmetry','A');
[evA,emAdj] = SF_Stability(bf,'shift',ev(1),'nev',1,'type','A');
Rec=bf.Re; Mac=bf.Mach; Omegac=imag(ev); ReT=Rec+0.1;
SFerror(3) = abs(imag(ev(1))/omegac_REF-1)

%% total error
value = sum((SFerror>1e-2))


end 

