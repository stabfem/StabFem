bf = SF_Load('BASEFLOWS',19);

%%
Mask1 = SF_Mask(bf,'rectangle',[.5 1.5 0.6 0.9, .05]);
figure ; SF_Plot(Mask1,'Maskx');

%%
Mask2 = SF_Mask(bf,'ellipse',[1.5, .3,  .5, 0.25, .05]);
figure ; SF_Plot(Mask2,'Maskx');

%% 
Mask3 = SF_Mask(bf,'trapeze',[2.5 3.8 0.3 0.7 -5 5 .05]);
 figure ; SF_Plot(Mask3,'Maskx');

 %% 
bf = SF_Adapt(bf,Mask1,Mask2,Mask3,'Hmax',0.2); 
figure ; SF_Plot(bf,'mesh'); 