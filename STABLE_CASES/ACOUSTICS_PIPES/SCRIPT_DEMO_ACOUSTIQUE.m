%% Acoustic field in a pipe with harmonic forcing at the bottom
%
% This scripts demonstrates the efficiency of StabFem for a linear
% acoustics problem.
%
% Problem : find the velocity potential $\phi$ such as :
%
% * $\Delta \phi + k^2 \phi = 0$
% * $u_z = \partial_z \phi = 1$ along $\Gamma_{in}$
% * Sommerfeld radiation condition on $\Gamma_{out}$ 
% ( $k = \omega c_0$ is the acoustic wavenuber) 
%
% 
% Variational formulation :
%
% $$ \int \int_\Omega \left( \nabla \phi \cdot \nabla \phi^* + k^2 \phi \phi^*\right) dV 
% + \int_{\Gamma_{in}} \phi^* dS
% + \int_{\Gamma_{out}} (i k +1/R) \phi \phi^* dV
% = 0 $$   
%
% Resolution is made by the FreeFem++ solver
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/SOURCES_FREEFEM/LinearForcedAcoustic.edp LinearForcedAcoustic.edp>
% which is interfaced by the driver <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/SOURCES_MATLAB/SF_LinearForced.m  SF_LinearForced.m>
% 
% Additional results on this situation can be found in two more advanced
% examples available in the StabFem project :
%
% - <https://stabfem.gitlab.io/StabFem/STABLE_CASES/ACOUSTICS_PIPES/SCRIPT_PIPE_Comparison_theory.html
% SCRIPT_PIPE_Comparison_theory.m> for comparison of impedances with theory
% 
% - <https://stabfem.gitlab.io/StabFem/develop/DEVELOPMENT_CASES/ACOUSTICS_PIPES_CM/SCRIPT_ACOUSTIC_PML_CM.html
% SCRIPT_ACOUSTIC_PML_CM.m> for demonstration of how to use PML and CM methods for nonreflective boundary conditions


%% initialisation
close all
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',2); 

set(groot, 'defaultAxesTickLabelInterpreter','latex');  % options for figures
set(groot, 'defaultLegendInterpreter','latex'); %idem
set(0,'defaultAxesFontSize',20); % idem
mkdir('html');


%% Chapter 1 : building an adapted mesh
%
% (See explanations in next chapter about usage of 'Harmonic_Acoustic.edp')

ffmeshInit = SF_Mesh('Mesh_1.edp','Params',10);
Forced = SF_Launch('Harmonic_Acoustic.edp','Options',{'omega',1},'mesh',ffmeshInit,'DataFile','ForcedFlow.txt','Store','FORCEDFLOWS');
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage
Forced1 = SF_Launch('Harmonic_Acoustic.edp','Options',{'omega',.5},'mesh',ffmesh,'DataFile','ForcedFlow.txt','Store','FORCEDFLOWS');
Forced2 = SF_Launch('Harmonic_Acoustic.edp','Options',{'omega',2},'mesh',ffmesh,'DataFile','ForcedFlow.txt','Store','FORCEDFLOWS');
ffmesh = SF_Adapt(ffmesh,Forced1,Forced2,'Hmax',2); % Adaptation du maillage

%% 
%plot the mesh :

 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 


%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 1;
Forced = SF_Launch('Harmonic_Acoustic.edp','Options',{'omega',omega},'mesh',ffmesh,'DataFile','ForcedFlow.txt','Store','FORCEDFLOWS');

% We use the program Harmonic_Acoustic.edp, which computes the solution of the Helmholtz equation
% with forcing at the bottom of the pipe, and writes the results in a file 'ForcedFlow.txt'. 
% The program is interfaced using the generic driver SF_Launch, the result is a flowfield object 
% containing the forced structure.

% NB : alternatively you can use the driver SF_Linear forced whose syntax is as follows:
% > Forced = SF_LinearForced(ffmesh,'omega',omega,'solver','LinearForcedAcoustic.edp')
% This syntax is depreciated, and it is now recommended to use SF_Launch for single-omega computations.


figure();
SF_Plot(Forced,'p.im','boundary','on','colormap','redblue','cbtitle','Re(p'')',...
                'YLim',[-10,20]);
hold on;
SF_Plot(Forced,'p.re','boundary','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'') and Re(p'')',...
                'YLim',[-10,20]);


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'html/SCRIPT_DEMO_ACOUSTIQUE_movie.gif';
SF_Plot(Forced,'p','boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',1);
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'p','boundary','on','contour','on','clevels',[-2 :.5 :2],...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',Amp); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<SCRIPT_DEMO_ACOUSTIQUE_movie.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = [-10 :.1 :10];
Uyaxis = SF_ExtractData(Forced,'uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'p',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''z)','Im(u''z)','Re(p'')','Im(p'')');
pause(0.1);



%% Chapter 3 : loop over k to compute the impedance $Z(k)$ 
%
% We use the same program "Harmonic_Acoustic.edp" but with a different set
% of options. In this case the  'DataFile' generated at the end is called
% "LinearForcedStatistics.ff2m". It is imported and returned as an output a structure 'IMP' 
% containing arrays for omega and Z (and possibly other customizable fields ; see reference manual)

IMP = SF_Launch('Harmonic_Acoustic.edp','Options',{'omegamin',0,'omegamax',2,'Nomega',401},'mesh',ffmesh,'DataFile','LinearForcedStatistics.ff2m','Store','IMPEDANCES')

%%
% Nb : alternatively you can use the driver "SF_LinearForced" to do the
% same thing, with a different syntax :
% IMP = SF_LinearForced(ffmesh,'solver','LinearForcedAcoustic.edp','omega',[0.005:.005:2],'plot','no')



%% 
% Plot $Z(k)$ 
figure;
k=1;
plot(IMP.omega,real(IMP.Z),'b',IMP.omega,imag(IMP.Z),'b--');
hold on;

title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%%
% plot in semilog
figure;
plot(IMP.omega,abs(IMP.Z),'k--');
xlabel('b'); ylabel('|Z|');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
title(['Impedance $|Z|$'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

pause(0.1);

%%
% plot reflection coefficient

figure;
semilogy(IMP.omega,IMP.R,'b--');
xlabel('$k$','Interpreter','latex','FontSize', 30);
ylabel('$R_i$','Interpreter','latex','FontSize', 30);
title(['Reflection coefficient'],'Interpreter','latex','FontSize', 30)
leg.FontSize = 20;
set(findall(gca, 'Type', 'Line'),'LineWidth',2);


%% SUMMARY and demonstration of database manager
%
% check what is available in the database :

sfs = SF_Status

% demo how to load direcly the last field computed at chapter 2:
Forced = SF_Load('FORCEDFLOWS','last')

% demo how to load direcly the impedances computed at chapter 3:
Imp = SF_Load('IMPEDANCES',1)

% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
