function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results 
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests
%
% [[AUTORUN:PERIODIC]] % tag to launch this autorun through gitlab runner
%
  
if(nargin==0) 
    verbosity=0; 
end

SF_core_setopt('verbosity', verbosity);
SF_DataBase('create', './WORK_AUTORUN/');
SF_core_setopt('BFSolver','Newton_2D.edp')

LsurD = 2;
ffmesh = SF_Mesh('Mesh_Cylinder_Periodic.edp','Params',LsurD)    

bf = SF_BaseFlow(ffmesh,'Re',1,'Omegax',.5,'solver','Newton_2D.edp');
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'Re',60,'Omegax',.5,'solver','Newton_2D.edp');% normally it should not be necessary to specify the solver ???
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf);

figure ; SF_Plot(bf,'ux','xlim',[-2 2])

Fx_REF = bf.Fx
SFerror(1) = abs(bf.Fx/Fx_REF-1)
% warning Fy is not working ??? to debug

[ev,em] = SF_Stability(bf,'nev',10,'shift',2i,'solver','Stab_2D.edp');
ev(1)
evref = 0.1414 + 1.9257i

SFerror(2) = abs(ev(1)/evref-1)


figure ; SF_Plot(em(1),'vort','xlim',[-2 2],'colorrange','cropcenter')


value = sum((SFerror>1e-2))

end
