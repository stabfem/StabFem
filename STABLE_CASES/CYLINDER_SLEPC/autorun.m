function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a cylinder with STABFEM  
%
% COMPARISON BETWEEN SLEPC AND ARPACK SOLVERS FOR EIGENVALUE COMPUTATION
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests
% autorun(1) -> produces the figures (in present case not yet any figures)

if(nargin==0) 
    verbosity=0; 
end

if(verbosity==0)
   SF_core_log('w', 'Autorun skipped because i''m tired of debugging');
   value = -1;
   return;
end

if ~SF_core_detectlib('SLEPc-complex','bin','FreeFem++-mpi')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
SF_core_arborescence('cleanall');


format long;


%% Chapter 0 : reference values for non-regression tests
np_REF = 2193;
Fx_REF =  0.6435;
ev_REF =   0.047311300000000 + 0.743981000000000i;
evA_REF = conj(ev_REF);



%% ##### CHAPTER 1 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE

disp('##### autorun test 1 : mesh and BASE FLOW');

type='S';
dimensions = [-40 80 40];


ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',dimensions,'problemtype','2D');
bf=SF_BaseFlow(ffmesh,'Re',1);

bf=SF_BaseFlow(bf,'Re',10);
bf=SF_BaseFlow(bf,'Re',60);
bf=SF_Adapt(bf,'Hmax',5);
bf=SF_Adapt(bf,'Hmax',5);

%bf=SF_BaseFlow(bf,'Re',60,'ffbin','/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++','type','NEW');
%bf=SF_BaseFlow(bf,'Re',60,'ffbin','FreeFem++','type','NEW');
SFerror(1) = abs(bf.Fx/Fx_REF-1)

%bf=SF_BaseFlow(bf,'Re',60,'ffbin','/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++-mpi','type','NEW');
bf=SF_BaseFlow(bf,'Re',60,'ffbin','FreeFem++-mpi','type','NEW');
SFerror(2) = abs(bf.Fx/Fx_REF-1)

%bf=SF_BaseFlow(bf,'Re',60,'ffbin','/usr/local/ff++/openmpi-2.1/3.61-1/bin/ff-mpirun -np 1','type','NEW');
%SFerror(3) = abs(bf.Fx/Fx_REF-1)+abs(bf.mesh.np/np_REF-1)




disp('');disp('#################################');
disp('#################################');disp('');
disp('              DIRECT / offset shift = 0.01 ');
disp('');disp('#################################');
disp('#################################');disp('');

offset = 0.01;


SF_core_setopt('eigensolver','ARPACK');
disp('##### (ARPACK (SI) / ff / nev = 1 (SI) )' );
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D');%,'ffbin','FreeFem++');
ev
SFerror(4) = abs(ev/ev_REF-1)

%ff3 = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++';
disp('#####  (ARPACK/ff / nev = 10) ');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D');%,'ffbin','FreeFem++');
ev(1)
SFerror(5) = abs(ev(1)/ev_REF-1)

disp('#####  (ARPACK (SI) /ffmpi / nev = 1)');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D');%,'ffbin','FreeFem++-mpi');
ev
SFerror(6) = abs(ev/ev_REF-1)

%disp('#####  (ARPACK/ffmpi)');
%[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D','ffbin','FreeFem++-mpi');
%ev
%SFerror(7) = abs(ev(1)/ev_REF-1)
% This combination fails on David's Mac !

disp('##### (SLEPC/ffmpi / nev=1)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D','ffbin','FreeFem++-mpi');
ev
SFerror(8) = abs(ev/ev_REF-1)

disp('##### (SLEPC/ffmpi / nev=10)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D','ffbin','FreeFem++-mpi');
ev
SFerror(9) = abs(ev(1)/ev_REF-1)



disp('');disp('#################################');
disp('#################################');disp('');
disp('              ADOINT / offset SHIFT = 0.01 ');
disp('');disp('#################################');
disp('#################################');disp('');


SF_core_setopt('eigensolver','ARPACK');
disp('##### (ARPACK (SI) / ff / nev = 1 (SI) )' );
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++');
ev
SFerror(10) = abs(ev/evA_REF-1)

%ff3 = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++';
disp('#####  (ARPACK/ff / nev = 10) ');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++');
ev(1)
SFerror(11) = abs(ev(1)/evA_REF-1)

disp('#####  (ARPACK (SI) /ffmpi / nev = 1)');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(12) = abs(ev/evA_REF-1)

%disp('#####  (ARPACK/ffmpi)');
%[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++-mpi');
%ev
%SFerror(7) = abs(ev(1)/ev_REF-1)
% This combination fails on David's Mac !

disp('##### (SLEPC/ffmpi / nev=1)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(14) = abs(ev/evA_REF-1)

disp('##### (SLEPC/ffmpi / nev=10)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(15) = abs(ev(1)/evA_REF-1)




disp('');disp('#################################');
disp('#################################');disp('');
disp('              DIRECT / offset shift = 0.01i ');
disp('');disp('#################################');
disp('#################################');disp('');


offset = 0.01i;

SF_core_setopt('eigensolver','ARPACK');
disp('##### (ARPACK (SI) / ff / nev = 1 (SI) )' );
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D','ffbin','FreeFem++');
ev
SFerror(21) = abs(ev/ev_REF-1)

%ff3 = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++';
disp('#####  (ARPACK/ff / nev = 10) ');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D','ffbin','FreeFem++');
ev(1)
SFerror(22) = abs(ev(1)/ev_REF-1)

disp('#####  (ARPACK (SI) /ffmpi / nev = 1)');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D','ffbin','FreeFem++-mpi');
ev
SFerror(23) = abs(ev/ev_REF-1)

%disp('#####  (ARPACK/ffmpi)');
%[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D','ffbin','FreeFem++-mpi');
%ev
%SFerror(24) = abs(ev(1)/ev_REF-1)
% This combination fails on David's Mac !

disp('##### (SLEPC/ffmpi / nev=1)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','D','ffbin','FreeFem++-mpi');
ev
SFerror(25) = abs(ev/ev_REF-1)

disp('##### (SLEPC/ffmpi / nev=10)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','D','ffbin','FreeFem++-mpi');
ev
SFerror(26) = abs(ev(1)/ev_REF-1)



disp('');disp('#################################');
disp('#################################');disp('');
disp('              ADOINT / SHIFT = 0.01 ');
disp('');disp('#################################');
disp('#################################');disp('');


SF_core_setopt('eigensolver','ARPACK');
disp('##### (ARPACK (SI) / ff / nev = 1 (SI) )' );
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++');
ev
SFerror(27) = abs(ev/evA_REF-1)

%ff3 = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++';
disp('#####  (ARPACK/ff / nev = 10) ');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++');
ev(1)
SFerror(28) = abs(ev(1)/evA_REF-1)

disp('#####  (ARPACK (SI) /ffmpi / nev = 1)');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(29) = abs(ev/evA_REF-1)

%disp('#####  (ARPACK/ffmpi)');
%[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++-mpi');
%ev
%SFerror(30) = abs(ev(1)/ev_REF-1)
% This combination fails on David's Mac !

disp('##### (SLEPC/ffmpi / nev=1)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',1,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(31) = abs(ev/evA_REF-1)

disp('##### (SLEPC/ffmpi / nev=10)');
SF_core_setopt('eigensolver','SLEPC');
[ev,em] = SF_Stability(bf,'shift',ev_REF+offset,'nev',10,'type','A','ffbin','FreeFem++-mpi');
ev
SFerror(32) = abs(ev(1)/evA_REF-1)




value = sum((SFerror>1e-2))

end
