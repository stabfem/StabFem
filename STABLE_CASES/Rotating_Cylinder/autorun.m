function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a cylinder with STABFEM  
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests
% autorun(4) -> display results

if(nargin==0) 
    verbosity=0; 
end

%if ~SF_core_detectlib('SLEPc-complex','bin','FreeFem++-mpi')
%  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
%  value = -1;
%  return;
%end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');
SF_core_arborescence('cleanall');


format long;

ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp','Params',[-40 80 40],'problemtype','2d');
bf=SF_BaseFlow(ffmesh,'Re',1,'Omegax',4.5);
bf=SF_BaseFlow(bf,'Re',10,'Omegax',4.5);
bf = SF_Adapt(bf);

bf.beta
bf.Omegax

bf=SF_BFContinue(bf,'step',0.05);

betaref = 0.98065;% 1.0199
bf.beta

alpharef =4.50181;
bf.Omegax


SFerror(1) = abs(bf.beta/betaref-1)+abs(bf.Omegax/alpharef-1);

value = sum((SFerror>1e-2))

end
