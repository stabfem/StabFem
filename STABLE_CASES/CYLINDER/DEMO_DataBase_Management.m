%% Demonstration of Database operation and management with StabFem
%
% This tutorial allows to observe step-by-step how, in a series of baseflow
% computations and mesh adaptations, the files are stored in the database,
% how the contents can be checked using SF_Status, reaccessed using
% SF_Load and sorted using SF_SortDataBase


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);
SF_DataBase('create','WORK_DEMO_DATABASE'); % open database, clean if not empty

%% Initial mesh
%

ffmesh =  SF_Mesh('Mesh_Cylinder.edp','Options',{'Xmin',-40,'Xmax',80,'Ymax',40});
SF_Status;

%%
% Note that here, one metadata 'H' (height of numerical domain) is
% associated to the mesh. It is specified in macro SFWriteMesh defined in
% SF_Custom.idp.


%% Computing one BF with initial mesh
bf = SF_BaseFlow(ffmesh,'Solver','Newton_2D.edp','Options',{'Re',1});

SF_Status;




%% Doing a few steps in Re

bf = SF_BaseFlow(bf,'Options',{'Re',2});

bf = SF_BaseFlow(bf,'Re',3); 
% NB for simple cases as here, the parameter 'Re' can be provided
% directly, not through 'Options'. howeer this may not work on more
% advanced cases. It is now recommended to use "Options".



SF_Status;

%%
% Note that the first base flow associated to each new mesh is stored both
% in folders 'BASEFLOWS' and 'MESHBF'. The next ones only in 'BASEFLOWS'.
% The folder 'MESHBF' is also used for cases involving deformable meshes (ALE, etc..)


%% Adapting mesh and recomputing base flow
% 


bf = SF_Adapt(bf); %NB this one is simply projected
bf = SF_BaseFlow(bf,'Re',3); % this one is recomputed
SF_Status;


%%
% NB other possibility for Adapting mesh with automatic recomputation of BF
% The two previous steps can be done in a single command
%
% bf = SF_Adapt(bf,'recompute',true) 
%
% However this is not recommended any more to use this method as it may not
% work for all cases


%% Doing a few steps in Re

bf = SF_BaseFlow(bf,'Re',5);
bf = SF_BaseFlow(bf,'Re',10); 
bf = SF_BaseFlow(bf,'Re',20); 
bf = SF_BaseFlow(bf,'Re',15);

%% Summary 

sfs = SF_Status('BASEFLOWS');

sfs.BASEFLOWS

%%
% Note that the objet sfs.BASEFLOWS contains metadata of the Baseflows AND
% the meshes ! on the other hand the display on screen only reports
% metadata of the baseflows (but indicates the mesh file names)

%% Recovering baseflow from the database
% We can pick a given Baseflow from the badabase using SF_Load. For
% instance to get the one for Re=5 whic is number 5: 

bf = SF_Load('BASEFLOWS',5)

%%
% It is also possible to pick the last computed one in this way :

bf = SF_Load('BASEFLOWS','last')



%% plotting Fx as function of Re with exploration of database
sfs = SF_Status('QUIET'); % quiet is to suppress output
Re = [sfs.BASEFLOWS.Re]; % NB brackets are mandatory
Fx = [sfs.BASEFLOWS.Fx]; % NB brackets are mandatory
figure; plot(Re,Fx,'x-');xlabel('Re');ylabel('Fx');title('Raw from database')

%% cleaning and sorting the database
% 
% The previous curve is ugly because 1/ the three first points for Re=<3 are
% not good because the mesh was not adapted, and 2/ the point are not in the good order 
% (Re=15 was computed after Re=20).
% we can remove files from the database and then sort it as follows :

SF_DataBase('rm','BASEFLOWS',[1:3]);
sfs = SF_Status('QUIET');
sfs.BASEFLOWS = SF_SortDataBase(sfs.BASEFLOWS,'Re'); %sort by ascending values of Re
Re = [sfs.BASEFLOWS.Re]; 
Fx = [sfs.BASEFLOWS.Fx]; 
figure; plot(Re,Fx,'x-');xlabel('Re');ylabel('Fx');title('After clean and sort')

%%

SF_Status;

%%
% Remark that after SF_DataBase('rm',...) the indexes are renumbered and no
% longer correspond to the names of the files


%% Database management of eigenmodes
% Finally we show how eigenmodes are stored and can be reaccessed 

[ev,em] = SF_Stability(bf,'solver','Stab_2D.edp','nev',10,'shift',1i); % requesting eigenvalues+eigenmodes
ev = SF_Stability(bf,'nev',10,'shift',2i); % requesting only eigenvalues

sfs = SF_Status;

%%
% Note that in the second computation, the eigenmodes were not requested
% hence they were not stored in the database.


%% Recovering an eigenmode

em5 = SF_Load('EIGENMODES',5);

%
% NOTE : eigenmodes are stored in this way if using the syntax [ev,em] = SF_Stability(...)
%        but not with the syntax ev =  SF_Stability(...)


%% Getting an index of all eigenvalue computations.

% the return object of SF_Status has two fields which provide a summary of eigenvalues. 

sfs.EIGENMODES

sfs.EIGENVALUES

% Note that EIGENMODES only describes the modes which are requested when
% launching the computation, and inherits metadata from the base flow and
% mesh.
%
% EIGENVALUES describes all eigenvalues. The metadata are processed
% differently (this second method is to be redisigned)

%% Remark : usage of 'Type','DataBase' vs. 'Type','NEW' (LEGACY)
% instead of 'Type','NEW' (which is now default) you can specify 'Type','DataBase'
% to pick up a base flow from the database IF EXISTS AND IS COMPATIBLE WITH CURRENT MESH
% for instance :
% SF_BaseFlow(bf,'Type','DataBase','Re',10) -> will not recompute, simply import
% SF_BaseFlow(bf,'Type','DataBase','Re',3) -> will recompute
% (may not work in all case and not recommended usage)



% [[PUBLISH]]
