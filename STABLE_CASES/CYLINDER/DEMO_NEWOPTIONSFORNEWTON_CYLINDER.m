%% Demonstration of a few novelties introduced in Newton solver. 
% (DF, 27/11/2020)
% Here we use the generic solver "Newton_2D.edp" ; these novelties are to be
% propagated in other solvers !



%% Initialization
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',3);
SF_DataBase('create','./WORK_DEBUG/');

SF_core_setopt('VhList','P2,P2P2P1,P1bP1bP1'); % to recognize [P1b,P1b,P1] which is not in the defaults 


%% Initial mesh adapted for Re=1

ffmesh = SF_Mesh('Mesh_Cylinder.edp','Options',{'Xmin',-40,'Xmax',80,'Ymax',40});
bf = SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Options',{'Re',1});
bf = SF_Adapt(bf);
bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re',1});



%% How to use DEBUG option to view Newton iterations
bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re',10,'DEBUG',1});

%%
SF_Status

%% 
It1 = SF_Load('DEBUG',6);
It2 = SF_Load('DEBUG',7);
It3 = SF_Load('DEBUG',8);
figure; 
subplot(3,1,1); SF_Plot(It1,'ux','xlim',[-1 3],'ylim',[0 2]); title('Iterations 1 to 3');
subplot(3,1,2); SF_Plot(It2,'ux','xlim',[-1 3],'ylim',[0 2]); 
subplot(3,1,3); SF_Plot(It3,'ux','xlim',[-1 3],'ylim',[0 2]); 

%%
% NB the WARNINGS are normal

%% Using integration by parts of viscous term : grad(v)':grad(u) instead of 2 D(u):D(v)
% Using this, outlet conditions are modified from "no-stress" to "no-traction". 
% This may arrange convergence problems in some cases

bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re',10,'IBPViscous','Grad'});




%% Using local pseudo-timestep method and relaxation factor gamma 
% (trick used at ONERA to improve convergence ; to be tested)

bfPTS = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re',100,'isdtNewton',1,'CFmin',10,'gammaNewton',0.85});






%% DEMO on how to use [P1b,P1b,P1] instead of [P2,P2,P1]

MACROS = {'macro Pk [P1b,P1b,P1] //','macro Pkstring() "P1bP1bP1" //',' cout << " USING P1b,P1b,P1" << endl; '};
% We will write these lines in file "SF_AutoInclude.idp" through the 'Include' option
% NB an alternative method would be to write directly these lines in 'SF_Custom.idp'

bfP1b = SF_BaseFlow(bf.mesh,'solver','Newton_2D.edp','Options',{'Re',10},'Include',MACROS);
%% 
% Note that we have to restart from the mesh; using SF_BaseFlow(bf,...)
% with bf a previously computed one using [P2,P2,P1] would fail.

figure; SF_Plot(bfP1b,'ux','xlim',[-1 3],'ylim',[0 2]); title('Solution computed using P1bP1bP1b'); 

%%
% SF_Adapt will work just as usual
bfP1b = SF_Adapt(bfP1b);
bfP1b = SF_BaseFlow(bfP1b,'solver','Newton_2D.edp','Options',{'Re',10},'Include',MACROS);


%[[PUBLISH]]



