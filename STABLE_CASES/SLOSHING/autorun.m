function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case VESSEL (sloshing modes)
%
if(nargin==0) 
   verbosity=0;
end

if SF_core_getopt('ffversion') < 4.7
    SF_core_log('w', 'This autorun skipped because this case requires FreeFem 4.7 or more recent');
    value = -1;
    return
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');
SF_core_arborescence('cleanall');

%SF_core_setopt('eigensolver','ARPACK');



%% CHAPTER 1 : flat surface, free condition
gamma=0.002;
rhog=1;
R = 1;
L =2;


density=100;
 %creation of an initial mesh 

freesurf = SF_Init('Mesh_Vessel.edp','Params',[L density],'problemtype','AxiFSStatic');

% deform this mesh
freesurf = SF_Deform(freesurf,'P',1e-4,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');


disp('###first test : flat surface, free condition')


[evm1] =  SF_Stability(freesurf,'nev',10,'m',1,'shift',2.1i,'typestart','freeV','typeend','axis');
% NB with this value of the shift the modes number n=1,2,3 will be found with index (2,1,3)

evTheory = [1.3606    2.3737    3.1274]; % see script Analytical solution
SFerror(1) = abs(imag(evm1(2))/evTheory(1)-1)+abs(imag(evm1(1))/evTheory(2)-1)+abs(imag(evm1(3))/evTheory(3)-1)




%% CHAPTER 2 : flat surface, pined condition

disp('### Second test : flat surface, pined condition')

evm1 =  SF_Stability(freesurf,'nev',10,'m',1,'shift',2.1i,'typestart','pined','typeend','axis');

evTheory = [1.4444    2.4786    3.2671]; % There is a paper by Miles with analytical solution... 
SFerror(2) = abs(imag(evm1(2))/evTheory(1)-1)+abs(imag(evm1(1))/evTheory(2)-1)+abs(imag(evm1(3))/evTheory(3)-1)






%% Chapter 3 : meniscus (theta = 45?), free conditions
disp('### third test : flat surface, free condition (cf. Viola et al)')

% This case is the same as in Viola, Gallaire & Brun
% first compute equilibrium shape and corresponding mesh

gamma = 0.002;
thetaE = pi/4;
hmeniscus = sqrt(2*gamma*(1-sin(thetaE))); % this is the height of the meniscus (valid for large Bond)
freesurf = SF_Init('Mesh_Vessel.edp','Params',[L, density],'problemtype','AxiFSStatic');
P = -rhog*hmeniscus; % pressure in the liquid at z=0 (altitude of the contact line) ; the result will be to lower the interface by this ammount 
freesurf = SF_Deform(freesurf,'P',P,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');


% check angle
angleREF = 3.8975
freesurf.mesh.meshlin.alphasurf(1)
SFerror(3) = abs(freesurf.mesh.meshlin.alphasurf(1)/angleREF-1)



[evm1,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'shift',2.1i,'typestart','freeV','typeend','axis');

%evTheory = [1.3587    2.3630    3.1118]
evTheory = [1.356043 2.35992 3.108292];

evm1

SFerror(4) = abs(imag(evm1(2))/evTheory(1)-1)+abs(imag(evm1(1))/evTheory(2)-1)+abs(imag(evm1(3))/evTheory(3)-1)



%% test 4: viscous problem

disp('### fourth test : viscous eigenmode')

nu = 0.01;
evm1VISCOUS =  SF_Stability(freesurf,'nev',4,'m',1,'nu',nu,'shift',1.37i+1,'typestart','freeV','typeend','axis');

evREF = -0.1570 + 1.3731i;

SFerror(5) = abs(evm1VISCOUS(1)/evREF-1)


value = sum((SFerror>1e-2))

end
