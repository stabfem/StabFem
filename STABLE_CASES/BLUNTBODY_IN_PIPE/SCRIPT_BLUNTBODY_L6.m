%% Stability analysis of the flow around a blunt body in a pipe
%
% This program is a simple example for Length ratio L/D = 6 and confinement
% ratio d/D = 0.5. 
% We find a stady mode for Re>Rec,1 ~ 120 
% and an oscillating mode  for Re>Rec,2 ~ 180.
%
%%


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',4,'workdir','./WORK/') % NB these two parameters are optional ; SF_core_Start() will do the same
%SF_core_arborescence('cleanall');
%% Chapter 1 : builds or recovers base flow
sfstat = SF_Status('all');
if isempty(sfstat.MESHES)
    bf = SmartMesh_L6();
else
    bf = SF_Load('lastadapted'); 
end


%% plot the base flow
figure;SF_Plot(bf,'ux','contour','on','clevels',[0,0],'xlim',[-3 10],'ylim',[0 1]);  %
pause(0.1);


%% Spectrum explorator 
[ev,eigenmode] = SF_Stability(bf,'m',1,'shift',1+0.2i,'nev',20,'type','D','plotspectrum','yes');   
pause(0.1);



 
%% COMPUTING THE UNSTEADY BRANCH (going backwards)
Re_RangeI = [250:-10:150]*2;
guessI = 0.04+0.657i;
[EVI,ResI,omegasI] = SF_Stability_LoopRe(bf,Re_RangeI,guessI,'plot','r');
       

        
%% COMPUTING THE STEADY BRANCH (going backwards)
Re_RangeS = [250:-25:150 140 :-10:80]*2;
guessS = 0.246;
[EVS,ResS,omegasS] = SF_Stability_LoopRe(bf,Re_RangeS,guessS,'plot','b');
      

%% FIGURES
figure(11);hold on;
subplot(2,1,1);hold on;
plot(Re_RangeS,real(EVS),'-b',Re_RangeI,real(EVI),'-r');hold on;
plot(Re_RangeS,0*real(EVS),':k');%axis
title('growth rate lambda_r vs. Reynolds ; unstready mode (red) and steady mode (blue)')
subplot(2,1,2);hold on;
plot(Re_RangeS,imag(EVS),'-b',Re_RangeI,imag(EVI),':r',Re_RangeI(real(EVI)>0),imag(EVI(real(EVI)>0)),'r-')
title('oscillation rate lambda_i vs. Reynolds')
%subtitle('Leading eigenvalues of a blunt body with L/d=6; D/d=2');

% [[PUBLISH]]
