%%  LINEAR Stability ANALYSIS of the wake of a blunt body confined within a pipe 
%
% This script reproduces sample results from the following paper :
% "The flow around a bullet-shaped blunt-body moving in a pipe"
% by P. Bonnnefis, D. Fabre & C. Airiau (submitted to PRF)
% 
%
% # Generation of an adapted mesh for Re = 400, a/A = 0.75, L/d = 2
% # Plot the base flow for Re = 110  and Re = 320 (figure 6a)
% # Plot the eigenmodes (figure 9)
% # plot ux(r) (figure  7)
%
% The raw source code is <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/BLUNTBODY_IN_PIPE/SCRIPT_SampleResult.m  here>. 


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity', 4)
SF_DataBase('open','./WORK_SAMPLE/');


%% Chapter 1 : builds a robust mesh adapted to base flow and eigenmode for Re=400
dsurD = sqrt(0.75);
Rbody = 0.5; Lel = 1; Lcyl = 1; Rpipe = 0.5/dsurD; xmin = -5; xmax = 30;Lbody=Lcyl+Lel;
bctype = 1;
ffmesh = SF_Mesh('meshInit_BluntBodyInTube.edp','Params',[Rbody Lel Lcyl Rpipe xmin xmax bctype]); 
bf = SF_BaseFlow(ffmesh,'Re',1,'solver','Newton_Axi.edp');

Re_start = [10 , 30, 60, 100, 150, 200, 250, 320, 400]; % values of Re for progressive increasing up to end
  for Rei = Re_start
      bf=SF_BaseFlow(bf,'Re',Rei); 
      bf=SF_Adapt(bf,'Hmax',1);
  end
% adapts to an eigenmode  
shift =  1.3 + 1.06i;
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','m',1,'shift',shift,'nev',1,'type','A');
bf = SF_Adapt(bf,em); 
bf=SF_BaseFlow(bf,'Re',400); 

% Computes base flows for Re=320,177,110
bf320 = SF_BaseFlow(bf,'Re',320); 
bf177 = SF_BaseFlow(bf320,'Re',177);
bf110 = SF_BaseFlow(bf177,'Re',110);

%% alternative from database
bf320 = SF_Load('BASEFLOWS',12);
bf177 = SF_Load('BASEFLOWS',13);
bf110 = SF_Load('BASEFLOWS',14);

%%
% Set options to plot baseflows and eigenmodes
PlotBFOptions1 = {'vort','boundary','on','xlim',[-1.5 4],'ylim',[0 0.577],'colorrange',[-10 10],'colormap','parula'};
PlotBFOptions2 = {'p','xlim',[-1.5 4],'ylim',[0 0.577],'contour','only'};
SF_core_setopt('PlotBFOptions',{PlotBFOptions1;PlotBFOptions2});

%% 
% Other way to set options
%PlotModeOptions = {{'vort','xlim',[-1.5 6],'ylim',[0, 0.777]};PlotBFOptions2};
%SF_core_setopt('PlotModeOptions',PlotModeOptions);


%% plot the base flow
figure;subplot(2,1,1);SF_PlotBF(bf110);  % plot with global options
subplot(2,1,2);SF_PlotBF(bf320);  % plot with global options
pause(0.1);


%% plot ux(r) along a line and compare with theory
% extract ux along a line
rline = linspace(.5,.5/sqrt(.75),100);
bf320line = SF_ExtractData(bf320,'ux',0.75,rline);
bf110line = SF_ExtractData(bf110,'ux',0.75,rline);
bf320line2 = SF_ExtractData(bf320,'ux',1.25,rline);
bf110line2 = SF_ExtractData(bf110,'ux',1.25,rline);
% theory (Annular Poiseuille)
r1 = .5;r2 = .5/sqrt(.75);
D = log(r2/r1)*(r1^2+r2^2)+r1^2-r2^2;A = -1/D;B = (r1^2+r2^2)/D;C = r1^2/D;
utheo = A*rline.^2+B*log(rline./r1)+C;
%% 
% plot
figure; plot(bf110line(1:2:end),rline(1:2:end),'r+',bf320line(2:2:end),rline(2:2:end),'bx',bf110line2,rline,'r--',bf320line2,rline,'b--',utheo,rline,'k:');
set(gca,'FontSize',21);xlabel('u_x');ylabel('r');
%legend('Re = 110','Re = 320','Theo.'); 
saveas(gcf,'BluntBody_Poiseuille','png')

%% Spectrum for Re = 400
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','m',1,'nev',10,'shift',1+1i,'PlotSpectrum',true)


%% Loop over Re to compute eigenvalues
shift = 4+.5i; evS = [];
Re_start = [400:-5:100];
 for Rei = Re_start
      bf=SF_BaseFlow(bf,'Re',Rei); 
      [ev,em] = SF_Stability(bf,'shift',shift,'nev',10,'sort','cont','threshold','multiple');
      evS = [evS,ev'];
 end

 
sfs = SF_Status; 
 
figure;plot(Re_start,evS(:,1),Re_start,evS(:,2));

%% Mode S1 for 110
[ev,emS1] = SF_Stability(bf110,'solver','StabAxi.edp','m',1,'nev',1,'shift',0,'PlotSpectrum',true)
rline = [0.01:0.001:0.577];thetaline = [0:2*pi/40:2*pi];
Y = rline'*cos(thetaline);Z = rline'*sin(thetaline);
emS1line = SF_ExtractData(emS1,2,rline);
VORTX = real(emS1line.vortx'*exp(1i*thetaline));
figure;contourf(Y,Z,VORTX);xlabel('y');ylabel('z');
c = colorbar;c.Label.String = '\omega_x';
saveas(gca,'modeS1_omegax','png')

%% Mode S2 for 177
[ev,emS2] = SF_Stability(bf177,'solver','StabAxi.edp','m',1,'nev',1,'shift',0.,'PlotSpectrum',true)
emS2line = SF_ExtractData(emS2,2,rline);
VORTX = real(emS2line.vortx'*exp(1i*thetaline));
figure;contourf(Y,Z,VORTX);
%VORT = real(emS2line.vort'*exp(1i*thetaline));
%figure;contourf(Y,Z,VORT);xlabel('y');ylabel('z');
c = colorbar;c.Label.String = '\omega_x';
saveas(gca,'modeS2_omegax','png')

%% [[PUBLISH]]

