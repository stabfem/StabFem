%% Computation of the potential flow around a 2D airfoil with FreeFem/StabFem
%
% This example is a demonstration of how to compute a potential flow or a viscous
% flow around a wing profile (NACA1408).
% 
% The source code of this program is 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_NACA/Example_NACA2408.m Example_NACA1408.m>
%

%% First initialize StabFem:

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4,'ffdatadir','WORK_NACA2408');

%% Generation of a mesh
%
% We use the program <<https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/NACAWINGS/NacaFourDigits_Mesh.edp NacaFourDigits_Mesh.edp> 
% which is a generic mesh generator for classical four-digits NACA profiles of the
% form NACA_MPXX where M is the camber, P the location of max camber and XX
% the thickness.
% This program is designed to be used as follows in shell mode (with M=1, P=4, XX=08):
%  
% > FreeFem++ NacaFourDigits_Mesh.edp -M 1 -P 4 -XX 8
%
% Using the driver SF_Mesh, this is interfaced as follows:

mesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',1,'P',4,'XX',8});

%%
% Plot this mesh :

figure(1); 
subplot(2,2,1); SF_Plot(mesh,'title','Mesh (full)');
subplot(2,2,2); SF_Plot(mesh,'title','Mesh (wing)','xlim',[-.5 1],'ylim',[-.75 .75]);
subplot(2,2,3); SF_Plot(mesh,'title','Mesh (Zoom LE)','xlim',[-.4 -.1],'ylim',[-.15 .15]);
subplot(2,2,4); SF_Plot(mesh,'title','Mesh (Zoom TE)','xlim',[.6 .9],'ylim',[-.15 .15]);

%%
% In, the sequel we will use the program <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/NACAWINGS/PotentialFlow.edp PotentialFlow.edp>
% which computes the potential flow for given values of incidence $\alpha$ and
% circulation $\Gamma$ (remind that the potential problem admits multiple
% solutions parametrized by the circulation $\Gamma$).

%% Compute Potential flow for alpha = 0 � ; Gamma = 0 (Kutta condition not verified)
alpha = 0; %angle in degrees
Gamma = 0; % circulation
Flow0 = SF_Launch('PotentialFlow.edp','Options',{'alpha',alpha,'Gamma', Gamma},'mesh',mesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS');
%% 
% The Kutta indicator is defined as ux(xc,.001)-ux(xc,-.001) ;
Flow0.Kutta              

%%
% Here this indicator is not zero meaning that the flow is not regular at the trailing
% edge. See the struture of the flow :
%              

figure(2); 

subplot(2,2,[1 2]); SF_Plot(Flow0,'p','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 0� ; \Gamma = 0');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[-2 2],'ylim',[-.75 .75]);
subplot(2,2,3); SF_Plot(Flow0,'p','xlim',[-.3 -.2],'ylim',[-.01 .05],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[-.3 -.2],'ylim',[-.05 .05]);
subplot(2,2,4); SF_Plot(Flow0,'p','xlim',[.7 .8],'ylim',[-.01 .05],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[.7 .8],'ylim',[-.05 .05]);
pause(0.1);           
         

%% Computing flow for alpha = 0 degrees with right circulation (Kutta condition verified)
%
% The value of Gamma has been determined by trial-and-error to satisfy the
% Kutta condition :
Gamma =0.062 ; alpha = 0;
FlowK = SF_Launch('PotentialFlow.edp','Options',{'alpha',alpha,'Gamma', Gamma},'mesh',mesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS');

%%
% Plot this solution :

figure(4); 

subplot(2,2,[1 2]); SF_Plot(FlowK,'p','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 0� ; \Gamma = \Gamma_K');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[-2 2],'ylim',[-.75 .75]);
subplot(2,2,3); SF_Plot(FlowK,'p','xlim',[-.3 -.2],'ylim',[-.01 .05],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[-.3 -.2],'ylim',[-.05 .05]);
subplot(2,2,4); SF_Plot(FlowK,'p','xlim',[.7 .8],'ylim',[-.01 .05],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[.7 .8],'ylim',[-.05 .05]);
pause(0.1);      
%% 
% This solution has nonzero Lift force, namely

FlowK.Fy

%% VISCOUS FLOWS
% We consider again alpha = 0 ; we want to ompute a solution for Re = 10^5.
% We will use the program 'Newton_2D.edp' which computes a steady solution
% of Navier-Stokes iteration for given parameters Re and alpha.

%%
% We first compute a solution for a lower value of Re :
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 100},'mesh',mesh,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');

%%
% We now progressively increase Re and use continuation method (i.e. the
% Newton is initiated at each time by the previously computed solution).
%
% At some stages we also use mesh adaptation to optimize the computation.
Re_Tab = [300 600 1000 2000 5000 1e4];
for Re = Re_Tab
  FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');
  Mask = SF_Mask(FlowV,[-.3 1 -0.1 0.1 0.005]);            
  FlowV = SF_Adapt(FlowV,Mask,'hmin',0.0001,'recompute',false);
end
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS') ;              


%%
% Plot this solution
              
figure(5) ;hold off; subplot(2,2,[1 2]);
SF_Plot(FlowV,'vort','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-1 3],'ylim',[-.75 .75]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-1 3],'ylim',[-.75 .75],'CStyle','dashedneg');
subplot(2,2,3);
SF_Plot(FlowV,'vort','xlim',[-.4 -.1],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-.4 -.1],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-.4 -.1],'ylim',[-.15 .15],'CStyle','dashedneg');
subplot(2,2,4);
SF_Plot(FlowV,'vort','xlim',[.6 .9],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[.6 .9],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[.6 .9],'ylim',[-.15 .15],'CStyle','dashedneg');
pause(0.1);
%%
figure;
SF_Plot(FlowV,'ux','xlim',[.6 .9],'ylim',[-.15 .15],'colormap','redblue');
hold on; SF_Plot(FlowV,'psi','contour','only','xlim',[.6 .9],'ylim',[-.15 .15],'CStyle','dashedneg');
pause(0.1);


%% Compare the Lift and drag forces in potential and viscous cases

PotentialLift = FlowK.Fy
PotentialDrag = FlowK.Fx
ViscousLift = FlowV.Fy
ViscousDrag = FlowV.Fx



%% SUMMARY 

SF_Status

% [[PUBLISH]]

