%%  Demonstration of how to do a DNS using StabFem
%
% In this tutorial script we show how to perform a DNS of the wake of a
% cylinder for Re = 60, allowing to observe the development and saturation
% of the well-known Von Karman vortex street.
% The DNS solver is based on Uzawa method using Cahouet preconditionner.
% The FreeFem++ code <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp> 
% is adapted from the one used in Jallas, Marquet & Fabre (PRE, 2017).

%% Chapter 0 : inzitialization
%
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',2);
SF_DataBase('open','./CYLINDER_nomovie/'); % open database folder

% NB change verbosity to 4 to follow the simulation while it's running 
%  or 2 to supress output from freefem++


%%
% 
% We use the same procedure as in 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER/CYLINDER_LINEAR.m CYLINDER_LINEAR.m>. 
% First we build an initial mesh (on a half-domain) and progressively increase the Reynolds :
    ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'cleanworkdir',true);
    bf=SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',1,'Symmetry','S');
    bf=SF_BaseFlow(bf,'Re',10);
    bf=SF_BaseFlow(bf,'Re',60);
    
%%
% Then we readapt the mesh to fit the base flow. We use an "adaptation mask"  
% generated by <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mask.m SF_Mask.m> 
% to enforce the grid size to be at least 0.15 in a window -2<x<10;  0<y<2   
    Mask = SF_Mask(bf.mesh,[-2 10 0 2 .1]);
    bf = SF_Adapt(bf,Mask,'Hmax',5);

% We do this twice for security
    Mask = SF_Mask(bf.mesh,[-2 10 0 2 .1]);
    bf = SF_Adapt(bf,Mask,'Hmax',5);
    
    
%% 
% If you think this mesh is not refined enough, a this stage you can use 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Split.m SF_Split.m> to divide all triangles in 4 !
%
%bf = SF_Split(bf);

%%
%
% We now have a convenient mesh defined on a half-domain. We then convert
% it into a full mesh using <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mirror.m SF_Mirror.m>
% 
%

bf = SF_Mirror(bf);
bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Re',60,'Symmetry','N'); % recompute after mirror. 

%%
% You now have a robust mesh well fitted to DNS. You may also try
% other adaptation strategies (adaptation to eigenmode, etc..). See other
% examples in the project !
% 
% Note that the initially generated mesh is not perfectly symmetric. In
% some cases (e.g. if you want to do DNS very close to the instability
% threshold Re ~ 47) it is desirable to have a perfectly symmetric mesh.
% A procedure to do that is to first generate a half-mesh, adapt it and 
% "mirror" it. An example on how to do this is available here :
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER_DNS/SCRIPT_DNS_WITHSYMMETRICMESH.m SCRIPT_DNS_WITHSYMMETRICMESH.m.m> 



%% Chapter 2 : Generation of a starting point for DNS using stability results
%
% We want to initialise the DNS with an initial condition 
% $u = u_b + \epsilon u_1$ where $u_b$ is the base flow, $u_1$ is the
% eigenmode, and $\epsilon = 0.01$.
% 
%

[ev,em] = SF_Stability(bf,'solver','Stab_2D.edp','shift',0.04+0.76i,'nev',1); % compute the eigenmode. 

startfield = SF_Add({bf,em},'coefs',[1,0.01],'NameParams',['Re'],'Params',[bf.Re]); % creates startfield = bf+0.01*em


%% Chapter 3 : Launch a DNS
%
% We do 5000 time steps and produce snapshots each 10 time steps.
% We use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_TS_Launch.m SF_TS_Launch.m> 
% which launches the Freefem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp>

Niter = 5000;   % total number of time steps
iout = 20;   % will generate snapshots each iout time steps
dt = 0.02;   % time step 
DNSOptions = {'Re',60,'Niter',Niter,'dt',dt};

folder = SF_TS_Launch('TimeStepper_2D.edp','Init',startfield,'Options',DNSOptions,'wait',true); % run simulation 
DNSstats = SF_TS_Check(folder); % import results (only statistics, not all snapshots)



%% Chapter 4 : plotting the results 
%  We want to plot the 6 last snapshots, corresponding to iterations 4900,4920,4940,...5000
iters = [4900:20:5000];
for iter = iters
   Snap = SF_Load('TimeStepping',1,iter);
   figure; 
   SF_Plot(Snap,'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-3 3],...
        'title',['t = ',num2str(Snap.t)],'boundary','on','bdlabels',2);
end


%% 
% We now plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstats.t,DNSstats.Fy);title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.t,DNSstats.Fx);title('Drag force as function of time');
xlabel('t');ylabel('Fx');


