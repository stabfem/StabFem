%% Tutorial example demonstrating the usage of adaptation masks
% 
% This tutorial is based on the "Lshape" example, and shows how to refine the
% mesh in two possible ways :
% - according to the solution of a problem 
%   (here an unsteady heat problem whose solution is of "boundary layer" type, 
%    forcing refinement along the boundaries).
% - by forcing mesh refinement in a specified region through a "mask".
%    three kinds of masks are demonstrated : rectangular, elliptic, trapezoidal.
% 

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',3); 
SF_DataBase('open','./WORK_Masks/'); 

%% First build an initial mesh and compute the solution of unsteady problem

Ndensity =40;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity})
figure; SF_Plot(ffmesh);title('Initial mesh');
saveas(gcf,'FIGURES/InitialMesh','png');

omega = 1000;
heatU=SF_Launch('Lshape_Unsteady.edp','Options',{'omega',omega,'kappa',1},'Mesh',ffmesh,'DataFile','Heat_Unsteady.ff2m','Store','HeatSteady')


%% Now build three masks :
Mask1 = SF_Mask(ffmesh,'rectangle',[.1 .3  .8 .9 .005]); 
%%
% Rectangular mask. Here parameters correspond to [xmin,xmax,ymin,ymax,delta]

Mask2 = SF_Mask(ffmesh,'ellipse',[.3, .6,  .2 , 0.1, .005]);
%%
% Elliptic mask. Here parameters correspond to [xc,yc,Lx,Ly,delta]
% (xc,yc) is center, (Lx,Ly) are semiaxes.

Mask3 = SF_Mask(ffmesh,'trapeze',[.3 .8 0.1 0.15 -5 10 .005]);
%%
% Trapezoidal mask. Here parameters correspond to [xmin,xmax,y1,y2,theta1,theta2,delta]
% the domain is a quadrangle with [xmin,y1], [xmin,y2], [xmax,y2T], [xmax,y1T]
% where y1T = y1+(xmax-xmin)tan(theta1), y2T = y2+(xmax-xmin)tan(theta2). 
% (theta1 and theta2 in degrees).
% NB This kind of mask may be particularly useful to design meshes for wake problems...

%% Adapt mesh
Newmesh = SF_Adapt(ffmesh,heatU,Mask1,Mask2,Mask3,'Hmax',0.2); 

%% Plot adapted mesh
figure ; SF_Plot(Newmesh); title('Adapted mesh');
hold on; % drawing the mask domains on top of the figure
plot([.1 .1 .3 .3 .1],  [.8 .9 .9 .8 .8],'r-');
th = linspace(0,2*pi,101);plot(.3+.2*cos(th),.6+.1*sin(th),'r-');
plot([.3 .3 .8 .8 .3],  [.1 .15 .15+.5*tan(10*pi/180) .1-.5*(tan(5*pi/180)) .1],'r-');

saveas(gcf,'FIGURES/AdaptedMesh','png');

% NB we could alternatively do :
% > heatU = SF_Adapt(heatU,Mask1,Mask2,Mask3,'Hmax',0.2); 
% > figure ; SF_Plot(heatU,'mesh'); 
% or even : 
% > [heatU,Mask1,Mask2,Mask3] = SF_Adapt(heatU,Mask1,Mask2,Mask3,'Hmax',0.2); 


%% How does it work ?
%
% the driver SF_Mask calls the Freefem program "AdaptatationMask.edp" which
% create an fake dataset composed of two fields "Maskx" and "Masky" which
% are oscillating functions in the specified "Mask" domain.
% when transmitted to SF_Adapt, this will force the mesh to be sufficiently
% refined to accurately reproduce these "Mask function". 
% 
% For instance let us plot the corresponding functions for the three Masks :

figure ; SF_Plot(Mask1,'Maskx');hold on;SF_Plot(Mask1,'Masky','contour','only');title('Mask 1');
figure ; SF_Plot(Mask2,'Maskx');hold on;SF_Plot(Mask2,'Masky','contour','only');title('Mask 2');
figure ; SF_Plot(Mask3,'Maskx');hold on;SF_Plot(Mask3,'Masky','contour','only');title('Mask 3');

% [[PUBLISH]] 

