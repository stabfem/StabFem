%% Tutorial example demonstrating how to use PETSc / Parallel 
% 
% This tutorial script show how to modify your programs to use parallel computing with PETSc 
% The sample problems considered here are the same ones as in 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape_parallel.m SCRIPT_Lshape_parallel.m>.
% 
% The FreeFem programs have only been modified for a few lines compared to their sequential versions. 
% See inside the program for explanations.


%% Initialization
close all;
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB']);

SF_Start('verbosity',3); 
SF_core_setopt('ffdatadir','./WORK_parallel/'); 
SF_core_arborescence('cleanall'); 

%% Build the mesh (sequencial)
Ndensity =40;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity});

%% First example : Steady conduction
%
% The FreeFem program is 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_Steady_parallel.edp Lshape_Steady_parallel.edp>.
% Please check inside this program and compare to its sequential counterpart 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_Steady.edp Lshape_Steady.edp>.
% to see what has been changed.
%
% Note that the program has also been modified to produce a second result
% file "Partition.txt" allowing to visualize the partition of the mesh


[heatS,Partition] = SF_Launch('Lshape_Steady_parallel.edp','Mesh',ffmesh,'ncores',2,'Datafile',{'Data.txt','Partition.txt'})
figure; SF_Plot(heatS,'T','title','Temperature field')

figure; SF_Plot(Partition,'color','title','Mesh partition')

%% Second example : Navier-Stokes
% The program is
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_NavierStokes_parallel.edp Lshape_NavierStokes_parallel.edp>.
% Please check inside this program and compare to its sequential counterpart 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_NavierStokes.edp Lshape_NavierStokes.edp>.
% to see what has been changed.
% Note that despite this program uses vectorial fespace [P2,P2,P1], the
% modifications are almost identical to those of the previous example !

Flow = SF_Launch('Lshape_NavierStokes_parallel.edp','Mesh',ffmesh,'ncores',2)
figure;SF_Plot(Flow,'ux')


% [[PUBLISH]] -> this tag is here to tell the git runner to automatically generate a web page from this script 
 
