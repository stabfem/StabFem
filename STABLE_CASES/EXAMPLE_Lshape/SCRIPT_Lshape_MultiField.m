%% 

%% Initialization
close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',7); 
% higest levels of verbosity results in more messages.
% recommended values are 2 (minimal notice) and 4 (notice + freefem output). 6 and more is debug mode.

SF_DataBase('create','./WORK/'); 
% this is to initialize the database management. All results will be placed
% in a folder "WORK" and subfolders.

mkdir('FIGURES');
set(0,'defaulttextfontsize',16); % set font size for figures
set(0,'defaultaxesfontsize',14); % idem

%% Generation of the mesh
%
% The program <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/EXAMPLE_Lshape/Lshape_Mesh.edp Lshape_Mesh.edp> is a simple mesh generator producing a
% mesh.msh file. We launch this program and import the mesh as a structure using the <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mesh.m SF_Mesh.m> driver :
%

Ndensity =40;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity})


omega = 100;
heatU=SF_Launch('Lshape_Unsteady_Multi.edp','Options',{'omega',omega,'kappa',1},'Mesh',ffmesh,'DataFile','Heat_Unsteady.ff2m','Store','HeatSteady')

