%% Stability analysis of the wake of a NACA0012 wing profile in the compressible regime (Ma = 0.1)


%% CHAPTER 0 : set the global variables needed by the drivers

clear all;
close all;
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',2);

figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;
%% CHAPTER 1 : Set parameters

Ma = 0.1;
Re = 1000;
np = 128;
Rout = 30;
HMax = 1;

%% CHAPTER 2 : Create an adapted  mesh

ffmesh = SF_Mesh('Naca0012MeshFull.edp','problemtype','2dcomp','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'Re',10,'Mach',Ma,'solver','Newton_2D_Comp.edp');
bf=SF_BaseFlow(bf,'Re',100,'Mach',Ma);
bf=SF_Adapt(bf,'Hmax',HMax);
bf=SF_BaseFlow(bf,'Re',500,'Mach',Ma);
bf=SF_Adapt(bf,'Hmax',HMax);
bf=SF_BaseFlow(bf,'Re',1000,'Mach',Ma);
bf=SF_Adapt(bf,'Hmax',HMax);
bf.mesh = SF_SetMapping(bf.mesh, 'MappingType', 'box', 'MappingParams', [-10,10,-10,10,-0.3,5,-0.3,5]) ;


[ev,em] = SF_Stability(bf,'shift',0.7121 + 2.5875i,'nev',3,'type','D','solver','Stab2D_Comp.edp');
bf=SF_Adapt(bf,em(1),'Hmax',HMax);

%% Chapter 3 : spectrum explorator
plotoptions = {'p','xlim',[-3,3],'ylim',[-3,3]};
[ev,em] = SF_Stability(bf,'shift',0.7121 + 2.5875i,'nev',20,'type','D','PlotSpectrum',true,'PlotModeOptions',plotoptions);
    

%% CHAPTER 4 : Compute sensitivity
[ev,em] = SF_Stability(bf,'shift',ev(1),'nev',3,'type','D');
[evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',3,'type','A');
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type', 'SMa');

%% Chapter 5 : summary

sfs = SF_Status('ALL');

% [[NOPUBLISH]]