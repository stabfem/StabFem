
function bf = SmartMesh_Hole_chi0
% This mesh generator creates a mesh adapted to base flow and forced flow
% for 3 vales of omega.
% Uses complex mapping for computing solution of linear problem.
% (Mesh M2 of the paper)

chi = 1e-4;

ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,20,40,10,10],'problemtype','AxiXR');
%ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params);

bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',300);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf,'Hmax',0.5);

%bf = SF_BaseFlow(bf,'Re',1500);
%bf = SF_Adapt(bf,'Hmax',.5);

Params = [4 1e30 1.25 0.5 20 1e30];
%Omega1 = .5/chi;
%ForcedFlow1 = SF_LinearForced(bf,Omega1,'mappingdef','jet','mappingparams',Params);
Omega2 = 1;
ForcedFlow2 = SF_LinearForced(bf,Omega2,'mappingdef','jet','mappingparams',Params);
Omega3 = 5;
ForcedFlow3 = SF_LinearForced(bf,Omega3,'mappingdef','jet','mappingparams',Params);

Mask = SF_Mask(bf.mesh,[-2*chi 5 0 3 .1]);
Mask2 = SF_Mask(bf.mesh,[-2-2*chi 10 0 5 .5]);

bf = SF_Adapt(bf,Mask,Mask2,ForcedFlow3,ForcedFlow2,'Hmax',4);

%bf = SF_Adapt(bf,ForcedFlow1,ForcedFlow2,ForcedFlow3,'Hmax',.25);

%figure;SF_Plot(bf,'mesh','xlim',[-3 5],'ylim',[0 3]);

end