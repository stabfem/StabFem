function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE

if SF_core_getopt('isoctave')
   disp(' This autorun skipped when using Octave')
   value = -1;
   return;
end

if(nargin==0)
    verbosity=0;
end


if(verbosity==0)
if ~SF_core_detectlib('SLEPc-complex')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end
end

close all;
SF_core_setopt('verbosity', verbosity);
SF_DataBase('create', './WORK_AUTORUN/');
SF_core_setopt('eigensolver','SLEPC')


% Test 1 :

%bf = SF_Load('lastadapted');
bf = SmartMesh_Hole(1);

% test : check if BF is correcty recovered


%% Test 2 : impedance computation for Re = 1600

bf = SF_BaseFlow(bf,'Re',1600);
Params = [5 17 1.25 .5 5 17];
omega = 0.8;
foA = SF_LinearForced(bf,omega,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);

Zref =-0.161814-1.04014i


SFerror(1) = abs(foA.Z/Zref-1)
foA.Z


%% test 3 with stability calculation

Params = [5 17 1.25 .5 5 17];
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','shift',0.09-2.1i,'m',0,'nev',1,'type','D','mappingdef','jet','mappingparams',Params);


EVref = 0.0999179-2.07256i;
ev(1)

SFerror(2) = abs(ev(1)/EVref-1)

value = sum(SFerror>1e-2)

end
