%% Chapter 0 : Initialization 
SF_core_start('verbosity',4,'workdir','./WORK_chi1/') % NB these two parameters are optional ; SF_core_Start() will do the same

%% Chapter 1 : builds or recovers base flow
    bf = SF_Load('lastadapted'); 


%% CHAPTER 3 : Forced structures

%  Computes forced structures for 5 values of omega
bf = SF_BaseFlow(bf,'Re',1600);
bfRe16000 = bf; 
Params = [5 1e30 1.25 .5 20 1e30];

omega = 1.7;Re = 1600; chi=1;
foB = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);


%% Chapter 3b : generate movies

 figure(1);SF_Plot(bf,'vort','xlim',[-2.1 0.1], 'ylim', [0 2],'colorrange',[-20 20]);
 title('base flow');
 pause;
 figure(1);SF_Plot(foB,'vort','xlim',[-2.1 0.1], 'ylim', [0 2],'colorrange',[-20 20]);
 title('Mode (re)' );
 pause;
 figure(1);SF_Plot(foB,'vort.im','xlim',[-2.1 0.1], 'ylim', [0 2],'colorrange',[-20 20]);
 title('Mode (im)' );
 pause;
 amp = 0.1;
for i = 0:20
    omegat = (i/20)*2*pi;
    snapshot = SF_Add(bf,foB,'coefs',[1 amp*exp(-1i*omegat)]);
    figure(1);SF_Plot(snapshot,'vort','xlim',[-2.1 0.1], 'ylim', [0 2],'colorrange',[-20 20])
    title([' t/T = ',num2str(omegat/2/pi)]);
    pause(0.1);
end
 figure(1);SF_Plot(bf,'vort','xlim',[-2.1 0.1], 'ylim', [0 2],'colorrange',[-20 20]);
 title('base flow');
 pause;
