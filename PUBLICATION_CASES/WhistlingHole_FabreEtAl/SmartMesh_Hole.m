
function bf = SmartMesh_Hole(chi)
% This mesh generator creates a mesh adapted to base flow and forced flow
% for 3 vales of omega.
% Uses complex mapping for computing solution of linear problem.
% (Mesh M2 of the paper)

ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,15,15,10,10]);
%ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params);

bf = SF_BaseFlow(ffmesh,'solver','Newton_Axi.edp','Re',1);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',300);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf,'Hmax',0.5);

bf = SF_BaseFlow(bf,'Re',1500);
bf = SF_Adapt(bf,'Hmax',.5);

Params = [0 17 2.5 0.3 5 17];
%Omega1 = .5/chi;
%ForcedFlow1 = SF_LinearForced(bf,Omega1,'mappingdef','jet','mappingparams',Params);
Omega2 = 2.1/chi;
ForcedFlow2 = SF_LinearForced(bf,Omega2,'solver','LinearForcedAxi_COMPLEX_m0.edp','mappingdef','jet','mappingparams',Params);
Omega3 = 4.5/chi;
ForcedFlow3 = SF_LinearForced(bf,Omega3,'mappingdef','jet','mappingparams',Params);

Mask = SF_Mask(bf.mesh,[-2*chi 5 0 3 .1]);
Mask2 = SF_Mask(bf.mesh,[-2-2*chi 10 0 10 .5]);

bf = SF_Adapt(bf,Mask,Mask2,ForcedFlow3,ForcedFlow2,'Hmax',4);
bf = SF_BaseFlow(bf);

%bf = SF_Adapt(bf,ForcedFlow1,ForcedFlow2,ForcedFlow3,'Hmax',.25);

%figure;SF_Plot(bf,'mesh','xlim',[-3 5],'ylim',[0 3]);

end