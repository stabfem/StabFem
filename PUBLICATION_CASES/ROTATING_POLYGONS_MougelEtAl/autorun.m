function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case ROTATING_POLYGONS
%
% USAGE : 
% autorun(0) -> automatic check
% autorun(1) -> produces the figures used for the manual
%

if SF_core_getopt('isoctave')
   disp(' This autorun skipped when using Octave')
   value = -1;
   return;
end

if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');

SF_core_setopt('eigensolver','ARPACK');

isfigures=0;


%% CHAPTER 0 : creation of initial mesh 
a=.3;
xi = .25;
rhog = 1;
R = 1;
gamma = 0;
GammaBAR = xi*sqrt(2*rhog*a*R^2/(R^2-xi^2-2*xi^2*log(R/xi)));

density=60;

%ffmesh = SF_Mesh('Mesh_PotentialVortex.edp','Params',[a xi density],'problemtype','3DFreeSurfaceStatic');
freesurf = SF_Init('Mesh_PotentialVortex.edp','Params',[a xi density],'problemtype','3DFreeSurfaceStatic');


%% CHAPTER 1 : Eigenvalue computation for m=3

%%% warning : in Mougel we have modal dependance with the form exp( i (m theta - omega t) )
%%% and omega is negative (real for pure waves).
%%% here the form is exp ( lambda t + i m theta) . So omega corresponds to
%%% minus the imaginary par of our omega. This is why the guess is -3i and
%%% the eigenvalues are transposed for comparison.



%[evm3,emm3] =  SF_Stability(ffmesh,'gamma',gamma,'rhog',1,'GammaBAR',GammaBAR,'nev',15,'m',3,'shift',-3i);
[evm3,emm3] =  SF_Stability(freesurf,'nev',15,'m',3,'shift',-3i);  
evm3FIG = 1i*evm3(1:4).' % transpose, not conjugate

evm3_REF =  [2.8333, 3.7210, 4.2695 1.3374]; % => Values from Mougel et al Paper 

disp('### autorun test 1 : check eigenvalues for a=0.3, xi = 0.25, m=3');
SFerror = sum(abs(evm3_REF-evm3FIG))

value = sum(SFerror>1e-2)


end
