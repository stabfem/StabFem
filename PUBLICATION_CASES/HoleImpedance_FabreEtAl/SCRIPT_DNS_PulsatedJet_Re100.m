

%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('create','WORKDNS');

%% Chapter 1 : Computing a base flow for Re=1000 and an adapted mesh
chi = 1e-4;
sfs = SF_Status;
if isempty(sfs)
chi = 1e-4;
ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,20,40,10,10],'problemtype','AxiXR');

bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
Mask = SF_Mask(bf.mesh,[-2*chi 5 0 3 .1]);
Mask2 = SF_Mask(bf.mesh,[-2-2*chi 10 0 5 .5]);
bf = SF_Adapt(bf,Mask,Mask2,'Hmax',4);
else
    bf = SF_Load('BASEFLOWS','last');
end


%% Chapter 2 : DNS with forcing
ParamsDNS.dt = 0.02;ParamsDNS.Niter = 50;ParamsDNS.iout = 20;
ParamsDNS.Re=1000;ParamsDNS.omega = 5;ParamsDNS.epsilon=0.1;
Run1 = SF_TS_Launch('TimeStepper_Axi_Jet.edp','Init',bf,'Options',ParamsDNS);
[DNSstats,DNSfields] = SF_TS_Check(Run1); 

%%
figure; 
subplot(2,1,1); SF_Plot(bf,'vort','xlim',[-1 5],'ylim',[0 2],'colorrange',[-2 2]);
subplot(2,1,2); SF_Plot(DNSfields(end),'vort','xlim',[-1 5],'ylim',[0 2],'colorrange',[-2 2]);

%%
figure; 
subplot(2,1,1); SF_Plot(bf,'ux','xlim',[-1 5],'ylim',[0 2]);
subplot(2,1,2); SF_Plot(DNSfields(end),'ux','xlim',[-1 5],'ylim',[0 2]);

%%
figure; 
subplot(2,1,1); SF_Plot(bf,'ux','xlim',[-1 5],'ylim',[0 2]);
subplot(2,1,2); SF_Plot(DNSfields(end),'ur','xlim',[-1 5],'ylim',[0 2]);