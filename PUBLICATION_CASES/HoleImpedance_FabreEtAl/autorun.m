
function value = autorun(verbosity)

% Autorun function for StabFem. 
% This function will produce sample results for the case Falling_Ellipse
%
% USAGE : 
% autorun -> automatic check
% autorun(1) -> produces the figures used for the manual


if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_DataBase('create', './WORK_AUTORUN/');

value=0;

chi = 1e-4;
ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,20,40,10,10],'problemtype','AxiXR');
bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
Mask = SF_Mask(bf.mesh,[-2*chi 5 0 3 .1]);
Mask2 = SF_Mask(bf.mesh,[-2-2*chi 10 0 5 .5]);
bf = SF_Adapt(bf,Mask,Mask2,'Hmax',4);

ParamsDNS.dt = 0.02;ParamsDNS.Niter = 50;ParamsDNS.iout = 20;
ParamsDNS.Re=100;ParamsDNS.omega = 5;ParamsDNS.epsilon=0.1;
Run1 = SF_TS_Launch('TimeStepper_Axi_Jet.edp','Init',bf,'Options',ParamsDNS);
[DNSstats,DNSfields] = SF_TS_Check(Run1); 

Xe = DNSstats.dP(end)
XeREF = 1.2176



SFerror(1) = abs(Xe/XeREF-1)

value = sum(SFerror>1e-2)
end
%[[AUTORUN:SHORT]]