
function bf = SmartMesh_Hole_chi0
% This mesh generator creates a mesh adapted to base flow across a single
% hole of zero thickness 

chi = 1e-4;
ffmesh = SF_Mesh('Mesh_OneHole.edp','Params',[chi,20,40,10,10],'problemtype','AxiXR');

bf = SF_BaseFlow(ffmesh,'Re',1);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',30);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',0.5); 
bf = SF_BaseFlow(bf,'Re',300);
bf = SF_BaseFlow(bf,'Re',1000);
bf = SF_Adapt(bf,'Hmax',0.5);

Mask = SF_Mask(bf.mesh,[-2*chi 15 0 3 .05]);
Mask2 = SF_Mask(bf.mesh,[-2-2*chi 20 0 5 .5]);

bf = SF_Adapt(bf,Mask,Mask2,'Hmax',4);


end