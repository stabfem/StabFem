%% DNS of a harmonically forced jet

%%
% This script performs a DNS of a jet through a hole of zero thickness.
% It reproduces figure 15 in section 6 of Fabre et al. JFM 2019

%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('create','WORKDNS');

%% Chapter 1 : Computing a base flow for Re=1000 and an adapted mesh
chi = 1e-4;
sfs = SF_Status;
if isempty(sfs)
    bf = SmartMesh_Hole_chi0; % or your own sequence for mesh/bf generation
else
    bf = SF_Load('BASEFLOWS','last');
end


%% Chapter 2 : DNS with forcing
ParamsDNS.dt = 0.001;ParamsDNS.Niter = 10000;ParamsDNS.iout = 200;
ParamsDNS.Re=1000;ParamsDNS.omega = 2;ParamsDNS.epsilon=0.01;
Run1 = SF_TS_Launch('TimeStepper_Axi_Jet.edp','Init',bf,'Options',ParamsDNS);
[DNSstats,DNSfields] = SF_TS_Check(Run1); 

%% Plots
figure;
subplot(4,1,1); SF_Plot(bf,'vort','xlim',[-1 15],'ylim',[0 2],'colorrange',[0 3.5],'colormap','ice');
subplot(4,1,2); SF_Plot(DNSfields(25),'vort','xlim',[-1 15],'ylim',[0 2],'colorrange',[0 3.5],'colormap','ice');
subplot(4,1,3); SF_Plot(DNSfields(end),'vort','xlim',[-1 15],'ylim',[0 2],'colorrange',[0 3.5],'colormap','ice');
subplot(4,1,4); SF_Plot(DNSfields(end),'mesh','xlim',[-1 15],'ylim',[0 2]);
%% 
figure; 
plot(DNSstats.t,DNSstats.Um,DNSstats.t,DNSstats.dP); ylim([.5 1.5]);xlabel('t');legend('Um(t)','dP(t)');

%% 
% Here is how to generate a movie
h = figure;
hAx=gcf;hAx.Position=hAx.Position.*[1 1 1 0.5];
mkdir('html');
filename = 'html/SCRIPT_DNS_PulsatedJet_Movie.gif';
for i=1:length(DNSfields)
    SF_Plot(DNSfields(i),'vort','xlim',[-2 20],'ylim',[0 3 ],'colorrange',[0 3.5],...
        'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2,'colormap','ice');
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<SCRIPT_DNS_PulsatedJet_Movie.gif>>
%


%% Chapter 3 : a second DNS to generate a short movie

ParamsDNS.Re=1000;ParamsDNS.omega = 2;ParamsDNS.epsilon=0.01;
ParamsDNS.dt = 0.001*pi/3;ParamsDNS.Niter = 3000;ParamsDNS.iout = 300;
Run2 = SF_TS_Launch('TimeStepper_Axi_Jet.edp','Init',DNSfields(end),'Options',ParamsDNS);
[DNSstats,DNSfields] = SF_TS_Check(Run2); 


%% 
h = figure;
hAx=gcf;hAx.Position=hAx.Position.*[1 1 1 0.5];
mkdir('html');
filename = 'html/SCRIPT_DNS_PulsatedJet_Movie_short.gif';
for i=1:length(DNSfields)
    SF_Plot(DNSfields(i),'vort','xlim',[-2 20],'ylim',[0 3 ],'colorrange',[0 3.5],...
        'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2,'colormap','ice');
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<SCRIPT_DNS_PulsatedJet_Movie_short.gif>>
%



%%
% [[PUBLISH]]



