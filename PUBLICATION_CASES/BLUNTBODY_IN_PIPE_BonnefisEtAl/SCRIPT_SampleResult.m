%%  LINEAR Stability ANALYSIS of the wake of a blunt body confined within a pipe 
%
% This script reproduces sample results from the following paper :
% "The flow around a bullet-shaped blunt-body moving in a pipe"
% by P. Bonnnefis, D. Fabre & C. Airiau (submitted to JFM)
%
% Available form arxiv : <https://arxiv.org/abs/2112.08730>
%
% # Generation of an adapted mesh for Re = 400, a/A = 0.75, L/d = 2
% # Plot the base flow for Re = 110  and Re = 320 (figure 6a)
% # plot ux(r) in a slice (figure  7)
% # amplification rate as function of Re for a/A = 0.75 (figure 8) 
% # Plot the eigenmodes S1 and S2 (figure 9a and 9b)

%
% The raw source code is <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/BLUNTBODY_IN_PIPE/SCRIPT_SampleResult.m  here>. 


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity', 4)
SF_DataBase('open','./WORK_SAMPLE/');

%% Set the solvers to be used in this program, and the options to be used for plotting
%
% (Using new features of version 3.10)
%
% The solvers are defines as follows :
SF_core_setopt('BFSolver','Newton_Axi.edp')
SF_core_setopt('StabSolver','StabAxi.edp')
%% 
% Options for plotting baseflows, with superposition of two layers
% (vorticity / pressure) are set as follows :
PlotBFOptions1 = {'vort','boundary','on','xlim',[-1.5 4],'ylim',[0 0.577],'colorrange',[-10 10],'colormap','viridis'};
PlotBFOptions2 = {'p','xlim',[-1.5 4],'ylim',[0 0.577],'contour','only'};
SF_core_setopt('PlotBFOptions',{PlotBFOptions1;PlotBFOptions2});
%%
% Options for plotting modes 
PlotModeOptions = {{'vort','xlim',[-1.5 6],'ylim',[0, 0.577],'colorrange',[-3 3],'colormap','viridis'};PlotBFOptions2};
SF_core_setopt('PlotModeOptions',PlotModeOptions);



%% Chapter 1 : builds a robust mesh adapted to base flow and eigenmode for Re=400
dsurD = sqrt(0.75);
Rbody = 0.5; Lel = 1; Lcyl = 1; Rpipe = 0.5/dsurD; xmin = -5; xmax = 30;Lbody=Lcyl+Lel;
bctype = 1;
ffmesh = SF_Mesh('meshInit_BluntBodyInTube.edp','Params',[Rbody Lel Lcyl Rpipe xmin xmax bctype]); 
bf = SF_BaseFlow(ffmesh,'Re',1);

Re_start = [10 , 30, 60, 100, 150, 200, 250, 320, 400]; % values of Re for progressive increasing up to end
  for Rei = Re_start
      bf=SF_BaseFlow(bf,'Re',Rei); 
      bf=SF_Adapt(bf,'Hmax',1);
  end
% adapts to an eigenmode  
shift =  1.3 + 1.06i;
[ev,em] = SF_Stability(bf,'m',1,'shift',shift,'nev',1,'type','A');
bf = SF_Adapt(bf,em); 
bf=SF_BaseFlow(bf,'Re',400); 

% Computes base flows for Re=320,177,110
bf320 = SF_BaseFlow(bf,'Re',320); 
bf177 = SF_BaseFlow(bf320,'Re',177);
bf110 = SF_BaseFlow(bf177,'Re',110);

%% alternative : reading from database
bf320 = SF_Load('BASEFLOWS',12);
bf177 = SF_Load('BASEFLOWS',13);
bf110 = SF_Load('BASEFLOWS',14);


%% plot the base flow
figure;title('Base flow for Re = 110 and 320');
subplot(2,1,1);SF_PlotBF(bf110);  % plot with global options
subplot(2,1,2);SF_PlotBF(bf320);  % plot with global options
pause(0.1);


%% plot ux(r) along a line and compare with Annular Poiseuille
% extract ux along a line
rline = linspace(.5,.5/sqrt(.75),100);
bf320line = SF_ExtractData(bf320,'ux',0.75,rline);
bf110line = SF_ExtractData(bf110,'ux',0.75,rline);
bf320line2 = SF_ExtractData(bf320,'ux',1.25,rline);
bf110line2 = SF_ExtractData(bf110,'ux',1.25,rline);
% theory (Annular Poiseuille)
r1 = .5;r2 = .5/sqrt(.75);
D = log(r2/r1)*(r1^2+r2^2)+r1^2-r2^2;A = -1/D;B = (r1^2+r2^2)/D;C = r1^2/D;
utheo = A*rline.^2+B*log(rline./r1)+C;
%% 
% plot
figure; plot(bf110line(1:2:end),rline(1:2:end),'r+',bf320line(2:2:end),rline(2:2:end),'bx',bf110line2,rline,'r--',bf320line2,rline,'b--',utheo,rline,'k:');
set(gca,'FontSize',21);xlabel('u_x');ylabel('r');
%legend('Re = 110','Re = 320','Theo.'); 
saveas(gcf,'BluntBody_Poiseuille','png')

%% Spectrum for Re = 400
[ev,em] = SF_Stability(bf,'solver','StabAxi.edp','m',1,'nev',10,'shift',1+1i,'PlotSpectrum',true)


%% Figure 9 : lambda vs Re for a/A = 0.75

%% 
% First loop over [300-178.75] in single-mode tracking to get branch O3
shift = .5+.5i; 
ReO1 =  [(300 :-10:190), 185, 182, 180, 179, 178.75];
evO1 = [];
bf = bf320;
 for Rei = ReO1
      bf=SF_BaseFlow(bf,'Re',Rei); 
      [ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'sort','LR','threshold','multiple','type','A');
      % note that we solve the adjoint problem ('type','A') ; this turns out to be much faster
      evO1 = [evO1,ev];
      shift = 'cont'; % will switch to continuation mode after first step : shift is interpolated 
 end
 
%% 
% Second loop over [178.75-100] in multiple-mode to get branches S1 and S2
ReSS =  [178.75, 178.5, 178, 176, 173, 170];
evSS = [];
shift = 0.3;
bf = bf177;
 for Rei = ReSS
      bf=SF_BaseFlow(bf,'Re',Rei); 
      [ev,em] = SF_Stability(bf,'shift',shift,'nev',4,'sort','LR','threshold','multiple','type','A');
      % note the option 'threshold','multiple' to catch the threshold occurring on the secondary branch
      evSS = [evSS,ev];
 end
 
%% 
% Third loop over [170-100] to get branch S1 
shift = .3;
ReS1 =  [170:-5:100];
evS1 = [];
shift = 0.3;
bf = bf177;
 for Rei = ReS1
      bf=SF_BaseFlow(bf,'Re',Rei); 
      [ev,em] = SF_Stability(bf,'shift',shift,'nev',1,'threshold','on','type','A');
      evS1 = [evS1,ev];
      shift = 'cont';
 end
 
 
 %% 
 % Recovers the two thresholds detected by threshold tracker
 sfs = SF_Status('QUIET');
 Rec = sfs.THRESHOLDS.Re;
 

 %%
 % plot the two leading eigenvalues
 figure(41);
 plot(ReO1,real(evO1),'m','LineWidth',2);
 xlabel('Re');ylabel('\lambda_r');grid on;ylim([-.5 1.5])
 hold on;
 plot(ReSS,real(evSS(1,:)),'b','LineWidth',2);
 plot(ReSS,real(evSS(2,:)),'r','LineWidth',2);
 plot(ReS1,real(evS1),'b','LineWidth',2);
 plot(Rec,0*Rec,'go','MarkerSize', 12)
 saveas(gcf,'asurA075_lambdaRe','png')
 %%
 figure(42);
 plot(ReO1,imag(evO1)/(2*pi),'m','LineWidth',2);
 xlabel('Re');ylabel('St');ylim([-.05 .2]);grid on;
 hold on;
 plot(ReO1,-imag(evO1)/(2*pi),'m','LineWidth',2);
 plot(ReSS,0*imag(evSS(1,:)),'b','LineWidth',2);
 plot(ReSS,0*imag(evSS(2,:)),'r','LineWidth',2);
 plot(ReS1,imag(evS1),'b','LineWidth',2);
 saveas(gcf,'asurA075_StRe','png')



%% Mode S1 for 110
[ev,emS1] = SF_Stability(bf110,'solver','StabAxi.edp','m',1,'nev',1,'shift',0) 
%%
% Plot in the longitudinal plane (using global options defined in preamble)
emS1 = SF_LoadFields(emS1);
figure; SF_PlotMode(emS1);
saveas(gca,'modeS1','png')

%% 
% Plot in a transverse plane
rline = [0.01:0.001:0.577];thetaline = [0:2*pi/40:2*pi];
Y = rline'*cos(thetaline);Z = rline'*sin(thetaline);
emS1line = SF_ExtractData(emS1,2,rline);
VORTX = real(emS1line.vortx'*exp(1i*thetaline));
figure;contourf(Y,Z,VORTX);xlabel('y');ylabel('z');
c = colorbar;c.Label.String = '\omega_x';
saveas(gca,'modeS1_omegax','png')

%% Mode S2 for 177
[ev,emS2] = SF_Stability(bf177,'solver','StabAxi.edp','m',1,'nev',1,'shift',0.)
%%
% Plot in the longitudinal plane (using global options defined in preamble)
emS2 = SF_LoadFields(emS2);
figure; SF_PlotMode(emS2);
saveas(gca,'modeS2','png')
%% 
% Plot in a transverse plane 
emS2line = SF_ExtractData(emS2,2,rline);
VORTX = real(emS2line.vortx'*exp(1i*thetaline));
figure;contourf(Y,Z,VORTX);
c = colorbar;c.Label.String = '\omega_x';
saveas(gca,'modeS2_omegax','png')


%% Summary for [[PUBLISH]]
sfs = SF_Status; 


