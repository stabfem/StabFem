% !TEX root = main.tex


\chapter{Getting started}

\section{Requirements}

\subsection{System}
StabFem works perfectly on Ubuntu (16 or later), MacOS (10.10 or later), and Windows 10.
For other systems please contact the developers.

\subsection{FreeFem++}
StabFem is a Matlab/Octave driver for the finite element software FreeFem++. Consequently it is required to install FreeFem++ on your computer before you use it. 

Most of the cases included in rhe project run with FreeFem++ version 3.61. However, a few among the most recent cases require FreeFem++ 4.7.


There are two ways to install FreeFem++ : $(i)$ get a precompiled version for your system, 
or $(ii)$ download the sources from github and compile.
For both methods, see the FreeFem github repository : \url{https://github.com/FreeFem/FreeFem-sources/releases}

Precompiled versions are perfect for basic usage and testing. However, they do not always provide the full set of possibilities, especially considering the choice of solvers (in particular, PETSc/SLEPc is not always present). To benefit of all the power of FreeFem++ and StabFem it is recommended to compile the program from the sources.

\subsection{Matlab/Octave}
In addition it is required to install either Matlab (version 2017b or later) or Octave (version 5.1 or later).

\section{Download StabFem}
You may download the project in two ways:

First, by direct download of the project on the gitlab site (in this way you may download a single specific branch as required).

Second, you may use "git clone" to get the full project including all branches (you may need to install a git client on your computed depending on your system).

It is recommended to install everything in your root directory, so that the name of the folder containing all the project will be \shell{$\sim$/StabFem/}.

\subsection{Contents of the repository} 

\ti{StabFem} has a main directory where all the projects are located and where the source codes are located too. The directory is composed by the following particular project directories:

\begin{enumerate}

\item Directory \sffile{TUTORIAL\_CASES/}  contains a number of simple examples, mostly coming from the {\em Learning by youreslf} section of the FreeFem manual.
This is where you should start if you are a newcomer to \freefem.

\item Directory \sffile{STABLE\_CASES/}  contains a number of validated cases, considering mostly fluid instability problems : 
\sffile{ACOUSTIC\_PIPES}, \sffile{CYLINDER}, \sffile{CYLINDER\_VIV}, 
%\sffile{DISK\_IN\_PIPE}
...

Each of these directories for stable and turorial cases normally contain an \mlfile{autorun.m} which is used as a non-regression test-case and regularly run automatically by the git tools of the project.  Some of them are written as tutorials and are published on the gitlab/pages website of the project. You are encouraged to have a look at these scripts to learn how to use the software !

% but normally you should not modify them.



\item Directory \sffile{DEVELOPMENT\_CASES/} contains a number of "work in progress" files. If you want to contribute to the project by starting a study on your own new case, you are encouraged to create a folder for your case in this directory.


\item Directory \sffile{TEACHING\_CASES/} contains a number of cases used for education (currently at Universit\'e Paul Sabatier, Toulouse).

\item Directory \sffile{PUBLICATION\_CASES/} regroups configurations for which StabFem has been used in published research papers. The folders typically contain one or several scripts producing sample results and figures from the associated paper.

\item Directory \sffile{SOURCES\_FREEFEM} contains a number of generic FreeFem++ solvers. These solvers are generic to a class of problems and normally you should not modify them. If you need to customize to your cases, the procedure is either to add your own solvers in the folder corresponding to your case, or to use the generic ones and design customisable macros in you local \fffile{SF\_Custom.idp} file, as explained in chapter 4 of this manual.


\item Directory \sffile{SOURCES\_MATLAB} contains the Matlab/Octave drivers and internal functions. 

%\item Directory \sffile{SOURCES\_OCTAVE} contains a few functions specifically needed by Octave and not needed if using Matlab.

\item Directory \sffile{99\_Documentation} contains some documentation, including the sources of this manual !
\end{enumerate}

\subsection{The website} 

In addition to the gitlab repository, \stabfem offers a dynamically generated website designed to share the codes in a "noteboook" format. The website has to entry doors:


\begin{itemize}

\item The main index \url{https://stabfem.gitlab.io/StabFem} presents a selection of well documented cases, including tutorials and a number of codes used in published research papers.


\item The full index \url{https://stabfem.gitlab.io/StabFem/AutomaticIndex.html} (also available under category "all codes" of the main index) regroups all cases currently 
published on the website and is dynamically regenerated at each new addition.
\end{itemize}


\section{Entering StabFem}

\subsection{Starting to use StabFem}
To start using StabFem, open Matlab or Octave and type the two following lines :

\mlcode{ addpath('$\sim$/StabFem/SOURCES\_MATLAB'); }

\mlcode{ SF\_Start; }

The effect of the first command is to give to Matlab/Octave access to the folder containing all programs.
If you have installed elsewhere that in your root directory, the path may be modified. You can also provide a relative path (such as \mlcode{'../../SOURCES\_MATLAB'}). You may also add the path permanently through preferences of Matlab or Octave (but this is not recommended). 

Then, the starter program \SF{Start} is called. The role of this program is to detect how and where FreeFem++ is installed, perform a series of tests to check if everything is correctly installed, and set a number of global variables required for correct operation of StabFem.



\subsection{ Data Base Management } 

\stabfem \, offers two different modes to manage the output files produced by your  \freefem programs:

\begin{itemize} 
\item 
When working in {\em Data Base mode},  all results files produced by the \freefem programs are stored and sorted in a {\em Data Base  } folder.
This is very powerful as all results produced at any step of your work can be easily recovered using \SF{Status} and \SF{Load}.

To enable the Data Base Management, the preamble of your scripts should contain the command :

 \mlcode{ SF\_Database( 'create'/'open',  mydatabase  )}
 
This will initiate the Database management  in a folder called \mlcode{mydatabase} ; for instance \mlcode{'./WORK/'}. Note the difference between \mlcode{'open'} 
and \mlcode{'create'} : 
 

 \begin{itemize} 
\item Using \mlcode{'open'}, if the specified database folder already exists, it will be opened and the computation done within your working session will be appended to it. If the folder does not exist it will be created.
 
\item Using \mlcode{'create'}, a new database folder will be created. If a folder with the same name was already present it will be desctroyed and recreated from scratch.

\end{itemize}

\item 
When working in {\em disabled Data Base mode},  all results files produced by the \freefem programs are simply left in the current directory.

To disable the Data Base Management, the preamble of your scripts should contain the command :

 \mlcode{ SF\_Database( 'disable' );}


This mode is used in some of the tutorials as it is simpler to follow the execution of the successive \freefem programs. 
It can also be useful in debugging mode. However, some advanced functionalities of StabFem (mesh adaptation, etc..) 
may not work correctly in this mode, so it is not recommended to use it in the general case.


\end{itemize}









\subsection{Global variables}



\begin{table}
\begin{tabular}{lll}
\hline
Verbosity level & code &  Signification \\
\hline
0 &      & Nothing displayed (for autorun mode only) \\
1 & 'e'  & Only error messages \\
2 & 'w'  & Errors + Important warnings \\
3 & 'n'   & Notice messages \\
4 & 'nn' & Notice messages + FreeFem messages \\
5 & 'l'    & Legacy messages \\
6 & 'd'   & Debug \\
7 & 'dd' & Debug+ \\
8 & 'ddd' & Debug++ (including full display of FreeFem errors) \\
\hline
\end{tabular}
\caption{ Signification of verbosity levels }
\label{tab:verbosity}
\end{table}



For good operation, StabFem require the definition of a number of global variables.  Most of them are set automatically by \SF{Start} and normally you don't need modify them. However, you can change the value of an individual global variable using the command \SF{core\_setopt('variable','value'}). 

Here is a list of the most useful variables which you may modify to adapt to your needs:


\begin{description}
\item{\mlcode{'verbosity'}} controls the level of verbiage of the software. See table \ref{tab:verbosity} for possible values and their significance. Default value is 2. Recommended values for basic usage are 2 or 4; others values are designed for developers. 

\item{\mlcode{'freefemexecutable'}} is the full address of the FreeFem++ executable installed on your computer.
Normally this address is detected automatically by \SF{Start}. If this does not succeed, you may set it by hand, for instance: 

\mlcode{SF\_core\_setopt('freefemexecutable','/usr/local/mybin/FreeFem++-mpi')}.

\item{\mlcode{'ffdatadir'}} is the name of the "Data Base" folder where all results are stored. Normally this variable should be set by \SF{DataBase} and not directly by 
 \SF{core\_setopt}. See section \ref{sec:DB} for details on this point. Default is \mlcode{'./WORK/'}.

\item{\mlcode{'ffdir'}} is a cell-array of strings corresponding to the folders where FreeFem++ (\shell{.edp}) programs are expected to be found. 

Default is \mlcode{ \{'$\sim$/FREFEM\_SOURCES/','$\sim$/FREFEM\_SOURCES\_PRIVATE/'\} }.

If you want to add a personal folder to this list you can do for instance: 

\mlcode{ SF\_core\_setopt('ffdir',[SF\_core\_getopt('ffdir'),'$\sim$/MyFFCodes']) }

\item{\mlcode{'ffinclude'}} is a cell-array of strings corresponding to the folders where FreeFem++ "include" (\shell{.idp}) files are expected to be found. 

Default is \mlcode{ \{'$\sim$/FREFEM\_SOURCES/INCLUDE/','$\sim$/FREFEM\_SOURCES\_PRIVATE/INCLUDE/'\} }.

\item{\mlcode{'VhList'}} is a list of fe-spaces (specified as a string with entries separated by commas) for which the driver will produce the "finite element connectivity" data necessary for plotting the fields (see chapter 3). Default is \mlcode{'P2,P2P2P1'}.


\item{\mlcode{'solver'}} is the name of the library used for solving linear problems. Default is \mlcode{'MUMPS'} if available. 


\item{\mlcode{'eigensolver'}} is the name of the library used for eigenvalue computations. It is either \mlcode{'ARPACK'} or \mlcode{'SLEPC'} . SLEPc is much more efficient and now available on most versions of FreeFem++. Some old cases still use ARPACK but it is not recommended to use it any more.


\end{description}

\Remarks{
\item 
Note that some of the global variables can be specified directly when calling \SF{Start} instead of using  \SF{core\_setopt} afterwards. For instance : \mlcode{SF\_Start('verbosity',4);} will set directly the verbosity level and the database manager address.

\item
If you want to see the list of all available global variables, just type \SF{core\_getopt}.

\item The variable \mlcode{'verbosity'} can also be modified with the shortcut command  \mlcode{SFverb}. For instance \mlcode{SFverb(4)}
is equivalent to \mlcode{SF\_core\_setopt('verbosity',4)}.
  }

\subsection{Notes on the interface with FreeFem++}
\label{sec:preamblefiles}
Interface between the FreeFem++ solvers and the Octave/Matlab layer requires a few files, either created by the user or internally created by the drivers. Here is a list :

\begin{itemize}


\item \shell{freefem.pref} is a file which tells FreeFem++ the paths where to look for .edp and .idp files.
This file is internally created according to the global variables \mlcode{ffdir} and \mlcode{ffinclude} and should not be modified.

%\item \shell{workdir.pref} contains the name of the database folder, and is internally created according to the global variable \mlcode{ffdatadir}. In your FreeFem++ programs, this file is read using a macro as follows: \ffcode{string ffdatadir = SFDetectWorkDir();}.

\item \shell{SF\_AutoInclude.idp}\footnote{Formerly called \shell{SF\_VersionDependent.idp}} contains a few statements dependent of the specificities of your FreeFem installation, in particular the "load" of solvers to be used (MUMPS, SLEPc or ARPACK, etc...), and possibly macro definitions. This file contains also definition of the variable \ffcode{ffdatadir} corresponding to the name of the database folder, possibly the variable \ffcode{themeshfilename} specifying which mesh as to be used,  
and the variable \ffcode{VhList} defining the kinds of vectorized elements ([P2,P2,P1], [P2,P2,P2,P1], etc...) to be recognized by the interface (see chapter 3).
This file is internally created by the drivers (see sec. 2.2.2) and should not be modified. 

\item \shell{SF\_Tools.idp} contains a number of macros specific to the StabFem project. This file is in the common folder \shell{SOURCES\_FREEFEM/INCLUDE/}. 
You should normally not modify its content.

\item \shell{SF\_Custom.idp}\footnote{Formerly called \shell{Macros\_StabFem.idp}} may contain case-dependent declarations or macros (customizable macros for stability-oriented codes, etc...). See chapter 4 for explanation on customizable macros.
\end{itemize}

Thus, the preamble of your FreeFem programs should normally contain the following lines:

\ffcode{\qquad include "SF\_Custom.idp"}
 
\ffcode{\qquad include "SF\_AutoInclude.idp"} 

\ffcode{\qquad include "SF\_Tools.idp"}	


For simplicity, in most programs, these three lines can be replaced by a single one as follows:

\ffcode{\qquad include "StabFem.idp"}






\section{ Using StabFem solvers outside of Matlab/Octave} 



If you don't have (or don't like) the Matlab/Octave environment, you can perfectly use the \freefem part of the StabFem project directly in a bash terminal ! 
 
For instance figure \ref{fig:stabfemoutsidematlab} shows how to perform the mesh adaptation and eigenmode computation (from the the example script at the beginning of chapter 4) in bash mode. This sequence is extracted from a file \shell{.stabfem\_log.txt} which is created each time you run a StabFem script in Matlab/Octave. Do not hesitate to look into those files to understand the behaviour and/or for debugging ! 

Note that running everything from a bash script requires to carefully copy the .txt /.msh files generated by \freefem into the names expected at input of next solvers. The Matlab/Octave drivers do all these manipulations automatically, and further automatically store all files in an indexed database.

When using StabFem in this way you should also take care to correctly create and position the required interface files described in the previous section ; namely, your directory should contain three files called \shell{freefem.pref} \fffile{SF\_Custom.idp} and \fffile{SF\_Autoinclude.idp}. If you are not sure whet they should contain, you can simply take these files from an existing case.












%\medskip

%\begin{leftbar}
%Attention: When you do some change in the mutual directory files, we have to assure that it will work on all the projects.
%\end{leftbar}


%\section{General Features}

%(section to be rewritten)

%\begin{itemize}
%\item With \ti{StabFem} we can solve forced problems, eigenvalue problems, etc...

%\item  \ti{StabFem} allows us to save the scripts used to generate the different data and graphs, enabling to reproduce the same results (useful for repeat figures for articles, confirming results, etc...).
%\end{itemize}



%\section{What is attended from this manual document}
%This document presents this first chapter describing what can be found in a  \ti{StabFem} project, a chapter describing the running logic of a \ti{StabFem} program and the main common files and a chapter for each project, describing succinctly their running features.
