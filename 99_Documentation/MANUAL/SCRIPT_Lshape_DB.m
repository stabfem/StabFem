%% Tutorial example demonstrating DATABASE management (extract from program SCRIPT_Lshape_DataBase.m)

%% Part I. Initialization and computations
addpath('../../SOURCES_MATLAB/');
SF_Start;
SF_DataBase('create','./WORK/'); 
(... same computations as in previous example ...) 

%% Part II. Post-processing from the DataBase
clear all;
SF_Start;
SF_DataBase('open','./WORK/'); 
 % To see what is available in the database, just use "SF_Status" to get a summary :
sfs = SF_Status
% How to load a result from the database and plot it 
heatS = SF_Load('HeatSteady',1);
figure(1);SF_Plot(heatS,'T','xlim',[0 1],'ylim',[0 1],'title', 'Steady conduction on a L-shaped domain: Temperature field ');
