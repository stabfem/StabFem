% !TEX root = main.tex

\chapter{Using StabFem for Time-Stepping simulations}

Time-stepping simulations, namely numerical resolution of a time-dependant problem using a temporal loop,  are a very common type of computations in all fields of engineering and physics. The \stabfem suite offers specific drivers devoted to monitor such computations, namely \SF{TS\_Launch} to launch a simulation and \SF{TS\_Check} to check the status and import the results.
This chapter explains how to use these drivers and how to adapt your \freefem programs to allow useage of these drivers.

\subsection{What is provided}

The presentation on this chapter is based on several examples, all available from the gitlab archive of the project and published as tutorials on the website of the project :
\begin{itemize}
\item 
\href{https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Unsteady_Thermal_Conduction/SCRIPT_ThermalConduction_Basic.html}{\mlfile{TUTORIAL\_CASES/Unsteady\_Thermal\_Conduction/SCRIPT\_ThermalConduction\_Basic.m}}
is a basic example, based on the time-stepping example available from the {\em learning by examples }section of the \freefem manual. This example considers the very simple case of unsteady heat equations in a closed domain.
For simplicity, this example Data Base management is disabled, so that one can easily follow all the steps and how output files are processed.

\item 
\href{https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Unsteady_Thermal_Conduction/SCRIPT_ThermalConduction_Advanced.html}{\mlfile{TUTORIAL\_CASES/Unsteady\_Thermal\_Conduction/SCRIPT\_ThermalConduction\_Advanced.m}}
is a more elaborated example, also based on the time-stepping example available from the {\em learning by examples }section of the \freefem manual. This example shows how to use the Data Base to store the results of several successive simulation, launch simulations in noon-interactive mode, possibly restart or stop them, and relad results from rhe database.


\item
\href{https://stabfem.gitlab.io/StabFem/STABLE_CASES/DNS_CYLINDER/SCRIPT_DNS_EXAMPLE.html}{\mlfile{STABLE\_CASES/DNS\_CYLINDER/SCRIPT\_DNS\_EXAMPLE.m}} is a more elaborate example, which shows how to perform a DNS of the wake flow around a 2D cylinder and observe the development of the well-known von Karman vortex street. This example also shows how to initiate the simulation with results from a linear stability analysis, and benefit from an adapted mesh.

\item 
\href{https://stabfem.gitlab.io/StabFem/TUTORIAL_CASES/Falling_Ellipse/SCRIPT_Falling_Ellipse.html}{\mlfile{TUTORIAL\_CASES/Falling\_Ellipse/SCRIPT\_Falling\_Ellipse.m}} is another example of Direct numerical simulation of a fluid flow, for the case of an elliptical body in free fall within a viscous fluid. Compared to the other cases, this one uses a dynamic mesh, recomputed at each time step. 

\item 
\href{https://stabfem.gitlab.io/StabFem/PUBLICATION_CASES/HoleImpedance_FabreEtAl/SCRIPT_DNS_PulsatedJet.html}{\mlfile{PUBLICATION\_CASES/Forced\_Jet/SCRIPT\_Forced\_Jet.m}} is yet another example of Direct numerical simulation of a fluid flow. This one is based on a time-stepper in axisymmetric coordinates.


\end{itemize}


\subsection{Legacy notes}

Time-stepping simulations were initially introduced in StabFem in 2019, with a first driver called \SF{DNS} mostly designed for Direct Numerical Simulations in fluid mechanics. 
Everything was redesigned in June 2021 with the new drivers \SF{TS\_Launch} and \SF{TS\_Check} which introduced the possibility of launch simulations in a non-interactive way and and a simplified and more powerful management of database. The old driver  \SF{DNS} is still available and can be found in some old examples available in the website, but it is advised to switch to the new drivers.


\section{Usage of the drivers}

\subsection{Launching a simulation within \stabfem : \SF{TS\_Launch} }

The general syntax of \SF{TS\_Launch} is as follows:
\setlistmatlab
\lstinputlisting{SF_TS_Launch_Syntax.txt}

The return value of the driver \SF{TS\_Launch} is a {\em handle} which corresponds to the name of the folder where results have been stored. The possible arguments are as follows:

\begin{itemize}

\item \mlcode{'Options'} is the set of options to be used when launching the \freefem time-stepper program. The syntax is identical to that used for \SF{Launch}, namely the value can be $(i)$ a cell array of descriptor/value pairs, $(ii)$ a structure, or $(iii)$  directly a string. 

\item \mlcode{'Mesh'} must be the handle of an existing mesh.

\item \mlcode{'Init'} must be the handle of an dataset which will be take as an initial condition for the time-stepping simulation. Note that when providing an initial condition it is not mandatory to provide a mesh as well, since information about the mesh should normally be contained within the initial condition handle.

\item \mlcode{'wait'} option is for the operation mode:
\begin{description}
\item{$(i)$} Using \mlcode{'wait',true} means that the simulation will be launched in {\em interactive mode } : Matlab/Octave will wait until the completion of the simulation before exiting from  \SF{TS\_Launch} and allowing the user to do anything else in the same session.

\item{$(ii)$} Using \mlcode{'wait',false} means that he simulation will be launched in {\em non-interactive mode } : Matlab/Octave will launch the simulation as a remote job and will exit from \SF{TS\_Launch}, hence allowing the user to keep on working in the same session.

\end{description}
 
 The {\em Non-interactive mode} is especially useful if you are lead to run simulations which are expected to run for several hours. It also allows to launch several simulations at the same time and run them simultaneously (provided your computed has a sufficient number of processors).

%\item \mlcode{'samefolder'} : if setting  this option to \mlcode{true}, the results will be written in the same DataBase folder than the initial condition instead of a newly created folder. This option is useful if you want to restart a previously finished simulations and want to continue it up to longer times using the same options  (default is \mlcode{false})  
 
 \item \mlcode{'folder'}  is used to specify the index of the directory where the data will be stored. If not provided, results will be stored in subdirectory \shell{TimeStepping/RUN000001/} for the first launched simulation, in folder   \shell{TimeStepping/RUN000001/} for the second simulation, etc... with incremental indexing. You may change this by specifying the number of the folder, for instance specifying  \mlcode{'folder',100} will store the results in a folder \shell{TimeStepping/RUN000100/}. 
 

%Note also that  
 
\end{itemize}


The \freefem  time-stepper program is expected to produce two kinds of files : the time-series  in files \shell{TimeStats.txt}/\shell{.ff2m} 
(column-ordered data), and the snapshots called \shell{Snapshot\_(iter).txt}/\shell{.ff2m} (mesh-associated data). %These files are written in a folder (internally called \ffcode{ffdatadir}) as follows: 
All are written in a results folder  %\shell{TimeStepping/RUN/} 
as explained above.
\footnote{ Except when Data Base management is disabled, such as in the simple example \shell{SCRIPT\_ThermalConduction.m}. In this case, all these files are simply left in the current directory. }

%(except if 

Note that it is perfectly allowed to specify with option \mlcode{'folder'} the number of an already existing directory. In this case the results will be appended to the existing folder.


%\begin{itemize}
%\item
%When Data Base management is enabled, all these files will be written directly in a folder with incremental index ; 
%for instance a folder \shell{./WORK/TimeStepping/RUN000001/} will be created for the first run, a folder \shell{./WORK/TimeStepping/RUN000002/} for the next one, etc...

%\item
%When Data Base management is disabled, such as in the simple example \shell{SCRIPT\_ThermalConduction.m}, all these files are simply left in the current directory. 
%This mode may be useful in debug mode to understand exactly what is done, but you can only launch a single simulation, not a series of run. 
%\end{itemize}

\begin{figure*}[ht]
\setlistfreefem
\lstinputlisting{../../TUTORIAL_CASES/Unsteady_Thermal_Conduction/TimeStepper_ThermalConduction_short.edp}
\caption{
\freefem Time-stepping program \fffile{TimeStepper\_ThermalConduction.edp}.}
\label{TimeStepper_ThermalConduction.edp}
\end{figure*}



\subsection{Checking simulation results : \SF{TS\_Check} }

After launching a time-stepping simulation you may want to import the results within your Matlab/Octave session. This is done by  \SF{TS\_Check}.
There are several ways to use this driver according what you want to import :
 
 \begin{description}
\item{$(i)$} \mlcode{ [TimeStats,Snapshots] = SF\_TS\_Check(run)}

This is the syntax to be used if you want to import both time series and all snapshots from a single run.

Here, \mlcode{run} refers to the time-stepping simulation you want to recover, it is either a numerical index (e.g. \mlcode{2}) or a string corresponding to the corresponding folder in the database (e.g. \mlcode{'./WORK/TimeStepping/RUN000002/'}).


The return object \mlcode{TimeStats} is a structure obtained from reading the files \shell{TimeStats.txt}/\shell{.ff2m} containing time-series statistics, allowing to plot 
any global post-processing quantity managed by you solver (e.g. the forces exercted on a body) as function of time.

The second return object \mlcode{Snapshots} is an array of dataset handles corresponding to all snapshots found in the folder corresponding to the desired run. 

\item{$(ii)$} \mlcode{ TimeStats = SF\_TS\_Check(run)}

Using this alternative syntax, only time-series are imported from the database, not the snapshots. This may be far more rapid as only files \shell{TimeStats.txt}/\shell{.ff2m} will be read. Note that importing only one snapshots instead of the whole series can be done using \SF{Load}, as explained below. 

\item{$(iii)$} \mlcode{ AllTimeStats = SF\_TS\_Check}

In this third alternative syntax, all folders containing Time-stepping results found in the database are explored and the contents are returned as an object \mlcode{AllTimeStats} corresponding to an array.

\item{$(iv)$} \mlcode{ [TimeStats,Snapshots] = SF\_TS\_Check(0)} is an alternative syntax to be used for {\em disabled Data Base mode} only.

\end{description}

\Remarks{

\item \SF{TS\_Check} works both if the simulation was completely finished or if it was launched in non-interactive mode and is still running. In that case the function will return the output files produced at the current state of the simulation.

\item In addition to returning results in objects \mlcode{[TimeStats,Snapshots]} usable for post-processing,  \SF{TS\_Check} displays on screen important informations about the simulation (initial condition file, list of options used at launching, status, ...). If several successive runs are launched in the same folder (for instance if the simulation was aborted and restarted), the driver will give information allowing to retrace the whole history of runs. It also displays information on available snapshots, including detected metadata, but to avoid generating long and useless logs, only the two first and two last ones are displayed on screen. On the other hand, information on all snapshots can be obtained through the second (optional) output object \mlcode{Snapshots}.
}


\subsection{Loading snapshots from the database : \SF{Load}}

As already explained in Sec. 2.4, any results stored in the database can be reopened using \SF{Load}.
As database management is slightly different for Time-Stepping than in other kinds of simulation, the syntax is different :

%\setlistmatlab
%\lstlisting{
\mlcode{
handle = SF\_Load('TimeStepping',nrun,iter)
}

Where \mlcode{nrun} in the index if the run, and \mlcode{iter} is the iteration number corresponding to the desired snapshot. For instance,
 \mlcode{handle = SF\_Load('TimeStepping',2,1500)} will import the snapshot corresponding to iteration 1500 from the second simulation which 
 was launched in the current working session (i.e. the second simulation stored in the database); in that case the driver will load the file 
 \shell{./WORK/TimeStepping/RUN000002/Snapshot\_00001500.txt}




\subsection{Finishing a running simulation : \SF{TS\_Stop} }

As it is possible to launch simulations simultaneously in non-interactive mode, several simulations may keep on running on your computer in the background.
You may want to stop one of those. Instead of "killing the process" which may result in incorrect termination, there is a procedure to stop the process in a clean way:
 
 \SF{TS\_Stop( run )}.
 
In practize, the effect of this function is to erase a file \shell{TS.status} existing in the folder where the simulation is running, which gives a signal of "clean exit" to the 
 Time-stepper program. Thus you may also directly remove this file.

When terminating a simulation in such a way, a call to \SF{TS\_Check} will return all results up to the iteration were the simulation was stopped. 
You may check that the simulation status indicated by  \SF{TS\_Check} will be \mlcode{'stopped'}.  
 



\section{How to design your \freefem Time-stepper program to work within the driver ?}



To be able to work correctly under the monitoring of the \SF{TS\_Launch} and \SF{TS\_Check} drivers, your \freefem Time-steppers have to be 
redesigned so that the input/output files and parameters are compatible. To explain this, consider the program \fffile{TimeStepper\_ThermalConduction.edp} 
displayed in Fig. \ref{TimeStepper_ThermalConduction.edp}. This program is adapted from an example in the {\em learning by examples} section
in the FreeFem manual. The reader is encouraged to compare the two versions of the program to understand what is important to add.
Here are a few explanations and advices which should allow you to easily adapt any existing time-stepped to run within \stabfem:
\begin{itemize}
\item First, the input parameters have to be detected using \ffcode{gatARGV} (lines 7-17). The {\em physical parameters} are case-specific and should be implemented according to the users's needs. On the other hand, for {\em numerical parameters} it is strongly advised to stick to the conventions and use the same names as in the examples since these parameters are also recognized by the driver. So please respect names \ffcode{Niter, dt, i0, t0, iout, istat} for constant-timestep simulations (possibly \ffcode{dtmax, tmax} for adaptative timesteps simulations). 
\item The mesh has to be opened with generic name \ffcode{themeshfilename} (line 20).
\item The initial condition has to be opened with generic name  \ffcode{ffdatadir+"Init.txt"} (line 29). Note that if no initial condition is specified you may specify a default initial condition as in the example (line 27-36).
\item Before the time loop, you have to open the time-statistics file \shell{TimeStats.txt} and write the associated descriptor file \shell{TimeStats.ff2m}. Note that 
\shell{TimeStats.txt} has to be opened in "append" for good operation when using the \mlcode{'samefolder',true} option as described above.
\item Time-statistics are expected to be written each \ffcode{istat} iterations (lines 82-83).
\item Snapshots are expected to be written each  \ffcode{istat} iterations (lines 84-85). Note that in the example this is done through a macro \ffcode{SFWriteField} 
defined on lines 56-68.
\item Finally, in order to let the driver know the status of the simulation, your programs should use a file \shell{TS.status}. This file is first written on line 72 to indicate \ffcode{"running"} and finally at line 91 to indicate \ffcode{"completed"}. This file is also referred to at line 87 when calling the macro \ffcode{CleanExitFromTimeStepping}.
The role of this macro, defined in \fffile{SF\_Tools.idp}, is to check if a "clean exit" signal was produced by intentionally erasing this file. If so, a "clean exit" from the time 
loop is done, and the file is \shell{TS.status} is re-created to indicate a status \ffcode{"stopped"}.

Note that in case of a possible divergence of the time-stepper, you should implement a divergence test and if so write a status \ffcode{"diverged"}  in file \shell{TS.status}
and terminate your FreeFem code with \ffcode{exit(203);}
  (see more advanced examples in the \stabfem project, e.g. \shell{SOURCES\_FREEFEM/TimeStepper\_2D.edp}

\end{itemize}
 
 \clearpage
\section{Launching a simulation outside \stabfem \, and using \stabfem \, for postprocessing}

In some cases, it may be simpler to launch the simulations outside \stabfem but still use \stabfem to do the postprocessing.
For instance, if you are launching a simulation on a remote server such as a multicore cluster, and working Matlab/Octave is not available (or it is not practical to open a Matlab/Octave session), it may be simpler to launch the computation from shell command or from a shell script. You can simply do this with a sequence of shell commands as follows:

{\small
\shell{ mkdir ./WORK/TimeStepping/RUN000005 }

\shell{ echo 'macro ffdatadir() "./WORK/TimeStepping/RUN000005/" //EOM' > SF\_AutoInclude.idp}

\shell{ echo 'macro themeshfilename() "./WORK/MESHES/FFMESH\_000001.msh" //EOM' >> SF\_AutoInclude.idp}

\shell{ echo 'macro theinitfilename() "./WORK/TimeStepping/RUN000001/Snapshot\_00000100.txt" //EOM' >> SF\_AutoInclude.idp}

\shell{ FreeFem++-mpi -nw -v 0 TimeStepper\_ThermalConduction.edp  -Niter 100 -alpha 0.25  -i0 100 -t0 10}
}

After this, opening Matlab/Octave and using SF{TS\_Check} will normally allow to access all results just as explained in the previous section.


For more details see the example :

\shell{TUTORIAL\_CASES/Unsteady\_Thermal\_Conduction/LaunchSingleComputation\_Example.bash}




 










