% !TEX root = main.tex

\chapter{How to add your own cases in StabFem } 


At this stage, you should have understood enough about the working logic of the StabFem project to begin incorporating your own cases of study. This section will explain how to proceed.

\section{Anatomy of a StabFem project}

Each StabFem study consists of a set of files contained in a directory which should be either a sobfolder of 
\shell{DEVELOPMENT\_CASES} (for still ongoing studies) or  \shell{STABLE\_CASES} (for completed studies).

Such a directory should contain :

\begin{enumerate}
\item The \mlcode{*.m} files corresponding to the Matlab/Octave scripts. Normally, a main script should be present, e.g. \mlfile{MyCase.m} ;

\item Your \freefem programs ; At least one mesh-generating FreeFem file, e.g.  \fffile{Mesh\_Mycase.edp}.

\item Optionally, the file \fffile{SF\_Custom.idp} containing customization choices. 

\item Optionally, the directory may contain some Matlab/Octave functions specific to your cases and used by your main script. For instance, many cases contain a function  \mlcode{SmartMesh.m} which generates a well-suited mesh starting with the .edp file and performing a series of mesh adaptations.

\item For "Stable" cases, the directory should also contain an \mlfile{autorun.m} program, 
which is designed to be used as an automatic non-regression test (see section ??). 


\end{enumerate}



%Once your study is sufficiently advanced, it is advised that you switch your files from the  \shell{DEVELOPMENT\_CASES} directory to the \shell{STABLE\_CASES} directory and that you create an 
%\mlfile{autorun.m} program performing elementary tests for your case. 

%If pushed on gitlab, such \mlfile{autorun.m} program will serve as automatic non-regression tests : namely, an automatic test will be launched after each new 'push' to the gitlab repository, to check that all cases are still operational.

\section{  Adapting from an existing case }

If the case you wish to study belongs to a class of problems already implemented in the project, then you should not have to modify any of the .m / .edp programs in the common directories. 
All adaptations to your cases should be made in your mesh generator program and possibly in the customizable macros.

The recommended procedure is to start from one of the cases already present in the project and close to the one you want to study, copy-paste the mesh-generator and custom-macro files in a new directory in the \shell{DEVELOPMENT\_CASES} section, and start modifying them.

For instance, if you want to study the wake of a 2D blunt body with elliptic section, you could start from the "CYLINDER" case and adapt to your needs.

WARNING : the syntax of StabFem has changed a little bit in the past years, and some of the codes you will find in the \shell{DEVELOPMENT\_CASES} section may use depreciated syntax. If you are not sure do not hesitate to ask the developer, we may give you indications about how to proceed to use the most recent syntax.


%\Remark{ 

%If you want to try modifications of the solver which require larger modifications than the customizable sections, If is also possible to tell the driver to use an alternative solver instead of the default one. For instance,  \mlcode{ bf= SF\_Baseflow(bf,'Re',10,'customsolver','Newton\_2D\_modified.edp')} will lanch a baseflow computation using \fffile{Newton\_2D\_modified.edp} instead of the default \fffile{Newton\_2D.edp}. In this case the altenative solver will generally be in the current directory, not in the common one. The same can be done using \SF{Stability} and a number of other generic drivers. Of course, this method requires that the input/output of the modified solvers respect the same syntax as the default one.
 

%This way of proceeding may be used in 'developing stage' to test new ideas, but should not be used any more  once your case has reached the "stable" status.

%}


\section{ Interfacing your own FreeFem++ programs }
 
%A number of classes of problems (incompressible, compressible, spring-mounted, etc...) are already incorporated in the project. However, it may turn out that the case you want to consider is not already present in the project.
%For instance, you are intested in Non-Newtonian flow with a given rheological law, or a MHD situation requiring to consider electric/magnetic fields, etc...

%In this case you will have to design your own Baseflow/Stability solvers (and possibly a few more depending on your needs) and "plug" them into the drivers.

If your problem does not fit with one of the classes of problems already present in the project (for instance, you are interested in Non-Newtonian flow with a given rheological law, or a MHD situation requiring to consider electric/magnetic fields, etc...) you will have to design your own FreeFem++ programs. Interfacing them with the driver is not a difficult task, and the recommended procedure is as follows:

\begin{enumerate}
\item First, design your FreeFem solvers and validate them outside of the StabFem interface.
(If you are already a	FreeFem user you may already have those programs at hand.)

\item Check that the input files have the desired format and the expected names.  (e.g. BaseFlow\_guess.txt, etc...). They should also be located in a working directory \ffcode{ffdatadir}.

\item Check also that the names and format of the output files (BaseFlow.txt, etc...) match with standards. Design as well the companion files .ff2m following the syntax explained in chapter 2. You may design macros SFWriteBaseflow , SFWriteMode for this purpose.

\item For input parameters, there are three possible ways. 
\begin{itemize}
\item If using generic interface, all parameters should be managed by getARGV and passed as \mlcode{'Options'}. 
\item you may also use the alternative (legacy) method where the selection of drivers is made thanks to a keyword 'problemtype' designed for your cases. In this case, the interfacing to your solver has to be done in the \mlcode{switch / case} branching structure according to this keyword. They should also be declared in the headers of the drivers using addParameter and/or SFcore\_addParameters (see explanations directly within the drivers).
\end{itemize}

\end{enumerate}

At this stage you are ready to use all the powerful facilities offered by the StabFem interface (including mesh adaptation, database management etc...). Have fun !

%\section{Using gitlab}

\section{Creating an autorun} 

If you have succeeded in implementing your own class of problems, 
it is highly advised that you switch it to the "stable" side of the arborescence and add an  \mlfile{autorun.m} into your directory.

Look at existing \mlfile{autorun.m} programs to see how they work : such programs  perform a series of elementary tests by comparing the results to reference values, and give a return value '0' if everything performs as expected, and a non-zero value otherwise.

All  \mlfile{autorun.m} programs present in the gitlab base of the project will be launched automatically each time new developments are "pushed", and an advice will be sent by e-mail to developers and maintainers in case of failure. It is thus highly advised to add such cases to the project, to be sure that your cases will remain compatible with future developments !


\section{Publishing your case on the website of the project}

The \stabfem project is associated to a website where all codes can be published in a dynamical way as "Notebooks".  To use this facility, here is what you should do :

\begin{itemize}
\item First, make sure you have installed everything by an initial \shell{git clone} so that your local installation can be synchronized with the remote gitlab server.

\item You should create a "branch" for your cases (see git documentation).

\item When designing the script you wish to publish, put the tag \mlcode{\% [[PUBLISH]] } somewhere in the code in a comment line (usually at the end).

\item  'git commit' your codes , with a commit message containing the tag \shell{[[PUBLISH]]}.

\item 'git push' on the remote server

\end{itemize}

Your script should execute on the server, and your case should be published after the execution time.

To check that everything works well, go to the 'pipelines' section in the gitlab interface.

\url{https://gitlab.com/stabfem/StabFem_Develop/-/pipelines}

\section{Note on the usage of gitlab}
  
The project is supported by the git subversion manager program. Here are a few tips/recommendations :

\paragraph{If you are contributing as a user}

\begin {itemize}

\item After the initial git clone, you may want to get the latest development of the main branch using {\em git pull}. This will only update the sources in the common repositories, not your own files. 

If you have made minor modifications to other files that you wish to keep in your local version but do not want to export to the main branch of the project, the procedure is to do successively:

\shell{git stash}

\shell{git pull}

\shell{git stash apply}.

(don't be afraid, any "mistake" with git can be undone!) 

\item 
If you want to incorporate your work in the project repository (normally after everything is validated...) you should do the following:

\shell{git chechout (mybranch)} to create your own branch. (it is not allowed to push direclty in the main branch).

\shell{git add (files)} .  

{\em NB  : Please add only source files of your case directory, such as freefem mesh generator and macro scripts, an example matlab script producing sample results for your case, and possibly matlab functions you have developed for your own case and are not sure they will work for other cases. Please don't add resul files (.txt/.ff2m) or graphical files (.png/.pdf) .}

\shell{git commit }

\shell{git push}

%{\em For this last step you need to be registered as a developer, just ask !}





\end{itemize}


\paragraph{If you are contributing as a developer}

\begin {itemize}

\item The best way is to create a "branch" : \shell{git chechout -b (mybranch)}

\item Once you have your own branch use git commit / git push as frequently as you wish !
Note that if the name of your branch contains "feature", an automatic non-regression test will be 
launched on a remote computed at each new "push". 

\item If you want to merge with the main branch, create a {\em merge request}.

\end{itemize}


\section{Explanations on Gitlab "Pipelines"}

In addition to providing a convenient development environment, the gitlab repository is configured to run automatically a 
number of 'pipeline" jobs on the server of the project hosted at IMFT. 
The pipelines can be launched either automatically at each commit/push, or manually through the gitlab interface (\shell{ CI/CD -> Pipeline }).


\subsection{Non-regression tests : autorun}

As explained previously, it is highly advised to create an \mlfile{autorun.m} file in each of the directories (at least stable ones).
There are currently about 30 of them. 

The \mlfile{autorun.m} programs may contain a tag with form \mlcode{\%[[AUTORUN:TAG]]} to identify them. For instance, a short list of autoruns designed to be run regularly is identified with tag  \mlcode{\%[[AUTORUN:SHORT]]}.

Automatic pipelines work as follows:


\begin{itemize}
\item The short list of autoruns   containing the tag  \mlcode{\%[[AUTORUN:SHORT]]} is run each time you push on a branch 
called \shell{develop\_(something)} or \shell{feature\_(something)}.

\item The full list of autoruns is launched each time a merge request is done where the target branch is  "master" or "develop".

\end{itemize}

You can also launch manually the pipeline through the gitlab interface. Go to CI/CD -> Pipeline,  click "Run Pipeline", select your branch and run.
You may select a subset of autoruns to run by setting a variable \shell{SFARVER} in the interface. For instance:

\begin{itemize}
\item Setting \shell{SFARVER} with value \shell{short} will launch the short list of autoruns  containing the tag  \mlcode{\%[[AUTORUN:SHORT]]} 

\item Setting \shell{SFARVER} with value \shell{MYCASES} will launch the short list of autoruns  containing the tag  \mlcode{\%[[AUTORUN:MYCASES]]} 

\end{itemize}

\subsection{Generating the web site}

The website contains a static page \shell{public/index.html} and a dynamic page \shell{public/AutomaticIndex.html} which is generated automatically by the gitlab runner.


\begin{itemize}
\item When your commit contains the tag \shell{[[PUBLISH]]}, a pipeline will be launched which will run in "Matlab Publish" mode all scripts containing the tag \mlcode{\%[[PUBLISH]]} which were added or modified at the commit.

 An html entry will be generated, and will be added to the "all cases" automatic index of the website.

\item The pipeline can also be launched manually by setting a variable \shell{GENERATE\_PAGE} with value \shell{1}.
 
\item Generation of the automatic index (without re-running each case) can also be launched manually by setting a variable \shell{GENERATE\_PAGE\_ONLY} with value \shell{1}.

\end{itemize}

\subsection{Generating the manual}

The manual (this document !) is also generated automatically from the latex source by a pipeline. To generate the manual there are two ways :

\begin{itemize}
\item Put a tag \shell{[[MANUAL]]} in your commit message.

\item The pipeline can also be launched manually by setting a variable \shell{MANUAL} with value \shell{1}.
 
\end{itemize}

\subsection{Exchanging data when running on the server (for IMFT users)} 

\begin{enumerate}
\item 
When launching a simulation on the StabFem server, you may want to get the data produced by the simulation on your local computer, for instance for doing your own post-processing.

You can do this by simply putting a tag  \mlcode{\%[[WORK]} in your Matlab script. 

A compacted archive of all the database will be generated and placed in the directory \shell{/work/SF\_works/} on the server. For instance, if your case is called  \shell{DEVELOPMENT\_CASES/MYCASE} and your database is called \shell{./WORK/} ,
then a file called something like 

\shell{/work/SF\_works/MYCASE/209499906\_20201030-0259\_MYCASE\_WORK.tar.gz}

will be created. You can then access on the server through ftp or some other way, and download it.

\item
Inversely, if you want to launch a simulation which requires some input data coming from a Data Base generated by a previous simulations (by a script generate in the same folder), you can put a line such as \mlcode{SF\_RecoverDataBase('WORK')} which will tell  the server to look for a tar.gz file in the directory \shell{/work/SF\_works/}.

\item A similar method can be used if your simulation requires large input data generated outside of StabFem (for instance a set of experimental data required for generating  comparison plots) and you want to run it on the server through the \shell{[[PUBLISH]]}  method. Instead of pushing the large data on gitlab (which should be avoided), you can put everything in an archive  \shell{MYDATA.tar.gz},
upload it on the stabfem server in the common directory \shell{/work/SF\_data/}. Then, your script should begin with \mlcode{SF\_RecoverDataBase('MYDATA')} to get the corresponding archive, unzip and unpack it.
\end{enumerate}

\subsection{Technical notes}
 
The rules of execution of the pipelines are in a file \shell{.gitlab-ci.yml} located in the root directory of StabFem.
 
 The jobs to be launched by the pipelines are in files  \shell{.gitlab-ci-(case).sh}.
