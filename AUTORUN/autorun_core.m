function autorun_core(mode)
% ================================================
% ==== AUTORUN SCRIPT USING SF_CORE FUNCTIONS ====
% ================================================
%
% Usage :
% autorun_core()  -> for automatic run
% autorun_core(1) -> manual run, short
% autorun_core(2) -> manual run, long list
%

tinit = tic;
if(nargin==0)
  mode = 0;
end



% ---------------------------
% -- LIST OF AUTORUN CASES --
% ---------------------------
% --------------------------
% -- SELECTING AN AUTORUN --
% --------------------------

% Pulling StabFem AutoRun Version from env. variables.
sfarver = getenv('SFARVER');
if isempty(sfarver)
    sfarver='short';
    if(mode>1)
      sfarver = 'long';
    end
end



D = dir('../STABLE_CASES/*/autorun.m');
D = [D ; dir('../STABLE_CASES/*/*/autorun.m')];
D = [D ; dir('../DEVELOPMENT_CASES/*/autorun.m')];
D = [D ; dir('../DEVELOPMENT_CASES/*/*/autorun.m')];
D = [D ; dir('../TEACHING_CASES/*/autorun.m')];
D = [D ; dir('../PRIVATE_CASES/*/autorun.m')];
D = [D ; dir('../PRIVATE_CASES/*/*/autorun.m')];
D = [D ; dir('../PUBLICATION_CASES/*/autorun.m')];
D = [D ; dir('../OBSOLETE_CASES/*/autorun.m')];

case_list = {};


if strcmp(sfarver, 'long')
    for i = 1:length(D)
        case_list = [case_list D(i).folder];
    end
else
    balise = ['[[AUTORUN:' upper(sfarver) ']]'];
    for i = 1:length(D)
        fid = fopen([D(i).folder '/' D(i).name], 'r');
        fcontent = textscan(fid,'%s','Delimiter','','endofline','');
        fcontent = fcontent{1}{1};
        fclose(fid);
        
        if ~isempty(strfind(fcontent,balise))
            case_list = [case_list D(i).folder];
        end
    end
end

if isempty(case_list)
    warning('The requested autorun is not defined.')
    exit(1)
end

% -------------------------
% -- Setting MATLAB PATH --
% -------------------------
addpath([fileparts(fileparts(mfilename('fullpath'))) '/SOURCES_MATLAB']);

%%
% ------------------------------
% -- DEFINING STABFEM OPTIONS --
% ------------------------------
% The structure of the current section copies that of SF_core_generateopts
% but does not require user-machine interactions. In case of doubt about
% expected options, the autorun exits with an status code = 1.

if(mode==0)
  SF_core_start('staticopts', @errorIfStaticRequired);
else
  SF_core_start();
end
%%

%%
disp('');disp('###############@####################################');disp('');

SF_core_getopt

disp('');disp('###############@####################################');disp('');




% ---------------------------
% -- GETTING ENV VERBOSITY --
% ---------------------------
sfarverbosity = str2num(['uint8(' getenv('SFARVERBOSITY') ')']);
if isempty(sfarverbosity)
    sfarverbosity=0;
end

%%
% ----------------------
% -- RUNNING AUTORUNS --
% ----------------------

numsuccess = 0;
numfailure = 0;
numerror = 0;
numskipped = 0;

cd(SF_core_getopt('sfroot'));

list_status = zeros(1,numel(case_list));
list_stacks = cell(1,numel(case_list));

for i=1:numel(case_list)
    SF_core_log('n','');SF_core_log('n','');
    SF_core_log('n','#############################################################');
    SF_core_log('n','#############################################################');    
    SF_core_log('n', sprintf('\n ### AUTORUN test number %i: %s\n',i,case_list{i}), true);
    SF_core_log('n','#############################################################');
    SF_core_log('n','#############################################################');    
    SF_core_log('n','');SF_core_log('n','');
    SF_core_setopt('eigensolver','SLEPC');
    SF_core_setopt('BFSolver',[]);
    SF_core_setopt('StabSolver',[]);
    t0case = tic;
    try
        cd(case_list{i});
        a = autorun(sfarverbosity);
        if a==0
            numsuccess = numsuccess + 1;
            list_status(i) = 0;
        elseif a<0
            numskipped = numskipped + 1;
            list_status(i) = -1;
        else
            numfailure = numfailure + 1;
            list_status(i) = 1;
        end
    catch err
        numerror = numerror + 1;
        list_status(i) = 2;
        if ~SF_core_getopt('isoctave')
            list_stacks{i} = getReport(err);
        else
            list_stacks{i} = 'Error with octave (cannot get report)';
        end

    end
    duration(i) = toc(t0case);
    SF_core_log('n',['Time spent in this case (seconds): ',num2str(duration(i))],true);
    cd(SF_core_getopt('sfroot'));
end

% ---------------------------
% -- SHOWING FINAL RESULTS --
% ---------------------------
SF_core_log('n', sprintf('\n== AUTORUN SUMMARY ==\n'), true);
SF_core_log('n', sprintf('NUMBER OF SUCCESSES: %i', numsuccess), true);
SF_core_log('n', sprintf('NUMBER OF SKIPPED CASES: %i', numskipped), true);
SF_core_log('n', sprintf('NUMBER OF PRECISION ISSUES: %i', numfailure), true);
SF_core_log('n', sprintf('NUMBER OF FATAL ERRORS: %i', numerror), true);
duration(numel(case_list)+1) = toc(tinit);
SF_core_log('n',['TOTAL TIME SPENT IN AUTORUN (seconds): ',num2str(duration(numel(case_list)+1))],true);

SF_core_log('n', sprintf('\n== AUTORUN DETAILS ==\n'), true);
for i=1:numel(case_list)
    switch list_status(i)
        case 0
            msg = 'SUCCESS';
        case 1
            msg = 'PRECISION';
        case 2
            msg = 'ERROR';
        case -1
            msg = 'SKIPPED';
    end
    [casepath,casedir] = fileparts(case_list{i});
    [~,casedir2] = fileparts(casepath);
    SF_core_log('n', sprintf('\tCase %s/%s :  %s \n             Time spent (seconds) : %s \n', casedir2,casedir, msg,num2str(duration(i))), true);
 %   SF_core_log('n',['     Time spent in this case (seconds): ',num2str(duration(i))],true);
    switch list_status(i)
        case 2
            SF_core_log('n', list_stacks{i}, true);

        otherwise
    end
end
% repeating SUMMARY to see it at the END of log file
SF_core_log('n', sprintf('\n== AUTORUN SUMMARY ==\n'), true);
SF_core_log('n', sprintf('NUMBER OF SUCCESSES: %i', numsuccess), true);
SF_core_log('n', sprintf('NUMBER OF SKIPPED CASES: %i', numskipped), true);
SF_core_log('n', sprintf('NUMBER OF PRECISION ISSUES: %i', numfailure), true);
SF_core_log('n', sprintf('NUMBER OF FATAL ERRORS: %i', numerror), true);
duration(numel(case_list)+1) = toc(tinit);
SF_core_log('n',['TOTAL TIME SPENT IN AUTORUN (seconds): ',num2str(duration(numel(case_list)+1))],true); 


cd AUTORUN/

sfarkeep = getenv('SFARKEEP');
if isempty(sfarkeep)
    sfarkeep=false;
    if(mode>0)
       sfarkeep=true;
    end
end
sfarkeep=logical(sfarkeep);

if numerror>0
    returnstatus = 2;
elseif numfailure>0
    returnstatus =0; %% SHOULD BE 1
else
   returnstatus = 0;
end

disp(['Return status is ',num2str(returnstatus)]);

if ~sfarkeep
  exit(returnstatus) ;
end

end

function r = errorIfStaticRequired()
if (~SF_core_isopt('sfroot') || ~SF_core_isopt('ffroot')) && ~SF_core_getopt('gitrepository')
    warning('The autorun expects all environment to be automatically defined.');
    r = false;
else
    r = true;
end
end
