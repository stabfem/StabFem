%% Flow around a wing profile : example for a NACA0012 profile
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4);
SF_DataBase('create','WORK_NACA2312');

%% Generation of a mesh

mesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',2,'P',3,'XX',12});

figure; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos); % resize aspect ratio
subplot(1,2,1); SF_Plot(mesh,'title','Mesh');
subplot(1,2,2); SF_Plot(mesh,'title','Mesh (Zoom)','xlim',[.5 1],'ylim',[-.25 .25]);

%% Generating elementary potential solutions

FlowU = SF_Launch('PotentialFlow.edp','mesh',mesh,'Options',{'U',1,'V',0,'Gamma',0},'Store','POTENTIALFLOWS');
FlowV = SF_Launch('PotentialFlow.edp','mesh',mesh,'Options',{'U',0,'V',1,'Gamma',0},'Store','POTENTIALFLOWS')
FlowG = SF_Launch('PotentialFlow.edp','mesh',mesh,'Options',{'U',0,'V',0,'Gamma',1},'Store','POTENTIALFLOWS')


%% Plotting elementary solution "U"
figure ; 
pos = get(gcf,'Position'); %pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(2,2,[1 2]); SF_Plot(FlowU,'p','xlim',[-1 1.5],'ylim',[-.5 .5],'colorrange',[-.5 .5],'colormap','redblue','title','Elementary potential Flow "U"');
hold on ; SF_Plot(FlowU,{'ux','uy'},'xlim',[-1 1.5],'ylim',[-.5 .5]);
subplot(2,2,4); SF_Plot(FlowU,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(FlowU,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);
subplot(2,2,3); SF_Plot(FlowU,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on leading edge)');
hold on ; SF_Plot(FlowU,{'ux','uy'},'xlim',[-.5 0],'ylim',[-.25 .25]);


%% Ploting elementary solution "V" 
figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(FlowV,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Elementary potential Flow "V"');
hold on ; SF_Plot(FlowV,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(FlowV,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(FlowV,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);

%% Ploting elementary solution "Gamma"
FlowK = FlowG;
figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(FlowK,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Elementary potential Flow "Gamma"');
hold on ; SF_Plot(FlowK,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(FlowK,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(FlowK,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);


%% Solution for 
alpha = 20;
Gamma = 1.3;
Flow = SF_Launch('PotentialFlow.edp','mesh',mesh,'Options',{'U',cos(alpha*pi/180),'V',sin(alpha*pi/180),'Gamma',Gamma},'Store','POTENTIALFLOWS');


%% Alternatively : Reconstructing solution
alpha = 20;
Gamma = 1.3;
Flow = SF_Add({FlowU,FlowV,FlowG},'Coefs',[cos(alpha*pi/180),sin(alpha*pi/180),Gamma]);
Flow.p = (1-(Flow.ux.^2+Flow.uy.^2))/2;
Flow.Gamma = Gamma; Flow.alpha = alpha;

%% Check kutta criterion
ute = SF_ExtractData(Flow,'ux',.75,.002);uti = SF_ExtractData(Flow,'ux',.75,-.002);
kutta = ute-uti

%%
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(Flow,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title',['alpha = ',num2str(Flow.alpha),' ; Gamma = ',num2str(Flow.Gamma)]);
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(Flow,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);


%% VISCOUS FLOW (Re = 1000 and 10 000; alpha = 0)
alpha = 0;
mesh = SF_Load('MESH',1);
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 100},'mesh',mesh,'Store','MISC');
for Re = [300 600 1000]
  FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','MISC');
  Mask = SF_Mask(FlowV,[-.3 1 -0.1 0.1 0.005]);            
  FlowV = SF_Adapt(FlowV,Mask,'hmin',0.0001,'recompute',false);
end
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','VISCOUSFLOWS') ;              
for Re = [1500 2000 3000 6000 10000]
  FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','MISC');
  Mask = SF_Mask(FlowV,[-.3 1 -0.1 0.1 0.005]);            
  FlowV = SF_Adapt(FlowV,Mask,'hmin',0.0001,'recompute',false);
end
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','VISCOUSFLOWS') ;              
           

%% Plot this solution
figure(15) ; subplot(2,2,[1 2]);
SF_Plot(FlowV,'vort','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-1 3],'ylim',[-.75 .75]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-1 3],'ylim',[-.75 .75],'CStyle','dashedneg');
subplot(2,2,3);
SF_Plot(FlowV,'vort','xlim',[-.4 -.1],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-.4 -.1],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-.4 -.1],'ylim',[-.15 .15],'CStyle','dashedneg');
subplot(2,2,4);
SF_Plot(FlowV,'vort','xlim',[.6 .9],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[.6 .9],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[.6 .9],'ylim',[-.15 .15],'CStyle','dashedneg');

%% VISCOUS FLOW (Re = 2000; alpha = 10)
alpha = 10;
mesh = SF_Load('MESH',1);
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 100},'mesh',mesh,'Store','MISC');
for Re = [300 600 1000 1500 2000]
  FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','MISC');
  Mask = SF_Mask(FlowV,[-.3 1 -0.1 0.1 0.005]);            
  FlowV = SF_Adapt(FlowV,Mask,'hmin',0.0001,'recompute',false);
end
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', Re},'Init',FlowV,'Store','VISCOUSFLOWS') ;              
           

%% Plot this solution
figure(15) ; subplot(2,2,[1 2]);
SF_Plot(FlowV,'vort','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-1 3],'ylim',[-.75 .75]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-1 3],'ylim',[-.75 .75],'CStyle','dashedneg');
subplot(2,2,3);
SF_Plot(FlowV,'vort','xlim',[-.4 -.1],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-.4 -.1],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-.4 -.1],'ylim',[-.15 .15],'CStyle','dashedneg');
subplot(2,2,4);
SF_Plot(FlowV,'vort','xlim',[.6 .9],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[.6 .9],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[.6 .9],'ylim',[-.15 .15],'CStyle','dashedneg');

%% SUMMARY

SF_Status

% [[PUBLISH]]

