%% Flow around a wing profile : example for a NACA0012 profile
%

close all;
addpath('../../SOURCES_MATLAB');
SF_RecoverDataBase('WORK_NACA2312');
SF_Start('verbosity',3,'ffdatadir','WORK_NACA2312');

%% Check what is available in the dataset
SF_Status

%% Part 1 : potential flows

%% importing and plotting the field for alpha=0, gamma=0 
mesh = SF_Load('MESH',1);
Flow = SF_Load('POTENTIALFLOWS',1)
figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(Flow,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 0� ; Gamma = 0');
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(Flow,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);

%% Plot U(y) along a vertical line
Yline = [-1:.001:1];
Uline = SF_ExtractData(Flow,'ux',0.25,Yline);
figure; plot(Uline,Yline); xlabel('U(0.25,y)');ylabel('y');


%% same thing for field for alpha=0, gamma=1 
Flow = SF_Load('POTENTIALFLOWS',4)
figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(Flow,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 0� ; Gamma = 1');
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(Flow,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);

%%
% NB : if you want to recompute a solution for a value of alpha,gamma not in the database:
%
% > Flow = SF_Launch('PotentialFlow.edp','mesh',mesh,'Options',{'alpha',alpha,'Gamma', Gamma})
% (see SCRIPT_GenerateData_Naca0012.m for more details)

%% Part 2 : viscous flows

%% Import and plot the solution for alpha = 0, Re=1000
FlowV = SF_Load('VISCOUSFLOWS',1)
figure(15) ; subplot(2,2,[1 2]);
SF_Plot(FlowV,'vort','xlim',[-.5 1.5],'ylim',[-.75 .75],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-1 3],'ylim',[-.75 .75]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-1 3],'ylim',[-.75 .75],'CStyle','dashedneg');
subplot(2,2,3);
SF_Plot(FlowV,'vort','xlim',[-.4 -.1],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-.4 -.1],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-.4 -.1],'ylim',[-.15 .15],'CStyle','dashedneg');
subplot(2,2,4);
SF_Plot(FlowV,'vort','xlim',[.6 .9],'ylim',[-.15 .15],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[.6 .9],'ylim',[-.15 .15]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[.6 .9],'ylim',[-.15 .15],'CStyle','dashedneg');


%% Plot U(y) along a vertical line
Yline = [-1:.001:1];
Uline = SF_ExtractData(FlowV,'ux',0.25,Yline);
figure; plot(Uline,Yline); xlabel('U(0.25,y)');ylabel('y');

%[[PUBLISH]]

saveas(gca,'NACA2312_Re2000_alpha10','png');
