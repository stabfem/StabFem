%%  STEADY FLOW - TUTORIAL 2 FOR MSc 1st year in Paul Sabatier University
%
%  This script has been designed to study the POTENTIAL flow solution around a cylinder 
%  


%% First : initialization of StabFem tools
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4);

%% 1. Generation of a mesh and computation of two potential flow solutions
% initialization of database
SF_DataBase('create','WORK_CYLINDER_POTENTIAL');

% mesh generation
ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 40 40],'problemtype','2D');
Mask = SF_Mask(ffmesh,[-2 2 0 2 .05]);
ffmesh = SF_Adapt(ffmesh,Mask,'Hmax',5);
ffmesh = SF_Mirror(ffmesh);

% computation of three elementary potential solutions
FlowPU = SF_Launch('PotentialFlow_2D.edp','Options',{'U',1,'V',0,'Gamma',0},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_U"
FlowPV = SF_Launch('PotentialFlow_2D.edp','Options',{'U',0,'V',1,'Gamma',0},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_V"              
FlowPG = SF_Launch('PotentialFlow_2D.edp','Options',{'U',0,'V',0,'Gamma',1},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_Gamma"             

%% 1 bis. If you have not installed FreeFem++ fails please try this             
              
% SF_RecoverDataBase('WORK_CYLINDER_POTENTIAL');
% SF_DataBase('open','WORK_CYLINDER_POTENTIAL');
% FlowPU = SF_Load('POTENTIALFLOWS',1); % solution "phi_U"
% FlowPV = SF_Load('POTENTIALFLOWS',2); % solution "phi_U"
% FlowPG = SF_Load('POTENTIALFLOWS',3); % solution "phi_Gamma"
%
              
%%  2.a Plot solution psi_U (zero circulation, external velocity in X direction)            
figure;SF_Plot(FlowPU,'p'); hold on; SF_Plot(FlowPU,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);

%% 
% Plot U(y) along a vertical line (Q4, NB en y=0 et pas 0.25)
Yline = [-3:.001:3];
Uline = SF_ExtractData(FlowPU,'ux',0.,Yline);
figure; plot(Uline,Yline); xlabel('U(0.,y)');ylabel('y');

%% 
% Plot U(x) along a horizontal line (Q5)
Xline = [-3:.001:3];
UlineX = SF_ExtractData(FlowPU,'ux',Xline,0);
figure(22); plot(Xline,UlineX,'r'); ylabel('U(x,0)');ylabel('x');

%% Comparez avec solution théorique
% le long de l'axe x
Rline = [0.5:.01:3];
Ux_x_theo = (1-0.5^2./Rline.^2);
figure(22); hold on; plot(Rline,Ux_x_theo,'+')


%% 
% Now we plot the pressure and tangential velocity at the surface

theta = linspace(-pi,pi,201);
Xsurf = .501*cos(theta);
Ysurf = .501*sin(theta);
Psurf = SF_ExtractData(FlowPU,'p',Xsurf,Ysurf);
uxsurf = SF_ExtractData(FlowPU,'ux',Xsurf,Ysurf);
uysurf = SF_ExtractData(FlowPU,'uy',Xsurf,Ysurf);
utsurf = -(uxsurf.*sin(theta)-uysurf.*cos(theta)); % attention mauvais signe
figure(16);
subplot(2,1,1);
plot(theta,Psurf);
hold on;
Psurftheo = .5*(1-4*sin(theta).^2);
plot(theta,Psurftheo,'x');legend('Freefem','theorique');
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);
utsurftheo = -2*sin(theta);
plot(theta,utsurf,'r-',theta,utsurftheo,'b+');legend('Freefem','theorique');
xlabel('\theta');ylabel('u_\theta(r=a,\theta)');

%% 
% Compute the force
Fx = trapz(theta, -0.5*Psurf.*cos(theta))
Fy = trapz(theta, -0.5*Psurf.*sin(theta))

%% 2.b Plot solution psi_V (zero circulation, external velocity in Y direction)               
figure; SF_Plot(FlowPV,'p'); hold on; SF_Plot(FlowPV,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);


%% 2.c Plot solution psi_Gamma (circulation Gamma = 1)            
figure; SF_Plot(FlowPG,'p'); hold on; SF_Plot(FlowPG,{'ux','uy'},'xlim',[-4 4],'ylim',[-4 4]);

%% 2.d Solution for Gamma = pi, U = 1
%
% Generate the composite solution by linear superposition
FlowP1 = SF_Add({FlowPU,FlowPG},'coefs',[ 1 pi]); 
FlowP1.p = .5*(1-FlowP1.ux.^2-FlowP1.uy.^2); % correct pressure
figure; SF_Plot(FlowP1,'p'); hold on; SF_Plot(FlowP1,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);

%% 
% Now we plot the pressure and tangential velocity at the surface

theta = linspace(-pi,pi,201);
Xsurf = .501*cos(theta);
Ysurf = .501*sin(theta);
Psurf = SF_ExtractData(FlowP1,'p',Xsurf,Ysurf);
uxsurf = SF_ExtractData(FlowP1,'ux',Xsurf,Ysurf);
uysurf = SF_ExtractData(FlowP1,'uy',Xsurf,Ysurf);
utsurf = -(uxsurf.*sin(theta)-uysurf.*cos(theta)); % attention mauvais signe
figure(1);
subplot(2,1,1);
plot(theta,Psurf);
hold on;
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);
plot(theta,utsurf,'r-');
xlabel('\theta');ylabel('u_\theta(r=a,\theta)');



%% Summary
% Check what is available in the dataset
SF_Status

%% Check solution U gamma

Xline = [-3:.001:3];
UlineX = SF_ExtractData(FlowPG,'uy',Xline,0);
PlineX = SF_ExtractData(FlowPG,'p',Xline,0);
figure(26); plot(Xline,UlineX,'r',Xline,PlineX,'b',Rline,-1./Rline,'r+'); 
ylabel('x');



% [[PUBLISH]]