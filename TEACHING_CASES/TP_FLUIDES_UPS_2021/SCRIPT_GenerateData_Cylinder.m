%%  Computation of base flow and DNS simulation for the 2D incompressible flow around a cylinder.
%
% This program is used to recompute all data provided in the practical work
% at UPS (M1 Mecanique)
%
% In this tutorial script:
% 1/ we bluild a well-suited mesh
% 2/ we compute base-flows (steady solutions) for sample values of Re
% 3/ we perform a DNS of the wake of a
%    cylinder for Re = 60, allowing to observe the development and saturation
%    of the well-known Von Karman vortex street.
% 4/ we compute a few solutions of the potential problem
%
% The FreeFem++ code <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp> 
% is adapted from the one used in Jallas, Marquet & Fabre (PRE, 2017).

%% Chapter 0 : initialization
clear all ; close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity', 4);
SF_DataBase('create', './WORK_CYLINDER/');


% NB change verbosity to 4 to follow the simulation while it's running 
%  or 2 to supress output from freefem++

%% Chapter 1 : mesh
% 
% We use the same procedure as in 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER/CYLINDER_LINEAR.m CYLINDER_LINEAR.m>. 
% First we build an initial mesh and progressively increase the Reynolds :
ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'problemtype','2D');
bf=SF_BaseFlow(ffmesh,'Re',1);
bf=SF_BaseFlow(bf,'Re',10);
bf=SF_BaseFlow(bf,'Re',60);
Mask = SF_Mask(bf.mesh,[-2 10 0 2 .15]);
bf = SF_Adapt(bf,Mask,'Hmax',5);
Mask = SF_Mask(bf.mesh,[-2 10 0 2 .15]);
bf = SF_Adapt(bf,Mask,'Hmax',5);
bf = SF_Mirror(bf);


%% Chapter 2 : steady flow
%
% Now computes the base flow for a few values of Re needed for the TP

bf = SF_BaseFlow(bf,'Re',60);
bf = SF_BaseFlow(bf,'Re',40);
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',1);

%% Plot the solution for Re = 40
bf = SF_BaseFlow(bf,'Re',40);

SF_Plot(bf,'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropminmax','boundary','on','bdlabels',2,'title','Steady flow for Re = 40');
hold on; SF_Plot(bf,'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);


%% Chapter 3 : DNS
%
% We first generate an initial condition for the DNS at Re = 60 using linear stability results
bf = SF_BaseFlow(bf,'Re',60);
[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'Symmetry','N','nev',1); % compute the eigenmode. 
startfield = SF_Add({bf,em(1)},'coefs',[1 0.01],'NameParams',['Re'],'Params',[bf.Re]); % creates startfield = bf+0.01*. Not sure the two last parameters are useful ??

%%
% Then we Launch a DNS
%
% We use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_DNS.m SF_DNS.m> 
% which launches the Freefem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp>

% first run over a long period without snapshots 

Niter = 8000;   % total number of time steps
iout = 8000;   % will generate snapshots each iout time steps
dt = 0.02;   % time step 
DNSOptions = {'Re',60,'Niter',Niter,'dt',dt,'iout',iout};

folder = SF_TS_Launch('TimeStepper_2D.edp','Init',startfield,'Options',DNSOptions,'wait',true); % launch DNS
[DNSstatslong,DNSfieldslong] = SF_TS_Check(folder); % imports results from DNS


%% second run over one period of oscillation to generate 10 snapshots ; 

dt = 7.4/400; % to simulate one period in 400 time steps
iout = 40; % to generate snapshots each 40 time steps
Niter=400; % number of time steps
DNSOptions = {'Re',60,'Niter',Niter,'dt',dt,'iout',iout};
startfield = DNSfieldslong(end); 
folder = SF_TS_Launch('TimeStepper_2D.edp','Init',startfield,'Options',DNSOptions,'wait',true);%,'samefolder',true);
[DNSstats,DNSfields] = SF_TS_Check(folder); % imports results



%% Chapter 3b : plotting the results and generating a movie
% 
 
% Here is how to generate a movie
h = figure;
mkdir('html');
filename = 'html/DNS_Cylinder_Re60.gif';
for i=1:length(DNSfields)
    SF_Plot(DNSfields(i),'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-3 3],'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2);
    hold on; SF_Plot(DNSfields(i),'p','contour','only','CStyle','dashedneg','xlim',[-2 10],'ylim',[-3 3]);
    hold off;
    pause(0.01);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% 
% We now plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstatslong.t,DNSstatslong.Fy);title('Lift force as function of time');
hold on; plot(DNSstats.t,DNSstats.Fy);
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstatslong.t,DNSstatslong.Fx);title('Drag force as function of time');
hold on; plot(DNSstats.t,DNSstats.Fx);
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');

%% Chapter 4 : compute a few potential solutions

ffmesh = SF_Load('MESH','last'); 
FlowPU = SF_Launch('PotentialFlow_2D.edp','Options',{'U',1,'V',0,'Gamma',0},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_U"
FlowPV = SF_Launch('PotentialFlow_2D.edp','Options',{'U',0,'V',1,'Gamma',0},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_V"              
FlowPG = SF_Launch('PotentialFlow_2D.edp','Options',{'U',0,'V',0,'Gamma',1},'mesh',ffmesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS'); % solution "phi_Gamma"     
FlowP1 = SF_Add({FlowPU,FlowPG},'coefs',[ 1 pi]); 
FlowP1.p = .5*(1-FlowP3.ux.^2-FlowP3.uy.^2);
                            
figure; SF_Plot(FlowP1,'p'); hold on; SF_Plot(FlowP1,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2])
saveas(gca,'PotentialCylinder','png')

%% Summary
SF_Status


% [[PUBLISH]]  
% [[WORK]] 