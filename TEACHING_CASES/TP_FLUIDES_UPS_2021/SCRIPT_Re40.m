%%  STEADY FLOW - TUTORIAL 2 FOR MSc 1st year in Paul Sabatier University
%
%  This script has been designed to study the flow around a cylinder 
%  at low Reynolds number [1-40]. 
%  We study how this flow is characterised by analysing main properties
%  of flow quantities such as flow field, pressure, vorticity and
%  shear stress.
%  
%  The script performs the following calculations :
% 
%  # Generation of a mesh
%  # Computation of the stationary solution at Re = [1-40]
%  # Display flow fields
%

%%
%
% First : initialization of StabFem tools
%

close all;
addpath('../../SOURCES_MATLAB');
SF_RecoverDataBase('WORK_CYLINDER');
SF_Start('verbosity', 4);
SF_DataBase('open','WORK_CYLINDER');

%% Check what is available in the dataset
SF_Status

% Here we see that Re = 40 is the field number 8
% Hence we load it this way :

 bf = SF_Load('BASEFLOWS',8);

 %% Plot norm of velocity + streamfunction + quiver plot (fig 201)
bf.Normu = sqrt(bf.ux.^2+bf.uy.^2);
figure(201);
SF_Plot(bf,'Normu','xlim',[-1 5],'ylim',[-3 3],'title','||u||');
hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',[-1 -.5 -.2 -.1 -0.05:0.01:0.05 .1 .2 .5 1],'CColor','y','xlim',[-3 5],'ylim',[-3 3]);
SF_Plot(bf,{'ux','uy'},'xlim',[-1 5],'ylim',[-3 3],'FColor','w');

 

%% plot Ux, Uy, omega, P (fig 202)
%
figure(202);
subplot(2,2,1); SF_Plot(bf,'ux','xlim',[-3 5],'ylim',[-3 3],'title','u_x','contour','on','clevels',[-0.05 0 0.5 1,1.1]);
subplot(2,2,2); SF_Plot(bf,'uy','xlim',[-3 5],'ylim',[-3 3],'title','u_y','contour','on');
subplot(2,2,3); SF_Plot(bf,'vort','Contour','on','CLevels',[-1,-0.5,-0.02,0.02,0.5,1],'xlim',[-3 5],'ylim',[-3 3],'title','\omega','colorrange',[-1,1]);
subplot(2,2,4); SF_Plot(bf,'p','Contour','on','xlim',[-3 5],'ylim',[-3 3],'title','p');


%%
% Plot Ux(x,0) along a horizontal line (fig 203)
X = 0.5:0.01:5.0;
Y = 0.0;
V= SF_ExtractData (bf ,'uy',X,Y);
U= SF_ExtractData (bf ,'ux',X,Y);
figure(203);
plot (X,V,'-k'); hold on; plot (X,U,'-r'); 
ylabel('U(x,0)');xlabel('x');


%% 
% Plot Ux(0,y) along a vertical line (fig 204)
Yline = [-3:.001:3];
Uline = SF_ExtractData(bf,'ux',0.,Yline);
figure(204); plot(Uline,Yline); 
xlabel('U(0.,y)');ylabel('y');



%%
% Extraction des donn�es pari�tales

theta = linspace(0,2*pi,200);
Ycircle = 0.5001*sin(theta); Xcircle = 0.5001*cos(theta); 

tauxxwall = SF_ExtractData(bf,'tauxx',Xcircle,Ycircle);
tauxywall = SF_ExtractData(bf,'tauxy',Xcircle,Ycircle);
tauyywall = SF_ExtractData(bf,'tauyy',Xcircle,Ycircle);
S = sin(theta); C = cos(theta);
tauWallN =  C.*(tauxxwall.*C+tauxywall.*S) + S.*(tauxywall.*C+tauyywall.*S);
tauWallT = - S.*(tauxxwall.*C+tauxywall.*S) + C.*(tauxywall.*C+tauyywall.*S);
pWall = SF_ExtractData(bf,'p',Xcircle,Ycircle); 

%%
% Trace de la contrainte parietale (fig 205)

figure(205);
plot(theta,tauWallT,'b^');
hold on;
plot(theta,tauWallN,'r^');
xlabel('\theta'); ylabel('\tau_{w}'); title('shear stress $\tau(r=R,\theta)$ along the surface', 'Interpreter','latex');
legend({'$\tau_t$','$\tau_n$'},'Interpreter','latex');


%% Calcul de la tra�n�e

mu = 1/bf.Re; % viscosite adimensionnelle
Cx = 2*trapz(theta, 0.5*mu*((tauWallN.*C - tauWallT.*S) - pWall.*C))

%%[[PUBLISH]]

