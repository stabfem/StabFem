%%  DNS FLOW - TUTORIAL 3 FOR MSc 1st year in Paul Sabatier University
%
% In this tutorial script we show how to postprocess DNS results for the wake of a
% cylinder for Re = 60, allowing to observe the development and saturation
% of the well-known Von Karman vortex street.
% 
% The data is in the directory ./WORK/ and can be re-created by running the
% script SF_GENERATE_DATA.m in the same directory

%%
%
% First : initialization of StabFem tools
%
clear all
close all;
addpath('../../SOURCES_MATLAB');
SF_RecoverDataBase('WORK_CYLINDER');
SF_Start('verbosity', 4);
SF_DataBase('open','WORK_CYLINDER');


%% Chapter 1 : check what is available in the database
sfs = SF_Status

% We can see that there are 11 data files in the "./WORK_CYLINDER/TimeStepping/RUN000002/" directory
% corresponding to 11 instants in an oscillation cycle.


%% Chapter 2 : import time-series and snapshots from database and plot a few things.
[DNSstatslong,DNSfieldslong] = SF_TS_Check(1); % imports results
[DNSstats,DNSfields] = SF_TS_Check(2); % imports results


%%

SF_Plot(DNSfields(1),'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropminmax',...
        'title',['t = ',num2str(DNSfields(1).t)],'boundary','on','bdlabels',2);
hold on; SF_Plot(DNSfields(1),'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);


%% Chapter 3 : generating a movie
% 





% Here is how to generate a movie
h = figure;
filename = 'html/DNS_Cylinder_Re60.gif';
for i=1:length(DNSfields)
%    DNSfields(i) = SF_Load('DNSFIELDS',i);
    SF_Plot(DNSfields(i),'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-5 5],...
        'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2);
    hold on; SF_Plot(DNSfields(i),'p','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);
    hold off;
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% Chapter 4
% We now plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstatslong.t,DNSstatslong.Fy,'b',DNSstats.t,DNSstats.Fy,'r');title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstatslong.t,DNSstatslong.Fx,'b',DNSstats.t,DNSstats.Fx,'r');title('Drag force as function of time');
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');


%% [[PUBLISH]]
