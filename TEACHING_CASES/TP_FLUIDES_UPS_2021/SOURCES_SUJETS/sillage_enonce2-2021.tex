\documentclass[french, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{xcolor}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}
\usepackage{epsfig, latexsym}
\usepackage{color}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfig}
\usepackage{mathtools}
\usepackage{lineno}
\usepackage{ulem}
\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{definition}[thm]{Definition}
\newtheorem{rmk}{Remarque}


%References
\newcommand{\Eref}[1]{\mbox{ Eq. }\ref{#1}}
\newcommand{\Fref}[1]{\mbox{ Figure }\ref{#1}}
\newcommand{\Tref}[1]{\mbox{ Table }\ref{#1}}
\newcommand{\Sref}[1]{\mbox{ Section }\ref{#1}}

% Commands for equations
 \newcommand{\sgn}{\operatorname{sgn}} 
\newcommand{\Dpart}[1]{ \frac{\partial{#1}}{\partial t}}
\newcommand{\ugradf}[2]{ #1 \cdot \gradient #2 }
\newcommand{\fdivu}[2]{ #1 \nabla \cdot #2 }
\newcommand{\contraction}[2]{ #1 : #2 }
\newcommand{\laplace}[1]{ \Delta #1 }
\newcommand{\inner}[2]{ \langle #1 , #2 \rangle }
\newcommand{\innerB}[2]{ \langle #1 , #2 \rangle_\vec{B} }
\newcommand{\adj}[1]{ #1^{\dagger}}

\newcommand{\be}[1]{ \begin{equation} \label{#1}}
\newcommand{\ee}{\end{equation}}


\newcommand{\bes}[1]{ \begin{equation} \label{#1}\begin{array}{rl}}
\newcommand{\ees}{\end{array}\end{equation}}

\let\vec\mathbf
\newcommand{\dx}[1]{\frac{\partial #1}{\partial x}}
\newcommand{\dy}[1]{\frac{\partial #1}{\partial y}}
\newcommand{\dr}[1]{\frac{\partial #1}{\partial r}}
\newcommand{\dq}[1]{\frac{\partial #1}{\partial \theta}}
\newcommand{\dt}[1]{\frac{\partial #1}{\partial t}}

\newcommand{\dxx}[1]{\frac{\partial^2 #1}{\partial x^2}}
\newcommand{\dyy}[1]{\frac{\partial^2 #1}{\partial y^2}}
\newcommand{\dxy}[1]{\frac{\partial^2 #1}{\partial x \partial y}}

\newcommand{\dyyy}[1]{\frac{\partial^3 #1}{\partial y^3}}
\newcommand{\deta}[1]{\frac{\partial #1}{\partial \eta}}
\newcommand{\gradient}{\vec{\mbox{gra}}\mbox{d}}
\newcommand{\divergence}{\vec{\mbox{di}}\mbox{v}}
\newcommand{\rotationnel}{\vec{\mbox{ro}}\mbox{t}}
\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}
\newcommand{\power}[1]{^{\mbox{\tiny #1}}}

\newcommand{\TPtitle}[2]{\begin{center}\fbox{\begin{minipage}[r]{#2}
\large \bf \center #1 \end{minipage}}\end{center}\medskip}

\newcounter{MyEnumCounter}
\newcounter{MySaveCounter}
\newenvironment{MyEnum}{%
  \begin{list}{\arabic{MyEnumCounter}.}{\usecounter{MyEnumCounter}%
  \setcounter{MyEnumCounter}{\value{MySaveCounter}}}
  }{%
  \setcounter{MySaveCounter}{\value{MyEnumCounter}}\end{list}%
}
\newcommand{\MyEnumReset}{\setcounter{MySaveCounter}{0}}

\setlength{\textwidth}{16cm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textheight}{24cm}
\setlength{\topmargin}{-15mm}

\setlength{\columnsep}{1cm}
\setlength{\fboxsep}{5mm}

\setlength{\unitlength}{1mm}

\newcounter{annee}
\setcounter{annee}{\number\year}
\newcounter{anneeplus}
\setcounter{anneeplus}{\number\year}
\addtocounter{anneeplus}{1}
%\newcommand{\annee}{\the\year}
\newcommand{\anneeuniversitaire}{\theannee--\theanneeplus}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\thispagestyle{empty}

\begin{titlepage}

\begin{picture}(160, 235)
  \put(-6, 240){
    \begin{minipage}{16cm} %
      \textsc{Universit\'e Paul Sabatier \hfill Ann\'ee universitaire \anneeuniversitaire}
      \\
      \textsc{M1 M\'ecanique--Energ\'etique \hfill M\'ecanique des Fluides}
    \end{minipage}
  }
  \put(10, 200){\textbf{\Large Projet :}}
  \put(10, 190){\textsc{\Large Ecoulement autour d'un obstacle}}
  \put(10, 185){\textsl{\large Analyse de simulations num\'eriques}}
  \put(10, 50){\includegraphics[width=13cm]{Figures/CylinderFlowUMag.pdf}}
  \put(25,  45){\textsl{-- Écoulement stationnaire autour du cylindre à $Re=40$ --}}
  \put( 5,   0){\line(0, 1){233}}
  \put(10,  20){\large Equipe p\'edagogique~:}
  \textcolor{black}{\put(10,  15){\large D. Fabre \& F. Moulin \& T. Schuller \& J. Sierra}}
  \put(10, 150){\fbox{\textbf{\Large Séance 2-3 : \large Écoulement stationnaire autour du cylindre $Re =40$}}}
\end{picture}

\end{titlepage}

%------------------------------
\section{Introduction}
Au cours des deux prochaines séances, nous nous intéresserons à l'écoulement autour d'un cylindre pour un nombre de Reynolds qui correspond à une dynamique stationnaire. Commençons par rappeler la définition du nombre de Reynolds pour le cylindre (avec $U_\infty$ la vitesse du fluide loin du cylindre, $\nu$ la viscosité cinématique du fluide supposée constante, et $D$ le diamètre du cylindre):
\be{Reynols}
Re = \frac{U_\infty D}{\nu}
\ee

Nous nous int\'eressons dans ces deux s\'eances \`a l'\'ecoulement autour
du cylindre pour un nombre de Reynolds relativement faible mais
pour lequel les effets d'inertie se font sentir \`a travers
l'apparition d'un ph\'enom\`ene particulier, le \textsl{d\'ecollement}.
Cette manifestation caract\'eristique de l'influence grandissante
de l'inertie par rapport aux effets de diffusion visqueuse
est \'etudi\'ee en d\'etail lors de la premi\`ere s\'eance,
\`a travers l'analyse d'une simulation num\'erique de l'\'ecoulement
pour un nombre de Reynolds de 40.
Une \'etude param\'etrique est ensuite propos\'ee 
pour quantifier l'influence du nombre de Reynolds sur la structure de
l'\'ecoulement et plus particuli\`erement sur la zone de recirculation.\\
\\
Au cours de ces s\'eances, nous allons traiter des simulations avec \texttt{StabFem}, et nous post-traiterons les r\'esultats des simulations avec le logiciel \texttt{MATLAB} ou Octave. 

Le TP est basé sur le script exemple  \texttt{SCRIPT\_Re40.m} qui est également accessible sur le site web du projet StabFem à l'adresse suivante :

\url{https://stabfem.gitlab.io/StabFem/TEACHING_CASES/TP_FLUIDES_UPS_2021/SCRIPT_Re40.html}

Il est recommandé d'exécuter ce script section par section pour répondre aux questions dans l'ordre de cet énoncé. 
Vous serez ensuite amené à le modifier ou le compléter. 



\section{Analyse de l'\'ecoulement \`a $Re=40$}

\begin{MyEnum}
\item
Se positionner dans le r\'epertoire \texttt{TEACHING\_CASES/TP\_FLUIDES\_UPS\_2021}; 

Lancez matlab (sur les machines du réseau en tapant matlab \& ) ou Octave.

Ouvrez le script \texttt{SCRIPT\_Re40.m}.

\item Exécutez la première section du script (commande \texttt{SF\_Status}) pour observer le contenu du jeu de données fourni.

\item Exécutez la seconde section pour importer avec la commande  \texttt{SF\_Load} la solution d'écoulement visqueux stationnaire pour $Re=40$. 
Observez le contenu de l'objet \texttt{bf} résultant.




%\newpage
%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Phénoménologie -- Visualisation de l'écoulement}
  \textbf{Lignes de courant~:}
\item
  A l'aide de la fonction \texttt{SF\_Plot}, visualisez les lignes de courant \`a l'int\'erieur du domaine en utilisant la variable \texttt{``psi''} de la structure \texttt{bf}.
  {D\'ecrire l'\'ecoulement derri\`ere le cylindre.
    Que s'est-il pass\'e \`a cet endroit ? 
    Quelle est la symétrie de l'écoulement?
    Quel m\'ecanisme physique est \`a l'origine de cette nouvelle
    structure de l'\'ecoulement ?}
    
\begin{lstlisting}[language=Matlab]
fig=figure();
SF_Plot(bf,'ux','xlim',[-3 5],'ylim',[-3 3],'title','u_x'); hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',...
[-0.05:0.01:0.05],'CColor','w','xlim',[-3 5],'ylim',[-3 3]);
\end{lstlisting}

\item[]
  \textbf{Champ de vitesse :}
\item
  On dispose des composantes de vitesse suivant $x$ et $y$ (variables appel\'ees \texttt{``ux''} et \texttt{``uy''} respectivement dans la structure \texttt{bf}).
  Calculer la norme du champ de vitesse et la visualiser \`a l'aide de la fonction \texttt{SF\_Plot}.  {Interpr\'eter ces r\'esultats (on s'int\'eressera en particulier aux
    zones de faible vitesse)~: quelle est l'intensit\'e de la vitesse dans la zone de recirculation ?
    On justifiera le terme d' ``eau morte'' parfois utilis\'e pour qualifier la r\'egion juste \`a
    l'arri\`ere d'un obstacle.}
\begin{lstlisting}[language=Matlab]
bf.Normu = sqrt(bf.ux.^2+bf.uy.^2);
\end{lstlisting}


\item
  Visualiser le champ de vitesse horizontale \texttt{``ux''}.
  {Que remarque-t'on dans le sillage par rapport le maximum de la vitesse horizontale ? Interpr\'eter.}
\item
  Visualiser le champ de vitesse verticale \texttt{``uy''}.
  {Interpr\'eter.}
  
\begin{lstlisting}[language=Matlab]
fig=figure();
subplot(2,2,1); SF_Plot(bf,'ux','xlim',[-3 5],...
'ylim',[-3 3],'title','u_x'); hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',...
[-0.05:0.01:0.05],'CColor','w','xlim',[-3 5],'ylim',[-3 3]);
subplot(2,2,2); SF_Plot(bf,'uy','xlim',[-3 5],'ylim',...
[-3 3],'title','u_y'); hold on;
SF_Plot(bf,'psi','Contour','only','CLevels',[-0.05:0.01:0.05],...
'CColor','k','xlim',[-3 5],'ylim',[-3 3]);
\end{lstlisting}

  
\item
  Visualiser le champ de vitesse vectoriel en utilisant la fonction \texttt{SF\_Plot}. Vous trouverez un exemple dans le script fourni.
  {Justifier a posteriori le terme de ``zone de recirculation'' utilis\'e
    dans les questions pr\'ec\'edentes.}
\item
  Pour mesurer la longueur de la zone de recirculation, 
  on peut tracer le profil de vitesse
  horizontale $u(x)$ le long de l'axe horizontal $y=0$. Pour ce faire on utilisera le champ \texttt{``ux''} 
  contenu dans une structure de champ de base (\texttt{bf}) et on tracera son profile en fonction de \texttt{``x''} en utilisant la fonction \texttt{SF\_ExtractData}. Affiche alors \`a l'\'ecran le profil de la vitesse horizontale le
  long de l'axe horizontal passant par le centre du cylindre.
  On pourra zoomer sur la zone de
  recirculation en
  sp\'ecifiant des nouveaux {minimum} et {maximum}. 
 {Comment peut-on d\'efinir objectivement la longueur de recirculation ? En
    notant que le cylindre a un rayon $R$ de 0.5, estimer sur la courbe la
    longueur $L_r$ de la zone de recirculation.
  }
  \begin{lstlisting}[language=Matlab]
R = 0.5;
X = R:0.01:5.0;
Y = 0.0;
V=SF_ExtractData(bf,'uy',X,Y);
U=SF_ExtractData(bf,'ux',X,Y);
fig=figure();
plot(X,V,'-k'); hold on; plot(X,U,'-r'); 
\end{lstlisting}

\item 
Comparez qualitativement l'écoulement à $Re=40$ avec l'écoulement potentiel vu a la séance précédente. Quelle symétrie est perdue ? Discutez sur la condition d'adhérence et les points d'arrêt. 

 
 \item Superposez les profil de vitesse axiale le long d'une droite verticale, correspondant à l'écoulement potentiel et l'écoulement visqueux. Que constate-t-on ?
 
  \item Même question pour les profils de vitesse axiale le long d'une droite horizontale. 


 
 
  \textbf{Champ de vorticit\'e :}
\item

  {En imaginant l'\'evolution d'un \'el\'ement infinit\'esimal
    de fluide qui arrive sur le cylindre, montrer comment la condition
    d'adh\'erence \`a la paroi met cet \'el\'ement en rotation,
    en imposant un sens de rotation qui permet de justifier le
    signe de la vorticit\'e.
    Quelle est la sym\'etrie de ce champ ?
    Rep\'erer la position des extrema et des points de vorticit\'e nulle
    sur la paroi.}
  \\
  On peut s'int\'eresser \`a la vorticit\'e dans la zone de
  recirculation en focalisant sur les r\'egions de faible vorticit\'e.
 {Rep\'erer les extrema locaux de vorticit\'e \`a la paroi au niveau de la
    zone de recirculation et les corr\'eler avec le sens de
    l'\'ecoulement dans cette zone pour justifier le signe de la vorticit\'e.}
  {Quel est le niveau de vorticit\'e \`a l'int\'erieur de la zone de recirculation ?
    Les recirculations correspondent-elles \`a des tourbillons au sens strict ?}
  {Decrire le champ global de
    vorticit\'e dans le voisinage du cylindre en pr\'ecisant le signe
    de la vorticit\'e.}
\item
  Tracer le profil de la vorticit\'e dite \textsl{pari\'etale}, comme dans le TP sur l'écoulement a très bas nombre de Reynolds.
   S'affiche alors \`a l'\'ecran le profil de la vorticit\'e le
  long de la paroi du cylindre.
  {V\'erifier la position des extrema du champ de
    vorticit\'e, en pr\'ecisant leur signe en fonction de la partie du cylindre concern\'ee, 
    ainsi que l'endroit sur le cylindre o\`u la vorticit\'e passe
    par z\'ero.}
  On pourra zoomer sur le point o\`u la vorticit\'e
  change de signe. {Donner un crit\`ere objectif sur la vorticit\'e pour
    d\'eterminer la position du point de d\'ecollement.
    Mesurer alors l'abscisse $X_d$ de ce point puis l'angle de
    d\'ecollement $\alpha_d = \pi - \arccos(X_d/R)$ corres\-pondant
    (cf fig.~\ref{fig:cylindre}).
  }
\begin{figure}[htb]
  \begin{center}
    \begin{picture}(100,50)
      \put(0, 0){\includegraphics[width=10cm]{Figures/cylindre.png}}
      \put(102,20){$x$}
      \put(42,42){$y$}
      \put(67,27){$\theta$}
      \put(70,40){$\vec{e}_r$}
      \put(54,40){$\vec{e}_\theta$}
      \put(56,33){$r$}
      \put(61,7){$R$}
      \put(43,15){$O$}
      \put(-2, 33){$U_\infty, \,P_\infty$}
      \put(42,22){$\alpha$}
    \end{picture}
  \end{center}
  \caption{Ecoulement autour du cylindre~: g\'eom\'etrie.}
  \label{fig:cylindre}
\end{figure}
%\pagebreak
\item[]
  \textbf{Contraintes pari\'etales :}
\item  Rappeler l'expression de la contrainte de cisaillement
    dite \textsl{pariétale} autour du cylindre.
    Montrer le lien avec la vorticité (indice : se placer directement en coordonnées polaires).
    \begin{rmk}
    La vorticité à la paroi et la contrainte pariétale ont une relation locale de proportionnalité. On peut le démontrer aisément dans le cas cartésien sur une paroi plane présente en $y=0$ : soit le champ de vitesse 2D $\vec{u}=(u,v)$, la vorticité s'exprime simplement $\omega_z = {\color{red} \partial_x u -\partial_y v } $. En utilisant la condition d'adhérence à la paroi, on a directement $\partial_x u=0$ à la paroi. D'autre part le tenseur visqueux s'exprime 
    \be{TauCon}
    \tau(\vec{u}) = \begin{pmatrix}
        \tau_{xx} & \tau_{xy} \\
        \tau_{yx} & \tau_{yy}
    \end{pmatrix} = \mu \begin{pmatrix}
        2 \partial_x u_x &  \partial_y u + \partial_x v \\
        \partial_y u + \partial_x v  & 2 \partial_y v
    \end{pmatrix}
    \ee
    Ainsi la contrainte pariétale s'écrit directement $(1, 0) \cdot \tau(\vec{u}) \cdot (0, 1)^T = \tau_{xy}(y=0) := \tau_p$. En utilisant à nouveau la condition d'adhérence, on a alors que $\tau_p = \mu \partial_y u$. On a donc une relation de proportionnalité entre vorticité à la paroi et contrainte pariétale (valable uniquement à la paroi en $y=0$ !).
    \item Rappel, dans des coordonnées cylindriques on a: 
    \be{GradU}
    \gradient \vec{u} = \begin{pmatrix}
    \partial_r u_r & \frac{1}{r} \big( \partial_\theta u_r -  u_\theta \big) \\
    \partial_r u_\theta & \frac{1}{r} \big( \partial_\theta u_\theta +  u_r \big)
    \end{pmatrix}
    \ee
    \end{rmk}
\item
  Tracer le profil de cette contrainte le long de la paroi.
   S'affiche alors \`a l'\'ecran le profil de la contrainte de cisaillement pari\'etale
  le long de la paroi du cylindre.
  {Noter la position des extrema de cette contrainte, 
    en pr\'ecisant leur signe en fonction de la partie du cylindre concern\'ee, 
    ainsi que l'endroit sur le cylindre o\`u cette contrainte passe
    par z\'ero.
    A quoi correspondent les points de d\'ecollement mesur\'es
    pr\'ec\'edemment ?
    En d\'eduire un crit\`ere objectif de d\'ecollement bas\'e sur le
    cisaillement \`a la paroi, en le justifiant qualitativement.
    D\'eterminer par cette m\'ethode la position $X_d$ et 
    l'angle de d\'ecollement $\alpha_d$ correspondant et
    comparer cette valeur avec celle obtenue par le crit\`ere en vorticit\'e.
  }   
  
  \begin{lstlisting}[language=Matlab]
theta = linspace(0,2*pi,200);
Ycircle = 0.5001*sin(theta); Xcircle = 0.5001*cos(theta); 
tauxxwall = SF_ExtractData(bf,'tauxx',Xcircle,Ycircle);
tauxywall = SF_ExtractData(bf,'tauxy',Xcircle,Ycircle);
tauyywall = SF_ExtractData(bf,'tauyy',Xcircle,Ycircle);
S = sin(theta); C = cos(theta);
tauWallN =  C.*(tauxxwall.*C+tauxywall.*S) + S.*(tauxywall.*C+tauyywall.*S);
tauWallT = - S.*(tauxxwall.*C+tauxywall.*S) + C.*(tauxywall.*C+tauyywall.*S);
pWall = SF_ExtractData(bf,'p',Xcircle,Ycircle); 
  \end{lstlisting}
   \item[]
  \textbf{Champ de pression :}
\item
  Visualiser les isobares (contours du champ de pression) en utilisant les fonctions \texttt{SF\_Plot}.
  On utilisera le champ \texttt{``p''}.
  On rappelle que la pression de r\'ef\'erence \`a l'infini a \'et\'e
  fix\'ee \`a z\'ero sans perte de g\'en\'eralit\'e.
  {Quelle est la sym\'etrie du champ de pression ?
    O\`u se trouvent les extrema de ce champ ? Interpr\'eter.}
\item
  {Tracer le profil de pression le long de la paroi du cylindre. Crée une figure de la pression pariétale en fonction de l'angle $\theta$. 
  V\'erifier la position des extrema du champ de
    pression, ainsi que l'endroit sur le cylindre o\`u la pression passe
    par z\'ero. 
    En d\'eduire le gradient de pression le long de la paroi du
    cylindre~: le tracer sch\'ematiquement en pr\'ecisant son signe.
    Quel est le signe du gradient de pression au point de
    d\'ecollement ? Interpr\'eter.} \\
    
    \item[]
\textbf{Coefficient de train\'ee :}
\item
  Donnez une expression de la force de traînée exercée sur le cylindre, sous forme d'une intégrale de la pression pariétale et de la contrainte pariétale. Calculez à l'aide de Matlab cette intégrale, en utilisant les tableaux \texttt{pWall,tauWallN,tauWallT} créés lors des questions précédentes.
   
  
%  et de la contrainte visqueuse pariétale $\bf{\tau} 
%  sous forme d'une intégrale le long de la paroi 

%  Calculez à l'aide de \texttt{SF\_ExtractData} la traînée à partir du tenseur de cisaillement et pression à la paroi. Le calcul devra être fait pour \texttt{bf} à un nombre de Reynolds, par exemple $Re=40$. \\

\item Comparez la valeur obtenue dans les expériences (voir figure 8 de l'énoncé 1). La valeur est-elle correcte ?

\label{item:last_question}
\end{MyEnum}


\end{document}

