%% Acoustic field in a pipe with harmonic forcing at the bottom
%
% Ce script correspond au cas de la chambre d'expansion (exercice 2.3.2)
% 
% Le programme Mesh_4.edp g�n�re un maillage correspondant � une chambre
% d'expansion constitu� de 3 sections cylindriques avec pour dimensions
% R1,R2,R3 et longueurs L1,L2,L3.
%
% Le programme Mesh_4.edp permet de g�n�rer un maillage en sp�cifiant les dimensions R2 et L2.
%
% Les autres dimensions sont fixes et valent  R1 = 1,  R3 = 1,  L1 = 10, L3 = 10.
% 
% Dans le programme les quantit�s sont adimensionalis�es en posant c=1 et rho = 1.
%


%% Chapter 0 : initialisation
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB/']);
SF_Start('verbosity',4);
SF_core_arborescence('cleanall');

%% Chapter 1 : building an adapted mesh
R2 = 4; L2 =10;
ffmeshInit = SF_Mesh('Mesh_4.edp','Params',[R2,L2],'problemtype','acousticaxi');
Forced = SF_LinearForced(ffmeshInit,'omega',2);
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage


%% 
%plot the mesh :

 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 


%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 0.6;
Forced = SF_LinearForced(ffmesh,'omega',omega)


figure();
SF_Plot(Forced,'p.im','boundary','on','colormap','redblue','cbtitle','Re(p'')');
hold on;
SF_Plot(Forced,'p','boundary','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'')/Re(p'')');


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'AcousticTube.gif';
SF_Plot(Forced,'p','boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',1);
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'p','boundary','on','contour','on','clevels',[-2 :.5 :2],...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',Amp); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<AcousticTube.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = [-21 :.1 :0];
Uyaxis = SF_ExtractData(Forced,'uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'p',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''_z)','Im(u''_z)','Re(p'')','Im(p'')');
pause(0.1);

%% Chapter 3 : loop over k to compute the impedance $Z(k)$ (comparing SOMMERFELD, PML and CM)

omegarange = [0.01:.01:1];

IMP = SF_LinearForced(ffmesh,omegarange)


%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

figure;
semilogy(IMP.omega,abs(IMP.Z),'r');
title(['Impedance $|Z|$ '],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


%%
% plot Reflection coefficient 


figure;
plot(IMP.omega,IMP.R,'b--',IMP.omega,1-IMP.R,'r--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R,T$','Interpreter','latex','FontSize', 30);
title(['R (blue) and T (red) coefficients'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

figure;
semilogy(IMP.omega,IMP.R,'b--',IMP.omega,1-IMP.R,'r--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R,T$','Interpreter','latex','FontSize', 30);
title(['R (blue) and T (red) coefficients'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

