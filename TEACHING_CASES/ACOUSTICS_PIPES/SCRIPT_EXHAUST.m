%% Script FreeFem/StabFem pour un modele de pot d'echappement
%
% Ce script correspond au cas de l'exercice 2.6
% 
% Le programme Mesh_Exhaust.edp génère un maillage 
%
% Dans le programme les quantités sont adimensionalisées en posant c=1 et rho = 1.
%

%% Chapter 0 : initialisation
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',4);
close all;


%% Chapter 1 : building an adapted mesh
ffmeshInit = SF_Mesh('Mesh_Exhaust.edp');
Forced = SF_Launch('LinearForced_Acoustics.edp','mesh',ffmeshInit,'Options',{'omega',2});
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage


%% 
%plot the mesh :

 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 


%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 1;
Forced = SF_Launch('LinearForced_Acoustics.edp','mesh',ffmeshInit,'Options',{'omega',omega});


figure();
SF_Plot(Forced,'P.im','boundary','on','colormap','redblue','cbtitle','Re(p'')');
hold on;
SF_Plot(Forced,'P','boundary','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'')/Re(p'')');


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'AcousticTube.gif';
SF_Plot(Forced,'P','boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',1);
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'P','boundary','on','contour','on','clevels',-2 :.5 :2,...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',Amp); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<AcousticTube.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = -21 :.1 :0;
Uyaxis = SF_ExtractData(Forced,'Uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'P',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''_z)','Im(u''_z)','Re(p'')','Im(p'')');
pause(0.1);

%% Chapter 3 : loop over k to compute the impedance $Z(k)$ (comparing SOMMERFELD, PML and CM)

IMP = SF_Launch('LinearForcedLoop_Acoustics.edp','mesh',ffmesh,'Options',{'omegamin',0,'omegamax',2,'Nomega',201});


%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title('Impedance $Z_r$ and $Z_i$','Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title('Impedance $Z_r$ and $Z_i$','Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

figure;
semilogy(IMP.omega,abs(IMP.Z),'r');
title('Impedance $|Z|$ ','Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


%%
% plot Reflection coefficient 


figure;
plot(IMP.omega,IMP.R,'b--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R$','Interpreter','latex','FontSize', 30);
title('Reflection coefficient','Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

figure;
semilogy(IMP.omega,IMP.R,'b--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R$','Interpreter','latex','FontSize', 30);
title('Reflection coefficient','Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

