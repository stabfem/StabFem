%% Acoustic field in a pipe with harmonic forcing at the bottom
%
%  This scripts demonstrates the efficiency of StabFem for a linear acoustics problem
%
%   Problem : find the velocity potential $\phi$ such as :
%
%  * $\Delta \phi + k^2 \phi = 0$
%  * $u_z = \partial_z \phi = 1$ along $\Gamma_{in}$
%  * Sommerfeld radiation condition on $\Gamma_{out}$ (PML is also available)
%  ( $k = \omega / c_0$ is the acoustic wavenuber) 
%
% 
%  Variational formulation :
%
%  $$ \int \int_\Omega \left( \nabla \phi \cdot \nabla \phi^* + k^2 \phi \phi^*\right) dV 
%  + \int_{\Gamma_{in}} \phi^* dS
%  + \int_{\Gamma_{out}} (i k +1/R) \phi \phi^* dV
%  = 0 $$   
%
% The problem is solved in the program 'LinearForced_Acoustics.edp'
% which is called using the generic StabFem driver SF_Launch.


%% Chapter 0 : initialisation
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',4);
close all;

%% Physical parameters
c = 1;      % speed of sound
rho=1;      % density
a=1;        % radius of pipe
S = pi*a^2; % section of the pipe
L =10;      % length of pipe

%% Chapter 1 : building an adapted mesh
ffmeshInit = SF_Mesh('Mesh_FlangedPipe.edp','Options',{'L',L,'a',a});
Forced = SF_Launch('LinearForced_Acoustics.edp','mesh',ffmeshInit,'Options',{'omega',2,'rho',rho,'c',c});
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage

%% 
%plot the mesh :
 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 
%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 1;
Forced = SF_Launch('LinearForced_Acoustics.edp','mesh',ffmesh,'Options',{'omega',omega,'rho',rho,'c',c});

% Generate a figure and save to a file
figure();
SF_Plot(Forced,'P','boundary','on','contour','on','colormap','redblue','cbtitle','Re(p'')',...
                'YLim',[-10,20]);
hold on;
SF_Plot(Forced,'P.im','boundary','on','contour','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'') / Re(p'')',...
                'YLim',[-10,20]);
saveas(gcf,'FlangedPipe_SolutionFF.png')


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'AcousticTube.gif';
SF_Plot(Forced,'P','Amp',1,'boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k');
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'P','Amp',Amp,'boundary','on','contour','on','clevels',(-2 :.5 :2),...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k'); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<AcousticTube.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = -10 :.1 :10;
Uyaxis = SF_ExtractData(Forced,'Uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'P',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''_z)','Im(u''_z)','Re(p'')','Im(p'')');
pause(0.1);

%% Chapter 3 : loop over k to compute the impedance $Zin(k)$ 

IMP = SF_Launch('LinearForcedLoop_Acoustics.edp','mesh',ffmesh,'Options',{'rho',rho,'c',c,'omegamin',0,'omegamax',3,'Nomega',301});

%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title('Impedance $Z_r$ and $Z_i$','Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


figure;
semilogy(IMP.omega,abs(IMP.Z),'r');
title('Impedance $|Z|$ ','Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


%%
% plot Reflection coefficient 
% figure;
% plot(IMP.omega,IMP.R,'b--');
% xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
% ylabel('$R$','Interpreter','latex','FontSize', 30);
% title('Reflection coefficient','Interpreter','latex','FontSize', 30)
% set(findall(gca, 'Type', 'Line'),'LineWidth',2);
% 
% figure;
% semilogy(IMP.omega,IMP.R,'b--');
% xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
% ylabel('$R$','Interpreter','latex','FontSize', 30);
% title('Reflection coefficient','Interpreter','latex','FontSize', 30)
% set(findall(gca, 'Type', 'Line'),'LineWidth',2);









%% Chapter 3b : compute theoretical results

om1 = IMP.omega;
Delta = 0.61;%8/(3*pi);
L = 10;
Z0 = rho*c/S;
Zout = Z0*(-1i*Delta*om1/c+(om1/c*a).^2/2);
Ztheo = Z0*(Zout/Z0-1i*tan(IMP.omega*L/c))./(-1i*Zout/Z0.*tan(IMP.omega*L/c)+1);


% Plot $Z(k)$ : comparison with asymptotic 
figure(31);hold off;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'b');
hold on;
plot(om1,real(Ztheo),'r--',om1,imag(Ztheo),'b--');
legend('Z_r (num)','Z_i (num)','Z_r (asympt)', 'Z_i (asympt)')
title('Impedance $Z_r$ and $Z_i$, comparison with approx.','Interpreter','latex','FontSize', 24)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 24);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 24);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%
figure(32);hold off;
semilogy(IMP.omega,abs(IMP.Z),'r');
hold on;
semilogy(om1,abs(Ztheo),'r:');
title('Impedance $|Z|$ , comparision with asympt. approx','Interpreter','latex','FontSize', 24)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 24);
ylabel('$|Z|$','Interpreter','latex','FontSize', 24);
legend('|Z| (num)', '|Z| (theory)')
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


%%
% mise en forme finale
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
% [[PUBLISH]]